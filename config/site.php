<?php

return [

    'currency' => [
    	'USD' => [
	        'type' => 'USD', 
	        'sign' => '$'
    	]
    ],

    'osticket' => [
    	'id' => 3,
    	'version' => '1.10',
    	'api' => [
    		'key' => [
				'192.168.2.230'			=> '8AA3EAA87B7AABA4DB75D91E749D5299',
				'192.168.2.231'			=> 'FBE4F5B1FCA88248433665D17B6872A0',
				'192.168.2.232'			=> '29D04C401F8A244118BC5B36CC873FDE',
				'192.168.2.233'			=> '2AE4F92A661B91C523D1BB1379177BD1',
				// api key for the processing node
				'192.168.2.227'			=> '38BBAA9E42504BACB09DBF16366685F4',
    		],
    	],
    ],


];
