<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->name('psDashboard');

Route::middleware('not.authenticated.user')->group(function(){
	Route::get('register', 'Auth\RegisterController@create')->name('psUserRegistration');
	Route::post('register', 'Auth\RegisterController@store')->name('psUserRegistrationPost');

	Route::get('login', 'Auth\LoginController@index')->name('psUserLogin');
	Route::post('authenticate', 'Auth\LoginController@authenticate')->name('psUserLoginAuthenticate');
	Route::get('logout', 'Auth\LoginController@logout')->name('psUserLogout');

	Route::get('login/forgot', 'Auth\ResetPasswordController@create')->name('psResetPassword');
	Route::post('login/forgot', 'Auth\ResetPasswordController@store')->name('psResetPasswordPost');
});

Route::prefix('support')->group(function(){
	Route::get('policies.html', 'Support\PoliciesController@index')->name('psSupportPolicies');
	Route::get('privacy.html', 'Support\PrivacyController@index')->name('psSupportPrivacy');
	Route::get('shipping.html', 'Support\ShippingController@index')->name('psSupportShipping');
	Route::get('returns.html', 'Support\ReturnsAndExchangesController@index')->name('psSupportReturnsAndExchanges');
	Route::get('contact.html', 'Support\ContactUsController@index')->name('psSupportContactUs');
	Route::post('contact.html', 'Support\ContactUsController@send')->name('psSupportContactUsSend');
	Route::get('faq.html', 'Support\FAQController@index')->name('psSupportFAQ');
});

Route::prefix('about')->group(function(){
	Route::get('scholarship.html', 'About\ScholarshipController@index')->name('psAboutScholarship');
	Route::post('scholarship.html', 'About\ScholarshipController@store')->name('psAboutScholarshipPost');
	Route::get('scholarship-winners.html', 'About\ScholarshipController@winners')->name('psAboutScholarshipWinner');

	Route::get('careers.html', 'About\CareersController@index')->name('psCareers');
	Route::post('careers.html', 'About\CareersController@store')->name('psCareersPost');

	Route::get('team.html', 'About\TeamController@index')->name('psTeam');
	Route::get('reviews.html', 'About\ReviewsController@index')->name('psReviews');

	Route::post('newsletter', 'CMS\NewsletterController@store')->name('psNewsletter');
});

Route::get('how-to-plumb.html', 'CMS\HowToPlumbController@index')->name('psHowToPlumb');
Route::get('how-to-hvac.html', 'CMS\HowToHVACController@index')->name('psHowToHVAC');
Route::get('how-to-sprinklers.html', 'CMS\HowToSprinklersController@index')->name('psHowToSprinklers');

Route::get('account', 'Account\AccountController@index')->middleware('authenticated.user')->name('psAccount');
Route::prefix('account')->middleware('authenticated.user')->group(function(){
	Route::resource('info', 'Account\InfoController')->only(['index', 'edit', 'update']);
	Route::resource('login', 'Account\LoginInfoController')->only(['edit', 'update']);
	Route::resource('address-book', 'Account\AddressBookController');

	Route::resource('shopping', 'Account\ShoppingListController')->only(['index', 'create', 'store', 'show']);
	Route::get('subscriptions', 'Account\SubscriptionsController@index')->name('psAccountSubscriptions');
	#Route::resource('book', 'Account\AddressBookController')->only(['index', 'create', 'store']);

	Route::prefix('orders')->group(function(){
		Route::get('/', 'Account\OrdersController@index')->name('psAccountOrders');
		Route::get('{order_id}/tracking', 'Account\OrderTrackingController@show')->name('psAccountOrdersTracking');
		Route::get('{order_id}/print', 'Account\OrdersController@show')->name('psAccountOrdersPrint');
		
		Route::get('{order_id}/cancel', 'Account\OrderCancelController@index')->name('psOrderCancel');
		Route::put('{order_id}/cancel', 'Account\OrderCancelController@update')->name('psOrderCancelPut');

		Route::resource('return', 'Account\OrderReturnController')->only(['index', 'store']);
	});
});

Route::get('cart.html', 'Account\CartController@index')->name('psCart');
Route::post('cart.html/store', 'Account\CartController@store')->name('psCartStore');
Route::delete('cart.html/{key}', 'Account\CartController@destroy')->name('psCartDelete');

Route::resource('questions-and-answer', 'Catalog\ProductQuestionAndAnswerController');
Route::post('reviews/store', 'Catalog\ProductReviewController@store')->middleware('web')->name('psProductReviewStore');

Route::get('/{slug?}', 'Catalog\ProductPageController@index')->where('slug', '.+')->middleware('web')->name('psProductPage');