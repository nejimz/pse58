<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReturnsAndExchangesController extends Controller
{
    public function index()
    {
        return view('support.returns-and-exchanges');
    }
}
