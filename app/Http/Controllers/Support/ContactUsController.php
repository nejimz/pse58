<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog\CatalogOrder;
use App\Models\UserTickets;
use App\Models\TicketSystem;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('support.contact-us');
    }

    public function send()
    {
    	request()->validate([
    		'name' => 'required',
    		'email' => 'required',
    		'message' => 'required'
    	]);

    	$order_id = request()->content_id;
    	$name = request()->name;
    	$email = request()->email;
    	$phone = request()->phone_number;
    	$message = request()->message;
    	$content_id = $order_id;

    	$order = CatalogOrder::where('eclipseOrderId', $order_id)->first();

    	if (is_null($order)) {
    		$order = CatalogOrder::where('orderId', $order_id)->first();
    		if (!is_null($order)) {
    			$content_id = $order->eclipseOrderId;
    		} else {
    			$content_id = null;
    		}
    	}

    	if (!is_null($content_id)) {
    		$message = $message . 'OrderID:<a href="//gogreenentinc.com/eclipse_xref.php/?ordernumber=$contentid">' . $content_id . '</a>';
    	}

    	$ticket_id = TicketSystem::create_contact_ticket($message, $email, $name, $phone);

    	UserTickets::create([ 'email' => $email, 'ticket_id' => $ticket_id ]);

    	return redirect()->route('psSupportContactUs')->with('success_message', 'Your message was successfully delivered.');
    }
}
