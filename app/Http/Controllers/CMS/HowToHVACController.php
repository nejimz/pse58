<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HowToHVACController extends Controller
{
    public function index()
    {
        return view('support.how-to-hvac');
    }
}
