<?php

namespace App\Http\Controllers\Catalog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Catalog\CatalogProductQuestions;

class ProductQuestionAndAnswerController extends Controller
{
    public function store(Request $request)
    {
    	$request->validate([
    		'productId' => 'required',
    		'question' => 'required'
    	]);

    	$row = new CatalogProductQuestions;
    	$row->productId = intval($request->productId);
    	$row->question = $request->question;
    	$row->answer = [];
    	$row->user = 'Anonymous';
    	$row->email = null;
    	$row->save();

    	return response()->json(['message' => 'Question successfuly posted!']);
    }

    public function update($product_id, Request $request)
    {
    	$request->validate([
    		'reply_answer' => 'required'
    	]);

    	$row = CatalogProductQuestions::where('productId', intval($product_id))->first();

    	$answers = (isset($row->answers))? $row->answers : [];
    	$answers_count = count($answers)+1;
    	$answers[$answers_count]['answer'] = $request->reply_answer;
    	$answers[$answers_count]['user'] = 'Anonymous';
    	$answers[$answers_count]['date'] = date('Y-m-d H:i:s');
    	$answers = array_values($answers);

    	$row->answers = $answers;
    	$row->save();

    	return response()->json(['message' => 'Answer successfuly posted!']);
    }

    public function show($product_id)
    {
    	$rows = CatalogProductQuestions::where('productId', intval($product_id))->paginate(5);
    	return response()->json($rows);
    }
}
