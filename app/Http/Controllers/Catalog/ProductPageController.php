<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Catalog\CatalogCategories;
use App\Models\Catalog\CatalogProduct;
use App\Models\Catalog\CatalogCategoryAttributes;
use Illuminate\Support\Str;

class ProductPageController extends Controller
{
    //
    public function index(Request $request)
    {
        $url = str_replace(url('').'/', '', url()->current());
        $currency = config('site.currency.USD.sign');
        $category = CatalogCategories::where('meta.slug', $url)->first();
                
        if (!is_null($category)) {

            $layoutType = $category->layout_type;

            $sub_categories = $category->sub_categories->get();
            $additional_sub_categories = $category->additional_sub_categories;

            $sub_categories_count = $sub_categories->count();
            $additional_sub_categories_count = $additional_sub_categories->count();

            $has_promotional_images = $category->has_promotional_images;
            $promotional_ad_type = $category->promotional_ad_type($sub_categories_count, $additional_sub_categories_count);

            $data = compact('currency', 'category', 'layoutType', 'sub_categories', 'additional_sub_categories', 'sub_categories_count', 'additional_sub_categories_count', 'has_promotional_images', 'promotional_ad_type');

            if ($layoutType == 1) {
                $data = $this->template_1($request, $data, $category->id);
                return view('product.template-1', $data);
            } elseif ($layoutType == 2) {
                return view('product.template-2', $data);
            } elseif ($layoutType == 3) {
                return view('product.template-3', $data);
            } elseif ($layoutType == 4) {
                return view('product.template-4', $data);
            } elseif ($layoutType == 5) {
                return view('product.template-5', $data);
            }

	        #$data = compact('category');
		    #return view('product.index', $data);
        }

        $product = CatalogProduct::where('meta.slug', $url)->first();
        
        if (!is_null($product)) {
            $product_inventory_availability = 0;
            $product_price = $product->pricing['mapPrice'];
            if ($product_price < 1) {
                $product_price = $product->inventory['availability']['plumbersstock']['price'];
            }

            $data = compact('product', 'currency', 'product_inventory_availability', 'product_price');
            return view('product.index', $data);
        }

        abort(404);
    }

    public function template_1($request, $data, $category_id)
    {
        $data['show'] = 24;
        $current_url = url()->current();
        $data['full_url'] = url()->full();

        if ($request->has('show')) {
            $data['show'] = intval($request->show);
        } else {
            $data['full_url'] .= ((Str::contains($data['full_url'], '?'))? '&' : '?' ) . 'show=' . $data['show'];
        }

        $data['products'] = CatalogProduct::where('categories', new \MongoDB\BSON\ObjectId($category_id))->paginate($data['show']);
        $current_page = $data['products']->currentPage();

        $data['display'] = 'grid';
        $data['display_button_grid'] = '';
        $data['display_button_list'] = 'hollow';
        $data['display_url_grid'] = 'javascript:void(0)';
        $data['display_url_list'] = $current_url . '?display=list&page=' . $current_page . '&show=' . $data['show'];

        if ($request->has('display') && $request->display == 'list') {
            $data['display'] = 'list';
            $data['display_button_grid'] = 'hollow';
            $data['display_button_list'] = '';
            $data['display_url_grid'] = $current_url . '?display=grid&page=' . $current_page . '&show=' . $data['show'];
            $data['display_url_list'] = 'javascript:void(0)';
        }

        $data['category_attributes'] = CatalogCategoryAttributes::where('categoryId', $category_id)->get();

        return $data;
    }
}
