<?php

namespace App\Http\Controllers\Catalog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductReviewController extends Controller
{
    public function store(Request $request)
    {
    	$request->validate([
    		'rating' => 'required',
    		'review' => 'required'
    	]);
    	#dd($request->all());
    	return redirect()->back()->with('success_msg', 'Question successfuly posted!');
    }
}
