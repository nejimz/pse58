<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Catalog\CatalogOrder;
use App\Models\TicketSystem;
use Str;
use Sentinel;

class OrderCancelController extends Controller
{
    public function index($order_id)
    {
    	$reason = old('reason');
    	$description = old('description');
    	$replacement_product = old('replacement_product');
    	$data = compact('order_id', 'reason', 'description', 'replacement_product');
    	return view('account.orders.cancel', $data);
    }

    public function update($order_id)
    {
    	$request = request()->validate([
    		'reason' => 'required',
    		'description' => 'required_if:reason,Damaged,Defective,Received wrong item,Other,Website description error',
    		'replacement_product' => 'required_if:reason,Damaged,Defective,Received wrong item,Other',
    		'image' => 'required_if:reason,Damaged,Received wrong item|image',
    	]);

    	$image_url = '';
    	if(request()->hasFile('image')) {
    		$new_filename = date('YmdHis') . '_' . Str::random(10) .'.'. $request['image']->getClientOriginalExtension();
    		$file_path = 'img/returns';
    		$request['image']->move($file_path, $new_filename);
    		$image_url = $file_path . '/' . $new_filename;
    	}

    	$user = Sentinel::getUser();
    	$order = CatalogOrder::where('orderId', $order_id)->first();

    	if (!$order) {
    		return redirect()->route()->with('custom_error', 'Unable to find Order!');
    	}

    	unset($request['image']);

    	$ticket_id = TicketSystem::create_return_ticket($order, $user->email, $request, $image_url);
    	if ($ticket_id == 'Valid API key required') {
    		$ticket_id = '123456';
    	}
    	$order->return_ticket_id = $ticket_id;
    	$order->return_in_process = true;
    	$order->save();

    	return redirect()->route('psAccountOrders')->with('success_msg', 'Your request has been submitted. Expect to hear from customer service soon.');
    }
}
