<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Catalog\CatalogOrder;
use Sentinel;

class OrdersController extends Controller
{
    public function index()
    {
    	$rows = CatalogOrder::where('userId', intval(Sentinel::getUser()->id))->orderBy('created_at', 'DESC')->get();
    	$data = compact('rows');
        return view('account.orders.index', $data);
    }

    public function show($order_id)
    {
    	$row = CatalogOrder::where('orderId', $order_id)->first();
    	$data = compact('order_id', 'row');
        return view('account.orders.show', $data);
    }
}
