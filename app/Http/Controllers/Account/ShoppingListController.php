<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Catalog\ShoppingList;
use Sentinel;

class ShoppingListController extends Controller
{
    public function index()
    {
        $rows = ShoppingList::whereUserId(Sentinel::getUser()->id)->get();
        $data = compact('rows');
        return view('account.shopping.index', $data);
    }

    public function create()
    {
        $route = route('shopping.store');
        $name = old('name');
        $description = old('description');

        $data = compact('route', 'name', 'description');
        return view('account.shopping.form', $data);
    }

    public function store()
    {
        $input = request()->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        $products = [];
        $product_id = request()->product_id;
        $item = ShoppingList::whereName(request()->name)->first();
        if ($item) {// update
            $n = 0;
            $item_not_exists = true;
            foreach($item->products as $key => $value) {
                $products[$n] = [ '_id' => intval($value['_id']), 'qty' => intval($value['qty']) ];
                if ($product_id == $value['_id']) {
                    $item_not_exists = false;
                    $products[$n]['qty'] = intval( ($value['qty'] + 1) );
                }
                $n++;
            }
            if ($item_not_exists) {
                $products[$n] = [ '_id' => intval($product_id), 'qty' => intval(1) ];
            }
        } else { // insert
            $item->name = request()->name;
            $item->description = request()->description;
            $item->user_id = Sentinel::getUser()->id;
        }
        $item->products = $products;

        $item->save();
        return redirect()->route('shopping.create')->with('success_msg', 'Wish List successfully created!');
    }

    public function show($id)
    {
        $row = ShoppingList::where('_id', $id)->first();
        $data = compact('row');
        return view('account.shopping.show', $data);
    }
}
