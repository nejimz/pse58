<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer\AddressBook;
use App\Models\Customer\UserAddressBook;
use Sentinel;

class AddressBookController extends Controller
{
    public function index()
    {
        $user_id = Sentinel::getUser()->id;
        $addresses = AddressBook::whereIn('id', function($query) use ($user_id){
            $query->select('address_book_id')->from('user_address_book')->where('user_id', $user_id)->get();
        })->get();
        $data = compact('addresses');
        return view('account.address-book.index', $data);
    }

    public function create()
    {
        $route = route('address-book.store');
        $nickname = old('nickname');
        $first_name = old('first_name');
        $last_name = old('last_name');
        $company = old('company');
        $phone_number = old('phone_number');
        $email = old('email');

        $address_line_1 = old('address_line_1');
        $address_line_2 = old('address_line_2');
        $zip_code = old('zip_code');
        $city = old('city');
        $state = old('state');
        $country = old('country');

        $billing = old('billing');
        $shipping = old('shipping');
        $is_default_billing = old('is_default_billing');
        $is_default_shipping = old('is_default_shipping');

        $data = compact('route', 'nickname', 'first_name', 'last_name', 'company', 'phone_number', 'email', 'address_line_1', 'address_line_2', 'zip_code', 'city', 'state', 'country', 'billing', 'shipping', 'is_default_billing', 'is_default_shipping');
        return view('account.address-book.form', $data);
    }

    public function store()
    {
        request()->validate([
            'address_line_1' => 'required',
            'city' => 'required', 
            'state' => 'required', 
            'country' => 'required'
        ]);
        $input = request()->only(['nickname', 'first_name', 'last_name', 'company', 'phone_number', 'email', 'address_line_1', 'address_line_2', 'zip_code', 'city', 'state', 'country', 'billing', 'shipping', 'is_default_billing', 'is_default_shipping']);
        $input['first_name'] = $input['first_name'] ?? '';
        $input['last_name'] = $input['last_name'] ?? '';
        $input['nickname'] = $input['nickname'] ?? '';

        $address = AddressBook::create($input);
        UserAddressBook::create([
            'user_id' => Sentinel::getUser()->id,
            'address_book_id' => $address->id,
        ]);

        return redirect()->route('address-book.create')->with('success_msg', 'Address Book successfully added!');
    }

    public function edit($id)
    {
        $user_id = Sentinel::getUser()->id;
        $address = AddressBook::where('id', function($query) use ($id, $user_id){
            $query->select('address_book_id')->from('user_address_book')->where('address_book_id', $id)->where('user_id', $user_id)->first();
        })->first();
        if (!$address) {
            abort(404);
        }
        $route = route('address-book.update', $id);
        $nickname = $address->nickname;
        $first_name = $address->first_name;
        $last_name = $address->last_name;
        $company = $address->company;
        $phone_number = $address->phone_number;
        $email = $address->email;

        $address_line_1 = $address->address_line_1;
        $address_line_2 = $address->address_line_2;
        $zip_code = $address->zip_code;
        $city = $address->city;
        $state = $address->state;
        $country = $address->country;
        
        $billing = $address->billing;
        $shipping = $address->shipping;
        $is_default_billing = $address->is_default_billing;
        $is_default_shipping = $address->is_default_shipping;

        $data = compact('route', 'id', 'nickname', 'first_name', 'last_name', 'company', 'phone_number', 'email', 'address_line_1', 'address_line_2', 'zip_code', 'city', 'state', 'country', 'billing', 'shipping', 'is_default_billing', 'is_default_shipping');

        return view('account.address-book.form', $data);
    }

    public function update($id)
    {
        request()->validate([
            'address_line_1' => 'required',
            'city' => 'required', 
            'state' => 'required', 
            'country' => 'required'
        ]);
        $input = request()->only(['nickname', 'first_name', 'last_name', 'company', 'phone_number', 'email', 'address_line_1', 'address_line_2', 'zip_code', 'city', 'state', 'country', 'billing', 'shipping', 'is_default_billing', 'is_default_shipping']);
        $input['first_name'] = $input['first_name'] ?? '';
        $input['last_name'] = $input['last_name'] ?? '';
        $input['nickname'] = $input['nickname'] ?? '';
        AddressBook::whereId($id)->update($input);

        return redirect()->route('address-book.edit', $id)->with('success_msg', 'Address Book successfully updated!');
    }
}
