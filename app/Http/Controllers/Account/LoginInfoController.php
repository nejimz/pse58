<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Sentinel;

class LoginInfoController extends Controller
{
    public function edit($id)
    {
        return view('account.login.form');
    }

    public function update($id)
    {
    	$data = request()->validate([
    		'email' => 'required|same:email_confirm|max:255',
    		'password' => 'required|same:password_confirm|max:255'
    	]);
        $data['password'] = bcrypt($data['password']);
    	User::whereId(Sentinel::getUser()->id)->update($data);

    	return redirect()->route('login.edit', Sentinel::getUser()->id)->with('success_msg', 'Login Information successfully updated!');
    }
}
