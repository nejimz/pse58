<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Sentinel;

class InfoController extends Controller
{
    public function index()
    {
        return view('account.info.index');
    }

    public function edit($id)
    {
        return view('account.info.form');
    }

    public function update($id)
    {
    	$data = request()->validate([
    		'first_name' => 'required|max:255',
    		'last_name' => 'required|max:255',
    		'phone_number' => 'max:255',
    		'fax' => 'max:255'
    	]);

    	User::whereId(Sentinel::getUser()->id)->update($data);

    	return redirect()->route('info.edit', Sentinel::getUser()->id)->with('success_msg', 'Personal Information successfully updated!');
    }
}
