<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderTrackingController extends Controller
{
	public function show($order_id)
	{
		$data = compact('order_id');
		return view('account.orders.tracking', $data);
	}
}
