<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class CartController extends Controller
{
    public function index(Request $request)
    {
    	$grand_total = 0;
    	$item_count = 0;
        $currency = config('site.currency.USD.sign');

        if ($request->session()->has('user.cart')) {
        	$item_count = count($request->session()->get('user.cart'));
        }

        $data = compact('currency', 'item_count', 'grand_total');

        return view('account.cart', $data);
    }

    public function store(Request $request)
    {
    	#dd(session()->all());
    	$session_key = 'user.cart';
    	if (!$request->session()->has($session_key)) {
    		$request->session()->put($session_key, [$request->except('_token')]);
    	} else {
    		$counter_exists = 0;
    		foreach($request->session()->get($session_key) as $key => $value) {
    			if ($request->warehouse == $value['warehouse'] && $request->productId == $value['productId']) {
    				$counter_exists++;
    				$value['quantity'] += $request->quantity;
    				$request->session()->put($session_key.'.'.$key.'.price', $request->price);
    				$request->session()->put($session_key.'.'.$key.'.quantity', $value['quantity']);
    			}
    		}
    		if ($counter_exists == 0) {
    			$request->session()->push('user.cart', $request->except('_token'));
    		}
    	}
    	$this->compute_total($request);
    	return redirect()->route('psCart')->with('message', 'Item Successfully added!');
    }

    public function destroy(Request $request, $key)
    {
    	$session_key = 'user.cart';
    	$request->session()->forget($session_key.'.'.$key);
    	$this->compute_total($request);
    	return response()->json(['message' => 'Item Successfully removed!']);
    	//return redirect()->route('psCart')->with('message', 'Item Successfully removed!');
    }

    public function compute_total($request)
    {
    	$grand_total = 0;
    	if ($request->session()->has('user.cart')) {
    		foreach($request->session()->get('user.cart') as $key => $value) {
    			$total = $value['quantity'] * $value['price'];
    			$grand_total += $total;
    		}
			$request->session()->put('user.cart-total', $grand_total);
    	}
    }
}
