<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserTickets;
use App\Models\TicketSystem;

class CareersController extends Controller
{
    public function index()
    {
    	$full_name = old('full_name');
    	$email = old('email');
    	$phone = old('phone');
    	$application_type = old('application_type');
    	$url = old('url');
    	$cover_letter = old('cover_letter');

    	$data = compact('full_name', 'email', 'phone', 'application_type', 'url', 'cover_letter');
        return view('about.careers', $data);
    }

    public function store()
    {
    	request()->validate([
    		'full_name' => 'required',
    		'email' => 'required|email',
    		'document' => 'required_if:application_type,document',
    		'url' => 'required_if:application_type,url'
    	]);

    	if (request()->hasFile('document')) {
			$file_extension = request()->document->getClientOriginalExtension();
    		$valid_file_extensions = ['jpg', 'png', 'pdf', 'rtf', 'odt', 'docx', 'xps'];
    		if (!in_array($file_extension, $valid_file_extensions)) {
    			return redirect()->route('psCareers')->withInput()->with('custom_error', 'File is not valid. (.jpg, .png, .pdf, .rtf, .odt, .docx, .xps)');
    		}
    	}

    	$name = request()->full_name;
    	$email = request()->email;
    	$phone = request()->phone;
    	$application_type = request()->application_type;
    	$cover_letter = trim(request()->cover_letter);
    	$message = '';

    	if ($application_type == 'url') {
			$message .= 'Applicant\'s Online Resume - ' . request()->url . '<br><br>';
    	}

    	if ($cover_letter == '') {
			$message .= '(No Cover Letter)';
    	} else {
			$message .= '' . $cover_letter;
    	}

		$ticket_id = TicketSystem::create_resume_ticket($message, $email, $name, request()->document, $phone);

    	UserTickets::create([ 'email' => $email, 'ticket_id' => $ticket_id ]);

    	#return redirect()->route('psCareers')->with('success_message', 'Your application was successfully submitted.');
    }
}
