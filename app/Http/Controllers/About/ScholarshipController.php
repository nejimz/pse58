<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\CMSScholarship;
use App\Models\DataConfig;
use Mail;

class ScholarshipController extends Controller
{
    public function index()
    {
        $full_name = old('full_name');
        $email = old('email');
        $confirm_email = old('confirm_email');
        $school_name = old('school_name');
        $school_url = old('school_url');
        $video_url = old('video_url');

        $data = compact('full_name', 'email', 'confirm_email', 'school_name', 'school_url', 'video_url');
        return view('about.scholarship-application', $data);
    }

    public function store()
    {
        $input = request()->validate([
            'full_name' => 'required',
            'email' => 'required|same:confirm_email',
            'confirm_email' => 'required',
            'school_name' => 'required',
            'school_url' => 'required',
            'video_url' => 'required'
        ]);

        $scholarship = CMSScholarship::create($input);
        $config = DataConfig::whereIn('key', ['scholarship_email', 'scholarship_name', 'scholarship_message'])->get();

        $to_email = $config->where('key', 'scholarship_email')->first()->value;
        $to_name = $config->where('key', 'scholarship_name')->first()->value;
        $to_msg = $config->where('key', 'scholarship_message')->first()->value;
        $subject = 'Scholarship Application';
        $data = [ 'msg' => $to_msg, 'params' => $scholarship ];

        // change this code if its in production. add in queue 
        Mail::send('mail.scholarship', $data, function($message) use ($to_email, $to_name, $subject) {
            $message->to($to_email, $to_name);
            $message->subject($subject);
        });

        return redirect()->route('psAboutScholarship')->with('success_message', 'Your Application was successfully submitted!<br />We will review your Application and respond to you!');
    }

    public function winners()
    {
        $winners = CMSScholarship::where('winner', '1')->get();
        $data = compact('winners');

        return view('about.scholarship-winners', $data);
    }
}
