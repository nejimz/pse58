<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Sentinel;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Users\Eloquent\User;
use Cartalyst\Sentinel\Users\UserNotFoundException;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function authenticate()
    {
        try {
            $remember = false;
            $credentials = request()->only('email', 'password');
            Sentinel::enableCheckpoints();
            $authenticate = Sentinel::authenticate($credentials, $remember);

            if (!$authenticate) {
                return redirect()->route('psUserLogin')->withErrors(['password' => 'Invalid username or password.']);
            }
            return redirect()->route('psDashboard');
        } catch (\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e) {
            $time = ceil($e->getDelay()/60);
            $data = ['password' => "User is suspended for $time minutes."];
            return redirect()->route('psUserLogin')->withErrors($data);
        } catch (NotActivatedException $e) {
            return redirect()->route('psUserLogin')->withErrors(['password' => 'Your account has not been activated yet.']);
        }
    }

    public function logout()
    {
        Sentinel::logout();
        return redirect()->route('psDashboard');
    }
}
//jimmya@plumbersstock.com