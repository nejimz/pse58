<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Sentinel;

class RegisterController extends Controller
{
    public function create()
    {
        return view('auth.register');
    }

    public function store()
    {
        $credentials = request()->validate([
            'email' => 'required|email|same:confirm_email|unique:users,email',
            'password' => 'required|min:8|same:confirm_password'
        ],[
            'email.same' => 'Email Address must match.',
            'password.same' => 'Password must match.'
        ]);
        $credentials['eclipse_entity'] = 0;
        $credentials['phone_number'] = 0;
        Sentinel::register($credentials);

        return redirect()->route('psUserLogin')->with('register_success', 'Your Account Has Been Created. Please Login To Continue.');
    }
}
