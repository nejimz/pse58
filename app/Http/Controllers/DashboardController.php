<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Models\Catalog\CatalogFeatured;
use App\Models\CMS\CMSSpot;
use Cache;
use Auth;
use Queue;
use App\Models\CMS\CMSMenus;

class DashboardController extends Controller
{
	public function index()
	{
		$request = request();
        $key = $request->server('HTTP_HOST').':dashboard:'.$request->server('REQUEST_SCHEME').'-content';

		if ($request->has('render')) {
			return $this->landing_page();
		}

		$content = $this->cache($key);

		$data = compact('content');

    	return view('index-partials', $data);
	}

	public function cache($key)
	{
		$cacheCheck = Redis::exists($key);

		if ($cacheCheck == 0) {
			$content = $this->content();
			Redis::set($key, $content);
		} else {
			$content = Redis::get($key);
		}
		return $content;
	}

	public static function content()
	{
		$featureds = CatalogFeatured::limit(3)->get();
		$spots = CMSSpot::whereIn('page', ['home_v2', 'home'])->get();

		$banners = $spots->where('page', 'home_v2');
		$blog_posts = $spots->where('page', 'home');

		$data = compact('banners', 'featureds', 'blog_posts');

		$content = view('index-contents', $data)->render();
		return $content;
	}

	public function landing_page()
	{
		$featureds = CatalogFeatured::limit(3)->get();
		$spots = CMSSpot::whereIn('page', ['home_v2', 'home'])->get();

		$banners = $spots->where('page', 'home_v2');
		$blog_posts = $spots->where('page', 'home');

		$data = compact('banners', 'featureds', 'blog_posts');

    	return view('index', $data);
	}
}
