<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CMS\CMSMenus;
use Illuminate\Support\Facades\Redis;

class CacheMenu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:menu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache menu of front-end';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $protocol = $this->ask('What is your request scheme? (ex. http or https)');
        $host = request()->server('HTTP_HOST');
        $key = $host.':mainmenu:'.$protocol;

        #$protocol = $this->ask('What is your request scheme? (ex. http or https)');
        #$key = $protocol . '-' .  $host . '-mainmenu-default';
        $this->info('Key = '.$key);
        $this->info('Generating...');
        $this->mainmenu($key, $protocol);
        $this->info('Done!');
    }

    public function mainmenu($key, $protocol)
    {
        $mainmenus = CMSMenus::whereEnabled(true)->orderBy('sortNumber', 'ASC')->get();
        $menus = $mainmenus->where('parent', 0)->sortBy('sortNumber')->all();

        $string = '<ul class="menu dropdown expanded site-menus" data-dropdown-menu>';
        
        foreach($menus as $menu) {
            $route = $this->route_filter($menu->path, $protocol);

            $string .= '<li class="site-menus-item text-center"><a href="'.$route.'" class="">'.$menu->name.'</a>';
            $string .= $this->recursive($mainmenus, $menu->menuId, $protocol);

            $string .= '</li>';
        }
        $string .= '</ul>';

        $cacheCheck = Redis::exists($key);

        if ($cacheCheck == 1) {
            Redis::del($key);
        }
        Redis::set($key, $string);
    }

    public function recursive($mainmenus, $menu_id, $protocol)
    {
        $menus = $mainmenus->where('parent', $menu_id)->where('enabled', true)->sortBy('sortNumber')->sortBy('group')->all();
        $string = '<ul class="menu">';
        foreach ($menus as $menu) {
            $route = $this->route_filter($menu->path, $protocol);

            $string .= '<li><a href="'.$route.'" class="">'.$menu->name.'</a>';
            $submenus = $mainmenus->where('parent', $menu->menuId)->where('enabled', true)->sortBy('sortNumber')->sortBy('group')->all();
            if (count($submenus) > 0 && $menu->displayChildren) {
                $string .= $this->recursive($mainmenus, $menu->menuId, $protocol);
            }
        }
        $string .= '</ul>';

        return $string;
    }

    public function route_filter($menu_path, $protocol)
    {
        $route = route('psProductPage', $menu_path);

        if ($menu_path == '') {
            return 'javascript:void(0)';
        } elseif ($protocol == 'https') {
            return str_replace('http', $protocol, $route);
        }
        return $route;
    }
}
