<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Catalog\CatalogProduct;
use App\Jobs\ProductInventoryJob;
use \Carbon\Carbon;

class ProductInventoryCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product-inventory:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dump('Creating Inventory Job...');

        $row_per_page = 10000;

        $products = CatalogProduct::where('channels.ebayCstock', true)
                    ->where('productId', '<', 10000000)
                    ->where('brand', 'exists', true)
                    ->where('brand', '<>', 'Brizo')
                    ->where('title', '<>', '')
                    ->orWhere(function($query) {
                        $query->where('images.0', 'exists', true)
                        ->where('images.0', '<>', '');
                    })
                    ->orWhere(function($query) {
                        $query->where('ebayInfo.image.PictureURL', 'exists', true)
                        ->where('ebayInfo.image.PictureURL', '<>', '');
                    })
                    ->orWhere(function($query) {
                        $query->where('amazon.content.attributes.SmallImage.URL', 'exists', true)
                        ->where('amazon.content.attributes.SmallImage.URL', '<>', '');
                    })
                    ->count();

        $total_page = ceil($products / $row_per_page);
        dump('Total Products = ' . $products);
        dump('Total Page = ' . $total_page);

        for($offset = 0; $offset <= $total_page; $offset++) {
            $job = (new ProductInventoryJob($offset))->delay(Carbon::now()->addSeconds(2));
            dispatch($job);
        }
        dump('Done Creating Inventory Job...');
    }


    /*

        foreach ($products as $product) {
            
            #dump('----------------------');
            #dump('Product ID = ' . $product->productId);
            
            $cost = [];
            foreach ($product->inventory['availability'] as $key => $value) {
                if ($value['qty'] > 0) {
                    $cost[$key] = $value['cost'];
                    //dump('   ' . $key . ' qty=' . $value['qty'] . ', cost=' . $value['cost']);
                }
            }
            if (count($cost) > 0) {
                $lowest_cost = min($cost);
                $vendor = array_search($lowest_cost, $cost);
                
                #dump('');
                #dump('     Lowest Cost : ' . $lowest_cost);
                #dump('     Vendor is : ' . $vendor);
                #dump('     Qty : ' . $product->inventory['availability'][$vendor]['qty']);
                
                
                #CatalogProduct::where('_id', $product->_id)->update([
                #    'vendor_lowest_cost' => [
                #        'name' => $vendor,
                #        'qty' => $product->inventory['availability'][$vendor]['qty'],
                #        'cost' => $lowest_cost
                #    ]
                #]);
                $product->vendor_lowest_cost = [
                    'name' => $vendor,
                    'qty' => $product->inventory['availability'][$vendor]['qty'],
                    'cost' => $lowest_cost
                ];
                $product->save();
                //$counter++;
            }
            //dump('----------------------');
        }
    */
}
