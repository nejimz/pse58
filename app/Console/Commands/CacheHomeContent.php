<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Redis;
use Cache;

class CacheHomeContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:home-content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache landing page content';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $protocol = $this->ask('What is your request scheme? (ex. http or https)');
        $host = request()->server('HTTP_HOST');
        $key = $host.':dashboard:'.$protocol.'-content';

        $this->info('Starting...');
        $this->info('Key='.$key);
        
        $content = DashboardController::content();
        $cacheCheck = Redis::exists($key);

        if ($cacheCheck == 1) {
            Redis::del($key);
        }
        Redis::set($key, $content);

        $this->info('Done!');
    }
}
