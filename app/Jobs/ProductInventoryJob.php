<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Catalog\CatalogProduct;
use \Carbon\Carbon;

class ProductInventoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $offset;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($offset)
    {
        $this->offset = $offset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $row_per_page = 10000;
        $offset = ($this->offset * $row_per_page) + 1;

        dump(number_format($offset) . ' - ' . number_format($row_per_page));

        $products = CatalogProduct::where('channels.ebayCstock', true)
                    ->where('productId', '<', 10000000)
                    ->where('brand', 'exists', true)
                    ->where('brand', '<>', 'Brizo')
                    ->where('title', '<>', '')
                    ->orWhere(function($query) {
                        $query->where('images.0', 'exists', true)
                        ->where('images.0', '<>', '');
                    })
                    ->orWhere(function($query) {
                        $query->where('ebayInfo.image.PictureURL', 'exists', true)
                        ->where('ebayInfo.image.PictureURL', '<>', '');
                    })
                    ->orWhere(function($query) {
                        $query->where('amazon.content.attributes.SmallImage.URL', 'exists', true)
                        ->where('amazon.content.attributes.SmallImage.URL', '<>', '');
                    })
                    ->offset($offset)
                    ->limit($row_per_page)
                    ->get();

        $counter = 0;
        foreach ($products as $product) {
            $cost = [];
            foreach ($product->inventory['availability'] as $key => $value) {
                if ($value['qty'] > 0) {
                    $cost[$key] = $value['cost'];
                }
            }
            if (count($cost) > 0) {
                $lowest_cost = min($cost);
                $vendor = array_search($lowest_cost, $cost);
                $product->vendor_lowest_cost = [
                    'name' => $vendor,
                    'qty' => $product->inventory['availability'][$vendor]['qty'],
                    'cost' => $lowest_cost,
                    'created_at' => Carbon::now()
                ];
                $product->save();
                $counter++;
            }
        }
        dump(number_format($counter));
    }
}
