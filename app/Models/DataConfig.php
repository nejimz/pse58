<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DataConfig extends Eloquent
{
    protected $connection = 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'data_config';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	protected $primaryKey = '_id';
	public $incrementing = false;
	public $timestamps = true;
	protected $guarded = [];
}
