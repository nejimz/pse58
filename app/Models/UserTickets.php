<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTickets extends Model
{
    protected $connection = 'mysql';
    protected $table = 'users_tickets';
	protected $guarded = [];
	public $timestamps = false;
}
