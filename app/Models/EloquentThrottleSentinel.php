<?php
namespace App\Models;

use Cartalyst\Sentinel\Throttling\EloquentThrottle;

class EloquentThrottleSentinel extends EloquentThrottle
{
    /**
     * {@inheritDoc}
     */
    protected $table = 'throttle_sentinel';

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'ip',
        'type',
    ];
}
