<?php

namespace App\Models;

/*use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;*/
use Cartalyst\Sentinel\Users\EloquentUser;

//class User extends Authenticatable
class User extends EloquentUser
{
    //use Notifiable;
    protected $connection = 'mysql';
    protected $table = 'users';
    protected $fillable = [
        'eclipse_entity', 'company_name', 'tax_id', 'profession_type', 'referral', 'retail', 'first_name', 'last_name', 'phone_number', 'fax', 'email', 'password', 'discount_level', 'permissions', 'activated', 'activation_code', 'activated_at', 'last_login', 'persist_code', 'reset_password_code', 'created_at', 'updated_at', 'contact_login'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    /*protected $casts = [
        'email_verified_at' => 'datetime',
    ];*/
}
