<?php

namespace App\Models;

class TicketSystem
{
	public static function create_resume_ticket($message, $email, $name, $file = '', $phone = '') {
		$row = [
			'email' => $email,
			'name' => $name,
			'subject' => 'Careers with PlumberStock - New Application from ' . $name,
			'message' => $message,
			'alert' => false,
			'autorespond' => false,
			'ip' => (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : "",
			'phone' => $phone,
			'priorityId' => 2,
			'created' => date('Y-m-d H:i:s'),
			'html' => true,
			'deptId' => config('osticket.id')
		];

		if(isset($file)){
			$row['attachments'] = [
		 			[$file->getClientOriginalName() => 'data:'.$file->getMimeType().';base64,' . base64_encode(file_get_contents($file->getRealPath()))]
			];
		}

		$data = json_encode($row);

		$transaction_id = self::send($data);
		return $transaction_id;
	}

	public static function create_contact_ticket($message, $email, $name = '', $phone = '')
	{
		$data = json_encode([
			'phone' => $phone,
			'autorespond' => false,
			'subject' => 'PlumberStock Support - New Question from ' . $name,
			'alert' => false,
			'ip' => (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '',
			'message' => $message,
			'email' => $email,
			'created' => date('Y-m-d H:i:s'),
			'priorityId' => 2,
			'name' => $name,
			'html' => true,
			'deptId' => config('osticket.id')
		]);

		$transaction_id = self::send($data);
		return $transaction_id;
	}

	public static function create_return_ticket($order, $email, $request, $image_url = null)
	{
		$reason = $request['reason'];
		$description = $request['description'];
		$replacement = $request['replacement_product'];

		$type = ($order->has_shipped) ? 'Return' : 'Cancel';

		$message = '<p>' . $type . ' Order #: ' . $order->orderId . '</p><p>Reason: ' . $reason . '</p><p>Description: ' . $description . '</p>';

		if($replacement === true) {
			$message .= "<p>Replacement Part Requested</p>";
		}
		if( !empty($image_url) ) {
			$message .= '<p><img src="' . $image_url . '"></p>';
		}

		$data = json_encode([
			'phone' => $order->shipping['phone'],
			'autorespond' => false,
			'subject' => "$type Order #" . $order->orderId,
			'alert' => false,
			'ip' => '',
			'message' => $message,
			'email' => $email,
			'created' => date('Y-m-d H:i:s'),
			'priorityId' => 11,
			'name' => $order->shipping_name,
			'html' => true,
			'deptId' => config('osticket.id')
		]);

		$transaction_id = self::send($data);
		return $transaction_id;
	}

	public static function send($data)
	{
		$url = 'http://www.gogreenentinc.com/support/api/tickets.json';

		if(config('site.osticket.version') == '1.10'){
			$url = 'https://ticket.gogreenentinc.com/api/tickets.json';
			$data = json_decode($data,true);
			$data['message'] = 'data:text/html,' . $data['message'];
			$data = json_encode($data);
		}

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CAINFO, base_path("cacert.pem")); // this is only for local to connect on OS ticket
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data),
			"X-API-KEY: {self::api()}" #. 
		]);
		$transaction_id = curl_exec($ch);
		$cerr = curl_error($ch);
		curl_close($ch);
		#dump($transaction_id);

		return $transaction_id;
	}

	public static function api()
	{
		$env = env('OSTICKET');
		if (!is_null($env)) {
			return $env;
		}
		$api_keys = config('site.osticket.api.key');
		return $api_keys[request()->server('SERVER_ADDR')];
	}
}
