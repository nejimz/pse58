<?php

namespace App\Models\CMS;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CMSStaticBlock extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'static_blocks';
	protected $guarded = [];

	public $timestamps = true;
}
