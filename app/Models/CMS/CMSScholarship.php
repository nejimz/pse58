<?php

namespace App\Models\CMS;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CMSScholarship extends Eloquent
{
    protected $connection = 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'scholarship_applications';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	protected $primaryKey = '_id';
	public $incrementing = false;
	public $timestamps = true;
	protected $guarded = [];
}
