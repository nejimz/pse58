<?php

namespace App\Models\CMS;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CMSMenus extends Eloquent
{
    protected $connection = 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'menus';
	protected $guarded = [];

	public $timestamps = true;
}
