<?php

namespace App\Models\CMS;

#use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CMSSpot extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'spots';
	protected $guarded = [];

	public $timestamps = false;
}
