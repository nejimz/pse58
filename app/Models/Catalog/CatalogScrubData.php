<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CatalogScrubData extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'products_cloudcover';
	protected $guarded = [];

	protected $primaryKey = '_id';
	public $timestamps = false;
	public $incrementing = false;
}
