<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogLeadTime extends Model
{
    protected $connection= 'mysql';
	protected $database = 'typhoeus';
	protected $table = 'pstock_leadtimes';
	protected $guarded = [];
	public $timestamps = false;
}
