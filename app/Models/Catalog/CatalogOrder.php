<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Catalog\CatalogSDShipments;

class CatalogOrder extends Eloquent
{

	use SoftDeletes;

    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'orders';
	protected $primaryKey = '_id';
	protected $guarded = [];
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	protected $orderConf = null;

	public $incrementing = false;
	public $timestamps = true;
	public $softdeletes = true;
	public $smsNotifications = false;

	public function getEclipseIdAttribute()
	{
		$eclipse_order_id =($this->eclipseOrderId != '') ? $this->eclipseOrderId : '';

		if (is_array($eclipse_order_id) && count($eclipse_order_id) > 0) {
			return (isset($eclipse_order_id)) ? $eclipse_order_id[0] : '';
		}
		return $eclipse_order_id;
	}

	public function getShippingNameAttribute() {
		$name = $this->shipping['firstName'] . ' ' . $this->shipping['lastName'];
		return $name;
	}

	public function getBillingNameAttribute() {
		$name = $this->billing['firstName'] . ' ' . $this->billing['lastName'];
		return $name;
	}

	public function getCardTypeAttribute() {
		if (isset($this->payment['card'])) {
			try {
				$type = decrypt($this->payment['card']['type']);
			} catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
				$type = 'NADC';
			} finally {
				if ($type == 'VI') {
					return 'Visa';
				} elseif ($type == 'MC') {
					return 'MasterCard';
				} elseif ($type == 'DS') {
					return 'Discover';
				} elseif ($type == 'AX') {
					return 'American Express';
				}
			}

		}
		return '';
	}

	public function card_number($last_4_digit) {
		if (isset($this->payment['card'])) {
			try {
				$number = decrypt($this->payment['card']['number']);
			} catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
				$number = 'NADC';
			} finally {
				if ($last_4_digit) {
					return substr($number, -4);
				}
			}

		}
		return '';
	}

	public function getHasShippedAttribute() {
		$status = $this->item_status();
		return ( !empty($status['carrier']) && !empty($status['tracking']) && $status['status'] == 'Complete');
	}

	public function item_status($eclipseId = null, $gen = null)
	{
		$search_value = '';
		if (is_null($eclipseId)) {
			$search_value = $this->eclipse_id;
		} elseif (isset($eclipseId) && isset($gen)) {
			$strlen = strlen($gen);
			if ($strlen == 1) {
				$gen = '00' . $gen;
			} elseif ($strlen == 2) {
				$gen = '0' . $gen;
			}
			$search_value = $eclipseId .".". $gen;
		} elseif (isset($eclipseId)) {
			$search_value = $eclipseId;
		} else {
			return ['status' => 'Pending',	'tracking' => null,	'carrier' => null];
		}

		$ship_order = CatalogSDShipments::where('EclipseId', 'LIKE', "$search_value%")->orderBy('EclipseId', 'DESC')->first();
		$carrierLinks = [
			'UPS'		=> 'https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=',
			'FEDEX'		=> 'https://www.fedex.com/Tracking?action=track&tracknumbers=',
			'USPS'		=> 'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=',
			'DHL'		=> 'https://track.dhl-usa.com/TrackByNbr.asp?ShipmentNumber=',
			'DHLGLOBAL'	=> 'https://webtrack.dhlglobalmail.com/?mobile=&trackingnumber=',
			'ONTRAC'	=> 'https://www.ontrac.com/trackingdetail.asp?tracking=',
			'ICC'		=> 'https://iccworld.com/track.asp?txtawbno=',
			'LASER'		=> 'https://www.lasership.com/track.php?track_number_input='
		];

		if (is_null($ship_order)) {
			return ['status'=>'Pending', 'carrier'=>null, 'tracking'=>null, 'trackLink'=>null];
		}

		$ship_data = ['carrier'=> $ship_order->CarrierType,'tracking'=>$ship_order->TrackingNumber,'trackLink'=>''];

		if (array_key_exists($ship_order->CarrierType, $carrierLinks)) {
			$ship_data['trackLink'] = $carrierLinks[$ship_order->CarrierType] . '' . $ship_order->TrackingNumber;
		}

		if($ship_order->ShipDate > date('Ytm') || $ship_order->ShipStatus == 'Pending') {
			$ship_data['status'] = 'Pending';
			return $ship_data;
		} elseif($ship_order->ShipDate <= date('Ytm') && $ship_order->ShipStatus == 'Processed') {
			$ship_data['status'] = 'ship_order';
			return $ship_data;
		} elseif($ship_order->ShipDate <= date('Ytm') && $ship_order->ShipStatus == 'Processing') {
			$ship_data['status'] = 'In Transit';
			return $ship_data;
		} else {
			return ['status'=>'Pending', 'carrier'=>null, 'tracking'=>null, 'trackLink'=>null];
		}
	}
}
