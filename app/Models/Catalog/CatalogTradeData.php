<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CatalogTradeData extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'products_trade';
	protected $guarded = [];

	protected $primaryKey = '_id';
	public $timestamps = false;
	public $incrementing = false;
}
