<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Models\CMS\CMSMenus;

class CatalogCategories extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'categories';
	protected $guarded = [];

	public function getLayoutTypeAttribute()
	{
		if (isset($this->layout)) {
			return $this->layout['type'];
		}
		return 1;
	}

	public function getSubCategoriesAttribute()
	{
		return self::where('parent', $this->categoryId);
	}

	public function getAdditionalSubCategoriesAttribute()
	{
		$additional_sub_categories = explode(',', $this->layout['additionalSubcategories']);
		if (count($additional_sub_categories) > 0) {
			$additional_sub_categories = $this->arrayIntParse($additional_sub_categories);
			return self::whereIn('categoryId', $additional_sub_categories)->get();
		}
		return $count($additional_sub_categories);
	}

	public function getSubLinksAttribute()
	{
		return self::where('parent', $this->categoryId)->where('layout.priority', true)->get();
	}

	public function getHasPromotionalImagesAttribute()
	{
		$count = count($this->layout['images']['promotional']);
		if ($count > 0) {
			return true;
		}
		return false;
	}

	#public function getPromotionalAdTypeAttribute()
	public function promotional_ad_type($sub_categories, $additional_sub_categories)
	{
		$sum = ($sub_categories + $additional_sub_categories) % 3;
		return $sum;
	}

	public function has_promotional_ad($ad_type)
	{
		return (trim($this->layout['images']['promotional'][$ad_type]) != '');
	}

	public function arrayIntParse($array)
	{
		foreach ($array as $key => $value) {
			$array[$key] = (int)$value;
		}
		return $array;
	}

	public function getTranslateSnippetAttribute()
	{
		$snippet = '';
		if (isset($this->snippetContent)) {
			$snippet = $this->snippetContent;
		}
		return $snippet;
	}

	public function getHasSubCategoriesSubLinksAttribute()
	{
		$sublinks = $this->sublinks;
		if (count($sublinks) > 0) {
			return true;
		}
		return false;
	}

	public function getSubCategoriesSubLinksAttribute()
	{
		$sublinks = $this->sublinks;
		if (count($sublinks) > 0) {
			return \App\Models\Catalog\CatalogSublink::whereIn('_id', $sublinks);
		}
		return false;
	}

	public function getBreadcrumbsAttribute()
	{
		$breadcrumb = '';

		$row1 = CMSMenus::wherePath($this->meta['slug'])->first();
		if ($row1->parent != 0) {
			$breadcrumb = '<li class="disabled">'.$row1->name.'</li>';

			$row2 = CMSMenus::where('menuId', $row1->parent)->first();
			if ($row2->parent != 0) {
				$breadcrumb = '<li><a href="'.route('psProductPage', $row2->path).'">'.$row2->name.'</a></li>' . $breadcrumb;

				$row3 = CMSMenus::where('menuId', $row2->parent)->first();
				if ($row3->parent != 0) {
					$breadcrumb = '<li><a href="'.route('psProductPage', $row3->path).'">'.$row3->name.'</a></li>' . $breadcrumb;

					$row4 = CMSMenus::where('menuId', $row3->parent)->first();
					if ($row4->parent != 0) {
						$breadcrumb = '<li><a href="'.route('psProductPage', $row4->path).'">'.$row4->name.'</a></li>' . $breadcrumb;

						$row5 = CMSMenus::where('menuId', $row4->parent)->first();
						if ($row5->parent != 0) {
							$breadcrumb = '<li><a href="'.route('psProductPage', $row5->path).'">'.$row5->name.'</a></li>' . $breadcrumb;
						}
					}
				}
			}
		}
		return $breadcrumb;
	}
}
