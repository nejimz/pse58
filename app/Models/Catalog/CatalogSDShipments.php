<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogSDShipments extends Model
{
	protected $connection = 'mysql_shipment';
	protected $database = 'typhoeus';
	protected $table = 'sd_shipments';
	protected $guarded = [];

	public $timestamps = true;
}
