<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CatalogCategoryAttributes extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'category_attributes';
	protected $guarded = [];
	public $incrementing = false;
	public $timestamps = false;
}
