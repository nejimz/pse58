<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
//use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Models\Catalog\CatalogProduct;

class CatalogFeatured extends Model
{
    protected $connection= 'mysql';
	protected $database = 'typhoeus';
	protected $table = 'featured';
	protected $guarded = [];

	public $timestamps = false;

	public function product()
	{
		#return $this->hasOne(CatalogProduct::class, 'productId', 'pId');
		$product = CatalogProduct::select('title', 'price', 'meta')->where('productId', $this->pId)->first();
		return $product;
	}
}
