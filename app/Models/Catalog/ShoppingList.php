<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Models\Catalog\CatalogProduct;

class ShoppingList extends Eloquent
{
    protected $connection = 'mongodb';
	protected $database = 'typhoeus';
 	protected $collection = 'shopping_lists';

  	public $timestamps = true;
  	public $softDeletes = true;

	protected $guarded = [];
  	protected $primaryKey = '_id';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function product_image($id, $width, $height)
	{
		$row = CatalogProduct::where('productId', intval($id))->first();
		if ($row) {
			return '<img src="//images2.plumbersstock.com/' . $width . '/' . $height . '/' . $row->images[0]['id'] . '" width="' . $width . 'px">';
		}

		return '';
	}

	public function product($id)
	{
		return CatalogProduct::where('productId', intval($id))->first();
	}
}
