<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CatalogSublink extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'sublinsk';
	protected $guarded = [];
	public $timestamps = false;

}
