<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Models\CMS\CMSMenus;
use App\Models\Catalog\CatalogAttributes;
use App\Models\Catalog\CatalogUOMOverride;
use App\Models\Catalog\CatalogScrubData;
use App\Models\Catalog\CatalogTradeData;
use App\Models\Catalog\CatalogVendorCodeName;
use App\Models\Catalog\CatalogLeadTime;
use Exception;
use App;

class CatalogProduct extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'products';
	protected $guarded = [];

	public $timestamps = false;
	public $incrementing = true;

	public function ratings($round_to_half = true)
	{
		if ($round_to_half === true) {
			return floor(($round_to_half * 2) + 0.5) / 2;
		}

		$avg_rating = 0;
		$review_count = 0;

		if (isset($this->reviews)) {
			foreach ($this->reviews as $key => $value) {
				if (isset($value['approve']) && $value['approve'] == true) {
					$avg_rating += $value['rating'];
					$review_count++;
				}
			}
			if ($review_count > 0) {
				$avg_rating = $avg_rating / $review_count;
				$avg_rating = round($avg_rating, 1);
			} else {
				$avg_rating = 0;
			}
		}

		return $avg_rating;
	}

	public function discount($double_retail_price = false)
	{
		$discount = 0;
		if (isset($this->pricing['mapMethod']) && $this->pricing['mapMethod'] == 'Cart' && isset($this->buyLine) && $this->buyLine == 'X-DELTA') {
			return false;
		} elseif ($double_retail_price == false) {
			$discount = (1 - ($this->pricing['mapPrice'] / $this->pricing['listPrice'])) * 100;
			#return floatval(number_format(((1 - ($this->getPrice() / $this->getListPrice())) * 100), 0));
		} else {
			$discount = (1 - ($this->pricing['mapPrice'] / $double_retail_price)) * 100;
			#return floatval(number_format(((1 - ($this->getPrice() / $doubleRetailPrice)) * 100), 0));
		}
		return number_format($discount, 2);
	}

	public function vendor_name($name)
	{
		$row = CatalogVendorCodeName::where('vendor', $name)->first();

		return $row->codename;
	}

	public function vendor_name_2($name)
	{
		if ($name == 'plumbersstock') {
			return 'Order From Manufacturer';
		}

		$row = CatalogVendorCodeName::where('vendor', $name)->first();

		return $row->codename;
	}

	public function lead_time($name)
	{
		$row = CatalogLeadTime::where('thirdparty_vendor', $name)->orWhere('thirdparty_vendor', 'default')->first();
		return $row->leadtime_msg_buyingOp;
	}

	public function featured()
	{
		return $this->belongsTo(CatalogFeatured::class, 'productId', 'pId');
	}

	public function getProductPriceAttribute()
	{
		return $this->pricing['price'] * $this->uomQty;
	}

	public function getIsFreeShipAttribute()
	{
		try {
			if($this->ships_freight == true) {
				return false;
			}
			elseif($this->ships_free == true) {
				return true;
			}
			elseif($this->price > 1000) {
				return true;
			}
			elseif(($this->getPrice() == $this->getMapPrice()) && ($this->getMapPrice() > 0.01)) {
				return true;
			}
			elseif( (strtolower($this->getMapMethod()) == "cart") && ($this->getPrice() <= $this->getMapPrice()) ) {
				return true;
			}
			else {
				return false;
			}
		} catch(Exception $e) {
			#dd('Exception');
			return false;
		}
	}

	public function getShipsFreightAttribute()
	{
		$weight = $this->weight;

		if (isset($this->orgill) && $this->orgill != '' && ($this->orgill['freight'] == true)) {
			return true;
		}

		if($weight < $this->dimensional_weight) {
			$weight = $this->dimensional_weight;
		}

		if(isset($this->shipping['freight']) && $this->shipping['freight'] == true){
			return true;
		}
		elseif( ($weight > 150) && isset($this->shipping['free']) && ($this->shipping['free'] == false) ){
			return true;
		}
		elseif(( ($this->height >= 180) || ($this->width >= 180) || ($this->length >= 180) ) && (isset($this->shipping['free']) && ($this->shipping['free'] == false)) ){
			return true;
		}
		elseif( ($this->girth_value >= 160) && (isset($this->shipping['free']) && ($this->shipping['free'] == false)) ){
			return true;
		}
		else {
			return false;
		}
	}

	public function getShipsFreeAttribute() {
		if ($this->shipping["free"] != '') {
			return true;
		}
		return false;
	}

	public function getPrice($vendor = 'plumbersstock', $testing = false) {
		try {
			$pricing	= App::make("Pricing");
			$pid		= $this->getProductId();
			if(empty($this->inventory['availability'][$vendor]['price']) || ($this->inventory['availability'][$vendor]['price'] <= 0.01)) {
				if($testing) {
					return "[!]The price on this product is set to ". $this->inventory['availability'][$vendor]['price'] ."!<br>";
				}
				return 0.01;
			}

			$avail = $this->inventory['availability'];

			if($testing){
				if(($vendor != "plumbersstock") && ($vendor != "swplumbing")){
					if(empty($avail[$vendor])){
						return "There is no pricing information for this vendor: $vendor";
					}
					if(!empty($avail[$vendor]['cost']) && !empty($avail["plumbersstock"]['price']) && ($pricing->figurePrice($this, $vendor, false) > $avail["plumbersstock"]['price'])){
						return $pricing->figurePrice($this, $vendor, $testing);
					}
					else {
						$response = $pricing->figurePrice($this, $vendor, $testing);
						$price = floatval($avail["plumbersstock"]['price']);
						$response .= "<br>Pricing from $vendor would have been cheaper than plumbersstock. Repriced to $price";
						return $response;
					}
				}
				else {
					return $pricing->figurePrice($this, $vendor, $testing);
				}
			}
			elseif(($vendor != "plumbersstock") && ($vendor != "swplumbing") && ($pricing->figurePrice($this, $vendor, $testing) > $avail["plumbersstock"]['price'])){
				$price = $pricing->figurePrice($this, $vendor, false);
			}
			elseif($avail['plumbersstock']['price'] > 0.01){
				//$price = $pricing->figurePrice($this, $vendor, $testing); //use this line to calculate price on-the-fly
				if($this->isEmailForPrice() === true) {
					$price = $this->pricing['mapPrice'];
				}
				else{
					$price = $avail['plumbersstock']['price'];
				}
			}
			else {
				return 0.01;
			}

			return $this->getOverridePrice($price);
		}
		catch(Exception $e) { //if pricing fails, return 0.01 so that the pages show "call for pricing"
			if($testing) {
				return ("There was an error getting pricing:<br> ".json_encode($e));
			}
			else {
				return 0.01;
			}
		}
	}

	public function getGirthValueAttribute() {
		return '1';
		$height = $this->height;
		return '2';
		$width = $this->width;
		return $height . ', ' . $width;
		$length = $this->length;

		return $height . ', ' . $width . ', ' . $length;
		$girth1 = $length + (2 * ($height + $width));
		$girth2 = $height + (2 * ($length + $width));
		$girth3 = $width + (2 * ($length + $height));

		if($girth1 > $girth2 && $girth1 > $girth3) {
			return $girth1;
		} else if($girth2 > $girth1 && $girth2 > $girth3) {
			return $girth2;
		} else {
			return $girth3;
		}
	}

	public function getDimensionalWeightAttribute()
	{
		#	getDimensionalWeight() {
		$height = $this->height;
		$weight = $this->width;
		$length = $this->length;

		if($height > 0 && $weight > 0 && $length > 0) {
			$DIMfactor = 194; //according to MikeG on 8.11, our current DIM factor from FedEx is 194 and from UPS is 200
			$packagingOverhead = 1.10;
			try {
				$dim = ($height * $weight * $length / $DIMfactor * $packagingOverhead);
				if($dim > 0){
					return $dim;
				}else
					return 0;
			}catch(Exception $e) {
				return 0;
			}
		}else {
			return 0; //use the larger of this or actual weight
		}
	}

	public function getWidthAttribute()
	{
		if(isset($this->dimensions['width']) && $this->dimensions['width'] !== 0) {
			return floatval($this->dimensions['width']);
		}
		$scrub = CatalogScrubData::where('mpn', $this->mpn)->first();
		if ($this->mpn != '') {
			if(!$scrub) {
				$trade = CatalogTradeData::where('mpn', $this->mpn)->first();
			} else {
				if($scrub->dimensions != '') {
					return floatval($scrub->dimensions['width']);
				}
				$trade = CatalogTradeData::where('mpn', $this->mpn)->first();
			}
			if($trade && $trade->dimensions != '') {
				return floatval($trade->dimensions['width']);
			}
		}
		return 0;
	}

	public function getHeightAttribute()
	{
		if(isset($this->dimensions['height']) && $this->dimensions['height'] != 0) {
			return floatval($this->dimensions['height']);
		}
		if ($this->mpn != '') {
			$scrub = CatalogScrubData::where('mpn', $this->mpn)->first();
			if(!$scrub) {
				$trade = CatalogTradeData::where('mpn', $this->mpn)->first();
			} else {
				if($scrub->dimensions != '') {
					return floatval($scrub->dimensions['height']);
				}
				$trade = CatalogTradeData::where('mpn', $this->mpn)->first();
			}
			if($trade && $trade->dimensions != '') {
				return floatval($trade->dimensions['height']);
			}
		}
		return 0;
	}

	public function getLengthAttribute()
	{
		if(isset($this->dimensions['height']) && $this->dimensions['height'] != 0) {
			return floatval($this->dimensions['height']);
		}
		if ($this->mpn != '') {
			$scrub = CatalogScrubData::where('mpn', $this->mpn)->first();
			if(!$scrub) {
				$trade = CatalogTradeData::where('mpn', $this->mpn)->first();
			} else {
				if($scrub->dimensions != '') {
					return floatval($scrub->dimensions['height']);
				}
				$trade = CatalogTradeData::where('mpn', $this->mpn)->first();
			}
			if($trade && $trade->dimensions != '') {
				return floatval($trade->dimensions['height']);
			}
		}
		return 0;
	}

	public function getWeightAttribute()
	{
		try {
			try {
				$override = CatalogUOMOverride::where('productId', $this->productId)->first();

				if(!isset($this->dimensions['weight']) && !isset($override->weightQty)) {
					throw new Exception("No valid weight found");
				}
				// Use weight override if available
				$weight = $this->dimensions['weight'];
				if (isset($override->weightQty)) {
					$weight = (float)$override->weightQty;
				}

				if(!is_numeric($weight)) {
					throw new Exception("Weight is not numeric");
				} elseif(is_nan($weight)) {
					throw new Exception("Weight is NaN");
				} elseif(is_object($weight)) { //to handle the WAMP (and linux php < 5.6) issues with 64-bit numbers
					$weight = intval($this->dimensions['weight']->__toString());
				} else {
					floatval($weight);
				}

				if($weight <= 0) {
					throw new Exception("Weight zero or less");
				}

				$uomQty = $override->uomQty;

				if($uomQty < 1) {
					throw new Exception("uomQty is less than one");
				}

				return $weight * $uomQty;
			}
			catch(Exception $e) {
				//$weight = 0; no return here because we'll try scrubdata and tradedata
			}

			$scrub = CatalogScrubData::where(['mpn'=>$this->mpn]);
			if($scrub->valid()) {
				return floatval($scrub->dimensions['weight']);
			}

			$trade = CatalogTradeData::where(['mpn'=>$this->mpn]);
			if($trade->valid()) {
				return floatval($trade->dimensions['weight']);
			}

			if($weight > 0){
				return floatval($weight);
			} else {
				throw new Exception("No valid weight found");
			}
		}
		catch(Exception $e) {
			return 0;
		}
	}

	public function getAvailableAttributesAttribute()
	{
		$ids = [];
		$value_ids = [];
		foreach ($this->facets as $key => $value) {
			if (trim($value['attributeId']) != '') {
				$ids[] = $value['attributeId'];
				$value_ids[$value['attributeId']] = $value['valueId'];
			}
		}

		$rows = CatalogAttributes::whereIn('_id', $ids)->orderBy('sortOrder', 'ASC')->get();

		$attributes = [];
		foreach($rows as $row) {
			$attributes[$row->_id]['name'] = $row->name;
			foreach($row->values as $value) {
				if($value['id'] == $value_ids[$row->_id]) {
					$attributes[$row->_id]['value'] = $value['name'];
				}
			}
		}
		ksort($attributes);

		return $attributes;
	}

	public function getBreadcrumbsAttribute()
	{
		$breadcrumb = '';

		$row1 = CMSMenus::wherePath($this->meta['slug'])->first();
		if (!is_null($row1) && $row1->parent != 0) {
			$breadcrumb = '<li class="disabled">'.$row1->name.'</li>';

			$row2 = CMSMenus::where('menuId', $row1->parent)->first();
			if (!is_null($row2) && $row2->parent != 0) {
				$breadcrumb = '<li><a href="'.route('psProductPage', $row2->path).'">'.$row2->name.'</a></li>' . $breadcrumb;

				$row3 = CMSMenus::where('menuId', $row2->parent)->first();
				if (!is_null($row3) && $row3->parent != 0) {
					$breadcrumb = '<li><a href="'.route('psProductPage', $row3->path).'">'.$row3->name.'</a></li>' . $breadcrumb;

					$row4 = CMSMenus::where('menuId', $row3->parent)->first();
					if (!is_null($row4) && $row4->parent != 0) {
						$breadcrumb = '<li><a href="'.route('psProductPage', $row4->path).'">'.$row4->name.'</a></li>' . $breadcrumb;

						$row5 = CMSMenus::where('menuId', $row4->parent)->first();
						if (!is_null($row5) && $row5->parent != 0) {
							$breadcrumb = '<li><a href="'.route('psProductPage', $row5->path).'">'.$row5->name.'</a></li>' . $breadcrumb;
						}
					}
				}
			}
		}
		return $breadcrumb;
	}
}
