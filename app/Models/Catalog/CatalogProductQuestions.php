<?php

namespace App\Models\Catalog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CatalogProductQuestions extends Eloquent
{
    protected $connection= 'mongodb';
	protected $database = 'typhoeus';
	protected $collection = 'product_questions';
	protected $guarded = [];
	public $incrementing = false;
	public $timestamps = true;
}
