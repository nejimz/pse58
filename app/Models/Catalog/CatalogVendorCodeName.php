<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogVendorCodeName extends Model
{
    protected $connection= 'mysql';
	protected $database = 'typhoeus';
	protected $table = 'vendor_codename';
	protected $guarded = [];
	public $timestamps = false;
}