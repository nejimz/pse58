<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Model;

class UserAddressBook extends Model
{
	protected $database = 'typhoeus';
	protected $table = 'user_address_book';
	protected $guarded = [];

	public $timestamps = true;
}
