<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Model;

class AddressBook extends Model
{
	protected $database = 'typhoeus';
	protected $table = 'address_book';
	protected $guarded = [];

	public $timestamps = true;

	/*public function setNicknameAttribute($value = null)
	{
		$this->attributes['nickname'] = is_null($value) ? '' : $value;
	}

	public function setFirstNameAttribute($value = null)
	{
		$this->attributes['first_name'] = is_null($value) ? '' : $value;
	}

	public function setLastNameAttribute($value = null)
	{
		$this->attributes['last_name'] = is_null($value) ? '' : $value;
	}*/

	public function getHeaderNameAttribute()
	{
		$nickname = $this->nickname;

		if ($nickname != '') {
			return $nickname . ' - Address';
		}
		return 'Shipping Address';
	}
}
