const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
 
//js
mix.scripts([
	'node_modules/jquery/dist/jquery.min.js',
	'resources/js/foundation.min.js',
	'resources/js/what-input.js',
	'resources/js/lazy-load.js',
	'node_modules/jquery-zoom/jquery.zoom.min.js',
	'node_modules/inputmask/dist/jquery.inputmask.min.js',
	//'node_modules/inputmask/dist/jquery.maskMoney.min.js',
	'resources/js/app-custom.js'
	], 'public/js/app.js');

// CSS
mix.styles([
	'resources/css/foundation.min.css',
	'resources/css/progress-indicator.css',
	'node_modules/font-awesome/css/font-awesome.min.css'
	], 'public/css/app.css');

//SASS
mix.sass('resources/sass/app-custom.scss', 'public/css/app-custom.css');

// Copy files
mix.copy('node_modules/font-awesome/fonts', 'public/fonts');