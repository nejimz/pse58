@extends('layouts.master')

@section('page-title', 'Login')

@section('content')

<div class="callout register-wrapper">
	<div class="grid-x">
		<div class="cell large-7">
			<form method="post" action="{{ route('psUserLoginAuthenticate') }}">
				<h4 class="text-center margin-top-custom-40">Login</h4>
				<div class="grid-container">
					<div class="grid-x grid-padding-x">
						<div class="large-3 medium-3 small-12 cell text-right">
							<label for="email" class="text-right middle"><strong>Email:</strong></label>
						</div>
						<div class="large-7 medium-7 small-12 cell">
							<input type="email" class="radius" name="email" id="email" autocomplete="off" value="" required="" />
							@if($errors->first('email'))
							<p class="alert-first text-center">{{ $errors->first('email') }}</p>
							@endif
						</div>
						<div class="cell auto"></div>
					</div>

					<div class="grid-x grid-padding-x">
						<div class="large-3 medium-3 small-12 cell text-right">
							<label for="password" class="text-right middle"><strong>Password:</strong></label>
						</div>
						<div class="large-7 medium-7 small-12 cell">
							<input type="password" class="radius" name="password" id="password" autocomplete="off" value="" required="" />
							@if($errors->has('password'))
							<p class="text-center alert-first">{{ $errors->first('password') }}</p>
							@elseif(session('register_success'))
							<p class="text-center success-first">{{ session('register_success') }}</p>
							@endif
						</div>
						<div class="cell auto"></div>
					</div>

					<div class="grid-x grid-padding-x">
						<div class="large-3 medium-3 small-12 cell text-right">&nbsp;</div>
						<div class="large-7 cell text-center">
							<div>
								<label><input type="checkbox" name="remember_me"> Remember Me</label>
							</div>
							@csrf()
							<button class="button radius success2" type="submit">Login</button>
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>

					<div class="grid-x grid-padding-x">
						<div class="large-3 medium-3 small-12 cell text-right">
							<label class="text-right middle"><strong>OR:</strong></label>
						</div>
						<div class="large-7 cell text-center">
							<a href="{{ url('login/google') }}"><img src="{{ asset('images/oauth/google.png') }}" width="170px"></a>
							<a href="{{ url('login/facebook') }}" class=""><img src="{{ asset('images/oauth/facebook.png') }}" width="170px"></a>
							<br><br><br><br>
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>
				</div>

			</form>
		</div>
		<div class="cell large-5 text-center">
			<div class="grid-x cell-right text-center margin-top-custom-50">
				<div class="cell large-12 medium-12 small-12">
					<h6 class="margin-top-custom-50">New To PlumbersStock?</h6>
					<a href="{{ route('psUserRegistration') }}" class="button radius margin-top-custom-10">Create Account</a><br />
					Forgot You Password?<br />
					Click <a href="{{ route('psResetPassword') }}">Here</a>
				</div>
			</div>

			<!-- <div class="grid-y" style="height: 100%;">
				<div class="cell auto">&nbsp;</div>
				<div class="cell large-5">
					New To PlumbersStock?<br />
					<a href="{{ route('psUserRegistration') }}" class="button radius margin-top-custom-10">Create Account</a><br />
					Forgot You Password?<br />
					Click <a href="{{ route('psResetPassword') }}">Here</a>
				</div>
				<div class="cell auto">&nbsp;</div>
			</div> -->

		</div>
	</div>
</div>

@endsection