@extends('layouts.master')

@section('page-title', 'Sign Up')

@section('content')

<h4>Create an Account</h4>
<div class="callout register-wrapper">
	<div class="grid-x">
		<div class="cell large-7 medium-7 small-12 cell-left">
			<h5 class="text-center margin-top-custom-50">With Credentials</h5>
			<form method="post" action="{{ route('psUserRegistrationPost') }}">
				<div class="grid-container">
					<div class="grid-x grid-padding-x">
						<div class="large-4 medium-4 small-12 cell text-right">
							<label for="email" class="text-right middle"><strong class="ps-blue">Email Address</strong></label>
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<input type="email" class="radius" name="email" id="email" autocomplete="off" value="" required="" />
							@if($errors->first('email'))
							<p class="alert-first text-center">{{ $errors->first('email') }}</p>
							@endif
						</div>
						<div class="cell auto"></div>
					</div>
					<div class="grid-x grid-padding-x">
						<div class="large-4 medium-4 small-12 cell text-right">
							<label for="confirm_email" class="text-right middle"><strong class="ps-blue">Confirm Email Address</strong></label>
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<input type="email" class="radius" name="confirm_email" id="confirm_email" autocomplete="off" value="" required="" />
						</div>
						<div class="cell auto"></div>
					</div>
					<div class="grid-x grid-padding-x">
						<div class="large-4 medium-4 small-12 cell text-right">
							<label for="password" class="text-right middle"><strong class="ps-blue">Password</strong></label>
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<input type="password" class="radius" name="password" id="password" autocomplete="off" value="" required="" />
							@if($errors->first('password'))
							<p class="alert-first text-center">{{ $errors->first('password') }}</p>
							@endif
						</div>
						<div class="cell auto"></div>
					</div>
					<div class="grid-x grid-padding-x">
						<div class="large-4 medium-4 small-12 cell text-right">
							<label for="confirm_password" class="text-right middle"><strong class="ps-blue">Confirm Password</strong></label>
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<input type="password" class="radius" name="confirm_password" id="confirm_password" autocomplete="off" value="" required="" />
						</div>
						<div class="cell auto"></div>
					</div>

					<div class="grid-x">
						<div class="cell large-4"></div>
						<div class="large-6 medium-6 small-12 cell text-center">
							@csrf()
							<button class="button success2 radius" type="submit">Create Account</button>
							<p>Already have an account? <a href="{{ route('psUserLogin') }}">Login here</a></p>
						</div>
						<div class="cell large-2"></div>
					</div>
				</div>
			</form>
		</div>
		<div class="cell large-5 medium-5 small-12">
			<div class="grid-x cell-right text-center margin-top-custom-50">
				<div class="cell large-12 medium-12 small-12">
					<h5 class="">With Provider</h5>
					<p class="margin-top-custom-40">
						<a href="{{ url('login/google') }}"><img src="{{ asset('images/oauth/google.png') }}" width="210px"></a>
					</p>
					<p class="margin-top-custom-20">
						<a href="{{ url('login/facebook') }}" class=""><img src="{{ asset('images/oauth/facebook.png') }}" width="210px"></a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection