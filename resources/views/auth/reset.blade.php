@extends('layouts.master')

@section('page-title', 'Reset Password')

@section('content')

<h3 class="text-center">Reset Your Password</h3>

<form action="{{ route('psResetPasswordPost') }}" method="post">
	
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="auto cell"></div>
			<div class="large-8 medium-8 small-12 cell">
				<label><strong>Email Address</strong>
					<input type="email" class="radius" name="email" autocomplete="off" value="" placeholder="name@example.com" />
				</label>
			</div>
			<div class="auto cell"></div>
		</div>
		<div class="grid-x grid-padding-x">
			<div class="large-12 medium-12 small-12 cell text-center">
				@csrf()
				<button class="button radius success2" type="submit">Reset Password</button>
			</div>
		</div>
	</div>

</form>

@endsection