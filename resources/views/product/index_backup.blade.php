@extends('layouts.master')

@section('page-title', 'Plumbing Supply Online, HVAC, Sprinklers, & Tools')

@section('content')
<?php
$layoutType = $category->layout_type;

$sub_categories = $category->sub_categories->get();
$additional_sub_categories = $category->additional_sub_categories;

$sub_categories_count = $sub_categories->count();
$additional_sub_categories_count = $additional_sub_categories->count();

$has_promotional_images = $category->has_promotional_images;
$promotional_ad_type = $category->promotional_ad_type($sub_categories_count, $additional_sub_categories_count);
?>
<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	{!! $category->breadcrumbs !!}
</ul>
<h4>{{ $category->name }}</h4>
@if($layoutType != 6)
	@if($layoutType == 2 && $layoutType != 4 && $layoutType != 7)
	<div class="grid-x">
		@foreach($sub_categories as $item)
		<div class="cell large-4 medium-4 small-6 margin-top-custom-10">
			<a href="{{ $item->meta['slug'] }}"><img width="300px" src="//images2.plumbersstock.com/lifestyle/300/300/{{ $item->layout['images']['lifestyle'] }}" alt="{{ $item->name }}"></a>
			<a href="{{ $item->meta['slug'] }}"><h6>All {{ $item->name }}</h6></a>
			@foreach($item->sub_links as $sublink)
				<a href="{{ $sublink->meta['slug'] }}">- {{ $sublink->name }}</a><br>
			@endforeach
		</div>
		@endforeach

		@foreach($additional_sub_categories as $item)
		<div class="cell large-4 medium-4 small-6 margin-top-custom-20">
			<a href="{{ $item->meta['slug'] }}"><img width="300px" src="//images2.plumbersstock.com/lifestyle/300/300/{{ $item->layout['images']['lifestyle'] }}" alt="{{ $item->name }}"></a>
			<a href="{{ $item->meta['slug'] }}"><h6>All {{ $item->name }}</h6></a>
			@foreach($item->sub_links as $sublink)
				<a href="{{ $sublink->meta['slug'] }}">- {{ $sublink->name }}</a><br>
			@endforeach
		</div>
		@endforeach

		@if($has_promotional_images)
			@if($promotional_ad_type == 0 && $category->has_promotional_ad('fullwidth'))
			<div class="cell large-12">
				<a href="#"><img src="{{ $category->has_promotional_ad('fullwidth')['img'] }}"></a>
			</div>
			@elseif($promotional_ad_type == 1 && $category->has_promotional_ad('2slots'))
			<div class="cell large-12">
				<a href="#"><img src="{{ $category->has_promotional_ad('2slots')['img'] }}"></a>
			</div>
			@elseif($promotional_ad_type == 2 && $category->has_promotional_ad('1slot'))
			<div class="cell large-12">
				<a href="#"><img src="{{ $category->has_promotional_ad('1slot')['img'] }}"></a>
			</div>
			@endif
		@endif
	</div>
	@elseif($layoutType == 1 && $layoutType != 4 && $layoutType != 7)
	<h1>Layout 1 and Not 4 & 7</h1>
	<div class="grid-x">
	</div>
	@endif
@endif
@if(!in_array($layoutType, [2, 4, 7]))
	<div class="grid-x">
	@if($category->sub_categories->count() > 0)
		@if($layoutType == 6)
			@if(isset($this->snippetContent))
				{!! $category->translate_snippet !!}
			@endif
		@else
			@foreach($sub_categories as $item)
				@if($item->name === "Sprinkler Design Center")
					<div class="cell large-3">
						<a href="/free-sprinkler-design.html">
							<img src="{{ asset('img/sprinkler_thumbnail.jpg') }}" />
							<p>{{ $item->name }}</p>
						</a>
					</div>
				@else
					<div class="cell large-3">
						<a href="{{ url($item->meta['slug']) }}">
							<img src="//images1.plumbersstock.com/162/162/marketing/{{ $item->image }}" />
							<p>{{ $item->name }}</p>
						</a>
					</div>
				@endif
			@endforeach

			@if($category->has_sub_categories_sub_links > 0)
				@foreach($category->sub_categories_sub_links->get() as $link)
					<div class="cell large-3">
						<a href="{{ url($link->path) }}">
							<img src="//images1.plumbersstock.com/162/162/marketing/{{ $item->image }}" />
							<p>{{ $link->name }}</p>
						</a>
					</div>
				@endforeach
			@endif
		@endif
	@else
	@endif

	@if(!in_array($layoutType, [3, 4, 5, 6, 7]))
		<!-- Get Data to Elastic Search -->
	@endif
	</div>
@endif

<div class="grid-x margin-top-custom-40">
	<div class="cell auto"></div>
	<div class="cell large-10">{!! $category->content !!}</div>
	<div class="cell auto"></div>
</div>

@endsection