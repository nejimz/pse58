@extends('layouts.master')

@section('page-title', 'Plumbing Supply Online, HVAC, Sprinklers, & Tools')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	{!! $category->breadcrumbs !!}
</ul>

<h4>{{ $category->name }} 2</h4>

<div class="grid-x">
	@foreach($sub_categories as $item)
	<div class="cell large-4 medium-4 small-6 margin-top-custom-10">
		<a href="{{ $item->meta['slug'] }}"><img width="300px" src="//images2.plumbersstock.com/lifestyle/300/300/{{ $item->layout['images']['lifestyle'] }}" alt="{{ $item->name }}"></a>
		<a href="{{ $item->meta['slug'] }}"><h6>All {{ $item->name }}</h6></a>
		@foreach($item->sub_links as $sublink)
			<a href="{{ $sublink->meta['slug'] }}">- {{ $sublink->name }}</a><br>
		@endforeach
	</div>
	@endforeach

	@foreach($additional_sub_categories as $item)
	<div class="cell large-4 medium-4 small-6 margin-top-custom-20">
		<a href="{{ $item->meta['slug'] }}"><img width="300px" src="//images2.plumbersstock.com/lifestyle/300/300/{{ $item->layout['images']['lifestyle'] }}" alt="{{ $item->name }}"></a>
		<a href="{{ $item->meta['slug'] }}"><h6>All {{ $item->name }}</h6></a>
		@foreach($item->sub_links as $sublink)
			<a href="{{ $sublink->meta['slug'] }}">- {{ $sublink->name }}</a><br>
		@endforeach
	</div>
	@endforeach

	@if($has_promotional_images)
		@if($promotional_ad_type == 0 && $category->has_promotional_ad('fullwidth'))
		<div class="cell large-12">
			<a href="#"><img src="{{ $category->has_promotional_ad('fullwidth')['img'] }}"></a>
		</div>
		@elseif($promotional_ad_type == 1 && $category->has_promotional_ad('2slots'))
		<div class="cell large-12">
			<a href="#"><img src="{{ $category->has_promotional_ad('2slots')['img'] }}"></a>
		</div>
		@elseif($promotional_ad_type == 2 && $category->has_promotional_ad('1slot'))
		<div class="cell large-12">
			<a href="#"><img src="{{ $category->has_promotional_ad('1slot')['img'] }}"></a>
		</div>
		@endif
	@endif
</div>

<div class="grid-x margin-top-custom-40">
	<div class="cell auto"></div>
	<div class="cell large-10">{!! $category->content !!}</div>
	<div class="cell auto"></div>
</div>

@endsection