@extends('layouts.master')

@section('page-title', $product->title)

@section('content')

<style type="text/css">
</style>

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	{!! $product->breadcrumbs !!}
</ul>

<div class="grid-x ps-product-border-top">
	<div id="ps-product-img-thumbnail" class="cell large-1 ps-img-thumbnail">
		<?php $first_image = ''; ?>
		@foreach($product->images as $image)
			@if($first_image == '')
			<?php $first_image = $image['id'] ?? 'default.jpg'; ?>
			@endif
			<div class="ps-product-border-right">
				<img  src="//images3.plumbersstock.com/750/750/{{ $image['id'] ?? 'default.jpg' }}" width="100px">
			</div>
		@endforeach
	</div>
	<div class="cell large-5">
		<div id="ps-zoom" class="zoom ps-product-img-container">
			<img src="//images3.plumbersstock.com/750/750/alternative/{{ $first_image }}" width="750px">
		</div>
	</div>
	<div class="cell auto ps-product-padding-top">
		<p class="text-right">
			<a href="#"><i class="fa fa-print fa-lg"></i> Printer friendly page</a>
		</p>
		<h4 class="ps-h-weight">{{ $product->title }}</h4>

		<p>SKU: {{ $product->productId }}</p>
		<div class="product-rating">
			<?php $rating = $product->ratings(false); ?>
			@if($rating > 0)
				<span class="stars">
				@for ($i = 0; $i < floor($rating); $i++)
					<i class="fa fa-star"></i>
				@endfor

				@if (fmod($rating, 1.0) > 0)
					<i class="fa fa-star-half-o"></i>
				@endif

				@for ($i = ceil($rating); $i < 5; $i++)
					<i class="fa fa-star-o"></i>
				@endfor
				</span>
			@endif
		</div>

		<h5 class="ps-h-weight">{{ config('site.currency.USD.sign') . '' . number_format($product_price, 2) . '/' . $product->uom }}</h5>
		<h6 class="ps-h-weight ps-product-discount">
			<span class="span1">Save {{ $product->discount() }}%</span>
			.<span class="span2">Retail {{ $currency . '' . number_format($product->pricing['listPrice'], 2) }}</span>
		</h6>
		<form action="{{ route('psCartStore') }}" method="post">
			<div class="grid-x">
				<div class="cell large-3">
					@csrf()
					<input type="hidden" name="price" value="{{ $product_price }}" />
					<input type="hidden" name="warehouse" value="plumbersstock" />
					<input type="hidden" name="productId" value="{{ $product->productId }}" />
					<button class="button expanded radius ps-h-weight" type="submit">Add To Cart</button>
				</div>
				<div class="cell large-1">&nbsp;</div>
				<div class="cell large-2">
					<div class="grid-x">
						<div class="cell auto">
							<input class="input-group-field text-center radius-left" name="quantity" id="quantity" type="text" value="1" onkeyup="increment_decrement('quantity', 0)">
						</div>
						<div class="cell large-5">
							<button type="button" class="button expanded radius-top-right input-number-top-btn" onclick="increment_decrement('quantity', 0, 'inc')"><strong>+</strong></button>
							<button type="button" class="button expanded radius-bottom-right input-number-bottom-btn" onclick="increment_decrement('quantity', 0, 'dec')"><strong>-</strong></button>
						</div>
					</div>
				</div>
				<div class="cell auto increment_decrement_validation_msg"></div>
			</div>
		</form>
		<p id="buying-options-container">
			<a href="javascript:void(0)" onclick="toggle_buying_options()"><i class="fa fa-plus"></i> More Buying Options</a>
			<div id="buying-options" class="callout radius" style="display: none;">
				<div class="grid-x">
					<div class="cell large-4"><h6 class="ps-h-weight">Ships From</h6></div>
					<div class="cell large-2 text-right"><h6 class="ps-h-weight">Price</h6></div>
					<div class="cell large-2 text-center"><h6 class="ps-h-weight">Qty</h6></div>
					<div class="cell large-4"></div>
				</div>
				@foreach($product->inventory['availability'] as $key => $value)
					@if($value['qty'] > 0)
						<form action="{{ route('psCartStore') }}" method="post">
							<div class="grid-x buying-options-row">
								<div class="cell large-4 buying-options-warehouse">
									<small>{{ $product->vendor_name_2($key) }} Warehouse<br />
										<a href="javascript:void(0)"><i class="fa fa-info-circle"></i> {{ $product->lead_time($key) }} - {{ $key }}</a>
									</small>
								</div>
								<div class="cell large-2 text-right"><small>{{ $currency . '' . number_format($product_price, 2) }}</small></div>
								<div class="cell large-2 text-center"><small>{{ (($key == 'plumbersstock')? '∞' : $value['qty']) }}</small></div>
								<div class="cell large-4">
									<div class="grid-x">
										<div class="cell large-6">
											<div class="grid-x">
												<div class="cell auto">
													<input class="input-group-field text-center radius-left bo_quantity" name="quantity" id="bo_quantity_{{ $key }}" type="text" value="1" onkeyup="increment_decrement('bo_quantity_{{ $key }}', {{ $value['qty'] }})" />
												</div>
												<div class="cell large-5">
													<button type="button" class="button expanded radius-top-right input-number-top-btn" onclick="increment_decrement('bo_quantity_{{ $key }}', {{ $value['qty'] }}, 'inc')"><strong>+</strong></button>
													<button type="button" class="button expanded radius-bottom-right input-number-bottom-btn" onclick="increment_decrement('bo_quantity_{{ $key }}', {{ $value['qty'] }}, 'dec')"><strong>-</strong></button>
												</div>
											</div>
										</div>
										<div class="cell large-6 text-right">
											<input type="hidden" name="price" value="{{ $product_price }}" />
											<input type="hidden" name="warehouse" value="{{ $key }}" />
											<input type="hidden" name="productId" value="{{ $product->productId }}" />
											@csrf()
											<button type="submit" class="button small radius">Add To Cart</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<?php $product_inventory_availability++; ?>
					@endif
				@endforeach
			</div>
		</p>
	</div>
</div>
<div class="grid-x grid-padding-x ps-product-border-top ps-product-padding-top">
	@if($product->description != '')
	<div class="cell large-7">
		<h4>
			@if($product->brand != '' && $product->mpn != '')
				{{ $product->brand . ' ' . $product->mpn }}
			@else
				{{ $product->title }}
			@endif
		</h4>
		{!! $product->description !!}
	</div>
	@endif

	<div class="cell large-5">
		<h4 class="ps-h-weight">Product Details</h4>
		<table>
			@foreach($product->available_attributes as $row)
				@if($row['name'] != 'Item_type')
				<tr>
					<td width="50%" class="text-right">{{ $row['name'] }}:</td>
					<td width="50%">{{ $row['value'] ?? '-' }}</td>
				</tr>
				@endif
			@endforeach
			@if($product->mpn != '')
			<tr>
				<td class="text-right">Mpn #:</td>
				<td>{{ $product->mpn }}</td>
			</tr>
			@endif
			@if(count($product->dimensions) > 0 && floatval($product->dimensions['length']) > 0 && floatval($product->dimensions['width']) > 00 && floatval($product->dimensions['height']) > 00)
			<tr>
				<td class="text-right">Shipping Dimensions:</td>
				<td>
					{{ number_format(floatval($product->dimensions['length']), 2) }}" x
					{{ number_format(floatval($product->dimensions['width']), 2) }}" x
					{{ number_format(floatval($product->dimensions['height']), 2) }}"
				</td>
			</tr>
			@endif
			@if($product->dimensions['weight'] > 0)
			<tr>
				<td class="text-right">Weight:</td>
				<td>{{ number_format(floatval($product->dimensions['weight']), 2) }} lbs.</td>
			</tr>
			@endif
			@if($product->dimensions['weight'] > 0)
			<tr>
				<td class="text-right">Shipping Dimensional Weight:</td>
				<td>{{ number_format(floatval($product->dimensions['weight']), 2) }} lbs.</td>
			</tr>
			@endif
		</table>
	</div>
</div>

@if(count($product->features) > 0)
<div class="grid-x ps-product-border-top ps-product-padding-top margin-top-custom-20">
	<div class="cell large-12">
		<h4 class="ps-h-weight">Product Features</h4>
		<ul class="ps-product-features">
		@foreach($product->features as $feature)
			<li>{{ $feature }}</li>
		@endforeach
		</ul>
	</div>
</div>
@endif

<div class="grid-x ps-product-border-top ps-product-padding-top">
	<div class="cell large-12">
		<h4 class="ps-h-weight">Resources</h4>
		<ul class="ps-product-features">
		@foreach($product->resources as $resource)

			<li><a href="{{ route('psProductPage', $resource['id']) }}">{{ $resource['name'] }}</a></li>
		@endforeach
		</ul>
	</div>
</div>

<div class="grid-x ps-product-border-top ps-product-padding-top">
	<div class="cell large-12">
		<h4 class="ps-h-weight">Reviews</h4>
		<button class="button radius" data-open="reviewModalForm">Submit a product review</button>
		<div class="reveal radius" id="reviewModalForm" data-reveal>
			<h4>Please enter your review here</h4>
			<hr>
			@include('partials.error-messages')
			<form id="product-review-form" action="{{ route('psProductReviewStore') }}" method="post">
				<div class="grid-container">
					<div class="grid-x">
						<div class="medium-12 cell">
							<label>Name: <input type="text" class="radius" name="name" value=""></label>
						</div>
						<div class="medium-12 cell">
							<label>Rating:</label>
							<span class="star-cb-group">
								<input type="radio" class="star-radio" id="rating_5" name="rating" value="5"><label class="star-label" for="rating-5" data-val="5">5</label>
								<input type="radio" class="star-radio" id="rating_4" name="rating" value="4"><label class="star-label" for="rating-4" data-val="4">4</label>
								<input type="radio" class="star-radio" id="rating_3" name="rating" value="3"><label class="star-label" for="rating-3" data-val="3">3</label>
								<input type="radio" class="star-radio" id="rating_2" name="rating" value="2"><label class="star-label" for="rating-2" data-val="2">2</label>
								<input type="radio" class="star-radio" id="rating_1" name="rating" value="1"><label class="star-label" for="rating-1" data-val="1">1</label>
							</span>
						</div>
						<div class="medium-12 cell">
							<label>Review Title: <input type="text" class="radius" name="title" value=""></label>
						</div>
						<div class="medium-12 cell">
							<label>Review: <textarea name="review" rows="3" class="radius"></textarea></label>
						</div>
						<div class="medium-12 cell">
							<label>Add photo of the product to your review: <input type="file" name="photo" value=""></label>
						</div>
						<div class="medium-12 cell">
							<legend>Do you recommend this product?:</legend>
							<input type="radio" id="recommend_yes" name="recommend" value="1">
							<label for="recommend_yes">Yes</label> 
							<input type="radio" id="recommend_no" name="recommend" value="0">
							<label for="recommend_no">No</label> 
						</div>
						<div class="medium-12 cell">
							@csrf()
							<button type="submit" class="button radius">Submit</button>
						</div>
					</div>
				</div>
			</form>
			<button class="close-button" data-close aria-label="Close modal" type="button">
				<span aria-hidden="true">&times;</span>
			</button>
			<img id="product-review-form-loading" src="{{ asset('img/loading-snake.gif') }}" width="40px" title="Loading" style="display: none;" />
		</div>
	</div>
</div>

@if(count($product->products['related']) > 0)
<div class="grid-x grid-margin-x ps-product-border-top ps-product-padding-top">
	<div class="cell large-12">
		<h4 class="ps-h-weight">Related Products</h4>
	</div>
	@foreach($product->products['related'] as $id)
		<?php
		$item = App\Models\Catalog\CatalogProduct::where('productId', intval($id))->first();
		?>
		<div class="cell large-3 text-center">
			<div class="grid-x grid-margin-x">
				<div class="cell large-12">
					<a href="{{ route('psProductPage', $item->meta['slug']) }}"><img src="//images1.plumbersstock.com/245/245/{{ $item->images[0]['name'] ?? 'default.jpg' }}"></a>
				</div>
				<div class="cell large-12 ps-fixed-height-100">
					<h6><a href="{{ route('psProductPage', $item->meta['slug']) }}">{{ $item->title }}</a></h6>
				</div>
				<div class="cell large-12">
					<p>
						{{ $currency . '' .number_format($item->inventory['availability']['plumbersstock']['price'], 2) }}
					</p>
					<a href="{{ route('psProductPage', $item->meta['slug']) }}" class="button radius">Details</a>
				</div>
			</div>
		</div>
	@endforeach
</div>
@endif

@if(count($product->products['recommended']) > 0)
<div class="grid-x grid-margin-x ps-product-border-top ps-product-padding-top">
	<div class="cell large-12">
		<h4 class="ps-h-weight">Recommended Products</h4>
	</div>
	<?php
	$product_recommended_counter = 1;
	$product_recommended = $product->products['recommended'];
	?>
	@foreach($product_recommended as $id)
		<?php
		$item = App\Models\Catalog\CatalogProduct::where('productId', intval($id))->first();
		if ($product_recommended_counter > 4) {
			break;
		}
		$product_recommended_counter++;
		?>
		@if(!is_null($item))
		<div class="cell large-3 text-center">
			<div class="grid-x grid-margin-x">
				<div class="cell large-12">
					<a href="{{ route('psProductPage', $item->meta['slug']) }}"><img src="//images1.plumbersstock.com/245/245/{{ $item->images[0]['name'] ?? 'default.jpg' }}"></a>
				</div>
				<div class="cell large-12 ps-fixed-height-100">
					<h6><a href="{{ route('psProductPage', $item->meta['slug']) }}">{{ $item->title }}</a></h6>
				</div>
				<div class="cell large-12">
					<p>
						{{ $currency . '' .number_format($item->inventory['availability']['plumbersstock']['price'], 2) }}
					</p>
					<a href="{{ route('psProductPage', $item->meta['slug']) }}" class="button radius">Details</a>
				</div>
			</div>
		</div>
		@endif
	@endforeach
</div>
@endif

@if(count($product->products['parts']) > 0)
<div class="grid-x grid-margin-x ps-product-border-top ps-product-padding-top">
	<div class="cell large-12">
		<h4 class="ps-h-weight">Product Parts</h4>
	</div>
	<?php
	$product_parts_counter = 1;
	?>
	@foreach($product->products['parts'] as $id)
		<?php
		$item = App\Models\Catalog\CatalogProduct::where('productId', intval($id))->first();
		if ($product_parts_counter > 4) {
			break;
		}
		$product_parts_counter++;
		?>
		<div class="cell large-3 text-center">
			<div class="grid-x grid-margin-x">
				<div class="cell large-12">
					<a href="{{ route('psProductPage', $item->meta['slug']) }}">
						<img src="//images1.plumbersstock.com/245/245/{{ $item->images[0]['name'] ?? 'default.jpg' }}">
					</a>
				</div>
				<div class="cell large-12 ps-fixed-height-100">
					<h6><a href="{{ route('psProductPage', $item->meta['slug']) }}">{{ $item->title }}</a></h6>
				</div>
				<div class="cell large-12">
					<p>
					{{ $currency . '' .number_format($item->inventory['availability']['plumbersstock']['price'], 2) }}
					</p>
					<a href="{{ route('psProductPage', $item->meta['slug']) }}" class="button radius">Details</a>
				</div>
			</div>
		</div>
	@endforeach
</div>
@endif

<div class="grid-x grid-margin-x ps-product-padding-top margin-top-custom-40">
	<div class="cell large-12">
		<h4 class="ps-h-weight">Questions & Answers</h4>
		<div class="callout secondary radius">
			<form id="questions-and-answer" action="{{ route('questions-and-answer.store') }}" method="post">
				<div class="grid-container">
					<div class="cell large-12">
						<div class="question-validation"></div>
						<label><strong>Have a question about this product?</strong>
							<input type="hidden" name="productId" value="{{ $product->productId}}">
					    	<textarea id="question" name="question" class="radius" placeholder="Type your question here.." rows="2"></textarea>
					    </label>
					</div>
					<div class="cell large-12">
						@csrf()
			    		<button id="questions-and-answer-button" class="button radius" type="submit">Send Question</button>
			    		<img id="questions-and-answer-loading" src="{{ asset('img/loading-snake.gif') }}" width="40px" style="display: none;" />
					</div>
				</div>
			</form>
		</div>
		<div id="questions-list">
			<div class="callout"></div>
		</div>
		<div class="grid-x grid-margin-x margin-top-custom-40">
			<div class="cell auto"></div>
			<div class="cell large-2">
				<div class="input-group">
					<div class="input-group-button">
						<button id="product-paginaton-prev" class="button secondary"><i class="fa fa-chevron-left fa-lg"></i></button>
					</div>
					<span id="product-paginaton-info" class="input-group-label"></span>
					<div class="input-group-button">
						<button id="product-paginaton-next" class="button secondary"><i class="fa fa-chevron-right fa-lg"></i></button>
					</div>
				</div>
			</div>
			<div class="cell auto"></div>
		</div>
	</div>
</div>


{{ dump($errors->any()) }}
{{ var_dump(session()->all()) }}
{{ dump(session('success_msg')) }}
@endsection

@section('scripts')
<script type="text/javascript">
	var xhr_question_and_answer = null;
	var xhr_question_and_answer_store = null;
	var xhr_product_review_form_store = null;
	var xhr_submit_reply_update = null;

	<?php
	if ($product_inventory_availability < 1) {
		echo '$("#buying-options-container").html("")';
	}

	if ($errors->any() || session('success_msg')) {
		echo '$("#reviewModalForm").foundation("open");';
	}
	?>

	question_and_answer();

	$(function(){
		$('#ps-zoom').zoom({on:'click'});
		$('#ps-product-img-thumbnail img').hover(function(){
			var new_src = $(this).attr('src');
			$('#ps-zoom img').attr('src', new_src);
		});

		$('.star-label').click(function(){
			$('.star-radio').removeAttr('checked');
			$('#rating_' + $(this).data('val')).attr('checked', 'checked');
		});

		$('#questions-and-answer').submit(function(e){
			$('#questions-and-answer-button').attr('disabled', true);
			$('#questions-and-answer-loading').show();
		    xhr_question_and_answer_store = $.ajax({
		    	type : $(this).attr('method'),
		    	url : $(this).attr('action'),
		    	cache : false,
		    	data : $(this).serialize(),
		    	dataType : "json",
		        async : false, 
		    	beforeSend: function(xhr){
		        	if (xhr_question_and_answer_store != null)
		        	{
		          		xhr_question_and_answer_store.abort();
		        	}
		    	}
		    }).done(function(response) {
				$('#questions-and-answer-button').attr('disabled', false);
				$('#questions-and-answer-loading').hide();
		    	question_and_answer();
		    	$('#question').val('');
		     	$('.question-validation').html('<div class="callout success radius">' + response['message'] + '</div>');
		    }).fail(function(jqXHR, textStatus) {
				$('#questions-and-answer-button').attr('disabled', false);
				$('#questions-and-answer-loading').hide();
		     	var errors = error_message(jqXHR['responseJSON']['errors']);
		     	$('.question-validation').html('<div class="callout warning radius">' + errors + '</div>');
		    });

			e.preventDefault();
		});

		/*$('#product-review-form').submit(function(e){
			$('#product-review-form button').attr('disabled', true);
			$('#product-review-form-loading').show();
		    xhr_product_review_form_store = $.ajax({
		    	type : $(this).attr('method'),
		    	url : $(this).attr('action'),
		    	cache : false,
		    	data : $(this).serialize(),
		    	dataType : "json",
		        async : false, 
		    	beforeSend: function(xhr){
		        	if (xhr_product_review_form_store != null)
		        	{
		          		xhr_product_review_form_store.abort();
		        	}
		    	}
		    }).done(function(response) {
				$('#product-review-form button').attr('disabled', false);
				$('#product-review-form-loading').hide();
		     	$('#review-validation').html('<div class="callout success radius">' + response['message'] + '</div>');
		    }).fail(function(jqXHR, textStatus) {
				$('#product-review-form button').attr('disabled', false);
				$('#product-review-form-loading').hide();
		     	var errors = error_message(jqXHR['responseJSON']['errors']);
		     	$('#review-validation').html('<div class="callout warning radius">' + errors + '</div>');
		    });
			e.preventDefault();
		});*/
	});

	function question_and_answer(url = '{{ route("questions-and-answer.show", $product->productId) }}')
	{
	    $('#questions-list').html('<div class="text-center"><img src="{{ asset('img/loading-bar-slant.gif') }}" /></div>');
	    xhr_question_and_answer = $.ajax({
	      type : 'get',
	      url : url,
	      cache : false,
	      dataType : "json",
	        async : false, 
	      beforeSend: function(xhr){
	        if (xhr_question_and_answer != null)
	        {
	        	xhr_question_and_answer.abort();
	        }
	      }
	    }).done(function(response) {
	    	var html_string = '';
	    	$.each(response['data'], function(key, value){
	    		var answer_count = (value['answers'] != null)? value['answers'].length : 0;
	    		html_string += '<div class="callout radius"><h6><i class="fa fa-user"></i>&nbsp;&nbsp;Anonymous</h6><p>' + value['question'] + '</p><div class="text-right"><button id="question-' + value['_id'] + '" class="dropdown button small clear" onclick="toggle(\'' + value['_id'] + '\')" style="margin:0;">Show/Hide Replies (' + answer_count + ')</button></div></div>';

	    		html_string += '<div id="answer-' + value['_id'] + '" class="grid-x grid-margin-x" style="display:none;">';

	    		if (answer_count > 0) {
		    		$.each(value['answers'], function(k, v){
		    			html_string += '<div class="cell large-2"></div><div class="cell large-10"><div class="callout primary radius"><h6><i class="fa fa-user"></i>&nbsp;&nbsp;' + v['user'] + '</h6><p>' + v['answer'] + '</p></div></div>';
		    		});
	    		}

	    		html_string += '<div class="cell large-12"><div class="callout secondary radius"><form id="form-' + value['_id'] + '" onsubmit="return submit_reply(\'' + value['_id'] + '\')" action="{{ route("questions-and-answer.update", $product->productId) }}" method="post"><div class="grid-container"><div class="cell large-12"><div class="question-reply-validation-' + value['_id'] + '"></div><label><strong>Know the answer?</strong><textarea id="reply_answer" name="reply_answer" class="radius" placeholder="Type your answer here.." rows="2"></textarea></label></div><div class="cell large-12">@method("PUT") @csrf()<button id="button-' + value['_id'] + '" class="button radius" type="submit">Submit</button><img id="form-loading-' + value['_id'] + '" src="{{ asset('img/loading-snake.gif') }}" width="40px" style="display: none;" /></div></div></form></div></div>';

	    		html_string += '</div>';
	    	});

	    	$('#questions-list').html(html_string);
	    	$('#product-paginaton-info').text(response['current_page'] + ' of ' + response['last_page']);

	    	if (response['prev_page_url'] == null) {
	    		$('#product-paginaton-prev').attr('disabled', 'disabled');
	    	}else{
	    		$('#product-paginaton-prev').attr('disabled', false).attr('onclick', 'question_and_answer(\'' + response['prev_page_url'] + '\')');
	    	}

	    	if (response['next_page_url'] == null) {
	    		$('#product-paginaton-next').attr('disabled', 'disabled');
	    	}else{
	    		$('#product-paginaton-next').attr('disabled', false).attr('onclick', 'question_and_answer(\'' + response['next_page_url'] + '\')');
	    	}
	    }).fail(function(jqXHR, textStatus) {
	      //var errors = error_message(jqXHR['responseJSON']['errors']);
	      //$('#ss_message').html('<br /><div class="notification is-warning">' + errors + '</div>');
	    });
	}

	function submit_reply(id)
	{
		$('#button-' + id).attr('disabled', true);
		$('#form-loading-' + id).show();
		
	    xhr_submit_reply_update = $.ajax({
	    	type : $('#form-' + id).attr('method'),
	    	url : $('#form-' + id).attr('action'),
	    	cache : false,
	    	data : $('#form-' + id).serialize(),
	    	dataType : "json",
	        async : false, 
	    	beforeSend: function(xhr){
	        	if (xhr_submit_reply_update != null)
	        	{
	          		xhr_submit_reply_update.abort();
	        	}
	    	}
	    }).done(function(response) {
			$('#button-' + id).attr('disabled', false);
			$('#form-loading-' + id).hide();
	    	question_and_answer();
	    	$('#reply_answer').val('');
	     	$('.question-reply-validation-' + id).html('<div class="callout success radius">' + response['message'] + '</div>');
	    }).fail(function(jqXHR, textStatus) {
			$('#button-' + id).attr('disabled', false);
			$('#form-loading-' + id).hide();
	     	var errors = error_message(jqXHR['responseJSON']['errors']);
	     	$('.question-reply-validation-' + id).html('<div class="callout warning radius">' + errors + '</div>');
	    });
	    console.log(id);
	    return false;
	}

	function toggle(id)
	{
		$('#answer-' + id).slideToggle('fast');
	}

	function toggle_buying_options()
	{
		$('#buying-options').slideToggle('fast');
	}
</script>
@endsection