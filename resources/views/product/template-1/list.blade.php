@foreach($products as $product)
	<div class="cell large-12 medium-12 small-12">
		<div class="grid-x grid-margin-x">
			<div class="cell large-4 medium-4 small-4">
				<p class="text-center">
					<a href="{{ route('psProductPage', $product->meta['slug']) }}">
						<img class="thumbnail radius" src="//images1.plumbersstock.com/245/245/{{ $product->images[0]['name'] ?? 'default.jpg' }}" width="245px" height="245px">
					</a>
				</p>
			</div>
			<div class="cell large-8 medium-8 small-8">
				<h5>{{ $product->title }}</h5>
				<div class="grid-x grid-margin-x">
					<div class="cell large-12">
						<ul class="product-feature">
						<?php $feature_counter = 0; ?>
						@foreach($product->features as $feature)
							<li>{{ $feature }}</li>
							<?php
							if ($feature_counter > 1) {
								break;
							}
							$feature_counter++; 
							?>
						@endforeach
						</ul>
						<h5>{{ $currency.' '.number_format($product->pricing['price'], 2) }}&nbsp;&nbsp;.&nbsp;&nbsp;<small>Save 40%</small></h5>
						<br>
					</div>
					<div class="cell large-12">
						<form>
							<a href="{{ route('psProductPage', $product->meta['slug']) }}" class="button primary radius">Details</a>
							<button class="button hollow radius">Add To Cart</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endforeach