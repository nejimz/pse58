@foreach($products as $product)
	<div class="cell large-4 medium-6 small-12">
		<div class="callout small radius product-list-1">
			<p class="text-center">
				<a href="{{ route('psProductPage', $product->meta['slug']) }}">
					<img src="//images1.plumbersstock.com/245/245/{{ $product->images[0]['name'] ?? 'default.jpg' }}" width="245px" height="245px">
				</a>
			</p>
			<h5 class="ps-h-weight">{{ $product->title }}</h5>
			<div class="grid-x grid-margin-x product-list-1-details-btn-bottom">
				<div class="cell large-12">
					<h5 class="ps-h-weight">
						{{ $currency.''.number_format($product->pricing['price'], 2) }}
						&nbsp;.&nbsp;
						<small class="ps-bold-lighter">Save {{ number_format(40, 2) }}%</small>
					</h5>
				</div>
				<div class="cell large-6">
					<a href="{{ route('psProductPage', $product->meta['slug']) }}" class="button primary expanded radius">Details</a>
				</div>
				<div class="cell large-6">
					<form>
						<button class="button hollow expanded radius">Add To Cart</button>
					</form>
				</div>
			</div>
		</div>
	</div>
@endforeach