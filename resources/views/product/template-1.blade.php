@extends('layouts.master')

@section('page-title', 'Plumbing Supply Online, HVAC, Sprinklers, & Tools')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	{!! $category->breadcrumbs !!}
</ul>


<div class="grid-x">
	<div class="cell large-3">
		<h5 class="ps-h-weight">Find it <i class="support-label">Fast!</i></h5>

		@foreach($category_attributes as $attribute)
			{{ $attribute->_id }}<br />
		@endforeach

	</div>

	<div class="cell large-9">
		<h4 class="ps-h-weight">{{ $category->name }}</h4>
		<div class="grid-x margin-top-custom-20">
			<div class="cell large-6 medium-6 small-6">
				<form>
					<div class="grid-x">
						<div class="cell small-3 medium-2 large-2">
							<label class="middle text-right">Show&nbsp;&nbsp;&nbsp;</label>
						</div>
						<div class="cell small-4 medium-3 large-3">
							<select class="radius" name="show" id="show">
								<option value="24">24</option>
								<option value="48">48</option>
								<option value="96">96</option>
							</select>
						</div>
						<div class="cell small-5 medium-7 large-7">
							<label class="middle">&nbsp;&nbsp;&nbsp;<i>of <strong>{{ $products->total() }}</strong> products</i></label>
						</div>
					</div>
				</form>
			</div>
			<div class="cell large-3 medium-3 small-3 text-right">
				<a class="button radius {{ $display_button_list }}" href="{{ $display_url_list }}"><i class="fa fa-lg fa-bars"></i></a>
				<a class="button radius {{ $display_button_grid }}" href="{{ $display_url_grid }}"><i class="fa fa-lg fa-th-large"></i></a>
			</div>
			<div class="cell auto">&nbsp;</div>
			<div class="cell large-2 medium-2 small-2">
				<form>
					<select class="radius">
						<option value="relevance">Relevance</option>
						<option value="asc">Price: Low to High</option>
						<option value="desc">Price: High to Low</option>
					</select>
				</form>
			</div>
		</div>
		<div class="grid-x margin-top-custom-10">
		@if($category->sub_categories->count() > 0)
			@foreach($sub_categories as $item)
				@if($item->name === "Sprinkler Design Center")
					<div class="cell large-3 medium-3 small-6 text-center">
						<a href="/free-sprinkler-design.html">
							<img src="{{ asset('img/sprinkler_thumbnail.jpg') }}" />
							<p>{{ $item->name }}</p>
						</a>
					</div>
				@else
					<div class="cell large-3 medium-3 small-6 text-center">
						<a href="{{ url($item->meta['slug']) }}">
							<img src="//images1.plumbersstock.com/162/162/marketing/{{ $item->image ?? 'default.jpg' }}" width="162px" height="162px" />
							<p>{{ $item->name }}</p>
						</a>
					</div>
				@endif
			@endforeach

			@if($category->has_sub_categories_sub_links > 0)
				@foreach($category->sub_categories_sub_links->get() as $link)
					<div class="cell large-3 medium-3 small-6">
						<a href="{{ url($link->path) }}">
							<img src="//images1.plumbersstock.com/162/162/marketing/{{ $item->image ?? 'default.jpg' }}" width="162px" height="162px" />
							<p>{{ $link->name }}</p>
						</a>
					</div>
				@endforeach
			@endif
		@else
		@endif

		</div>

		<div class="grid-x margin-top-custom-40 grid-margin-x">
			@include('product.template-1.'.$display)
		</div>
		{!! $products->appends(['display'=>$display, 'show'=>$show])->links('vendor.pagination.foundation-6') !!}
	</div>
</div>


<div class="grid-x margin-top-custom-40">
	<div class="cell auto"></div>
	<div class="cell large-10">{!! $category->content !!}</div>
	<div class="cell auto"></div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$(function(){
		<?php echo '$("#show").val("'.$show.'");';?>
		$('#show').change(function(){
			var value = $(this).val();
			var full_url = "{!! $full_url !!}";
			full_url = full_url.replace("{{ $show }}", value);
			window.location.replace(full_url);
		});
	});
</script>

@endsection