@extends('layouts.master')

@section('page-title', 'Privacy Policy')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Privacy Policy</li>
</ul>

<div class="support-wrapper">
	<h4 class="text-center support-head">Privacy Policy</h4>
	<p>
		When making a purchase with PlumbersStock.com we ask for some of your information. The information that is required (name, address, phone number, email address) is the information we need to process and fulfill you order. We also ask for your credit card number, expiration date, CVV code, and billing address, this information is used strictly for billing purposes. When you place and order with us you are creating account with us. At the time of order placement, you will be prompted to create a user name and designate a password for your account. An account is not needed to visit our website; however, it is needed to place an order with us. Once your account is created, you will be able to store billing/shipping information and check order status. For the purpose of security and privacy PlumbersStock.com does not store your credit card information in your account. If changes need to be made to your account, please contact customer service. 
	</p>
	<p>
		We value your privacy and any of the information that you provide us will not be sold or used for marketing purposes. We do provide your personal information to several third party companies that aid in fulfilling orders, deliver products, and process credit card transactions. These companies are reputable companies and only use your information for order fulfillment purposes. These third part companies are not authorized to use your information in any way other than to perform their contractually assigned functions. We may be required to disclose your personal information to third parties to aid in investigations or fraud prevention. 	
	</p>
	<h6 class="support-header margin-top-custom-20">How is my information used?</h6>
	<p class="margin-top-custom-10">
		The information you provide to PlumbersStock.com is used in the following ways:
		<ul>
			<li>Deliveries of purchased product</li>
			<li>Create an online account for you</li>
			<li>Email you tracking and other order information</li>
			<li>Bill your credit card</li>
			<li>Update you about the status of your order</li>
			<li>Respond to customer service inquiries</li>
		</ul>
	</p>
	<h6 class="support-header margin-top-custom-20">Email</h6>
	<p class="margin-top-custom-10">
		We do not sell or market your email address in any way. Your email address is used for the sole purpose of communicating with you about your order. You will receive order confirmations and shipping notifications via email. You will also receive any information regarding back ordered items on your order via email. We also have an email for the use of our customer to communicate with us. If you contact us via email, you will receive your response via email. 
	</p>
	<h6 class="support-header margin-top-custom-20">Transaction Security</h6>
	<p class="margin-top-custom-10">
		Each transaction that is processed through our website is processed immediately. We generally do not process your credit card until the time that your order ships except in special circumstances. In the event that we have a problem with processing your credit card, we will contact you immediately. Our customer security is very important to us and we work hard to protect it. We use Secure Sockets Layer (SSL) software to protect the security of your information during transmission. This is usually symbolized on web-browsers by a small padlock or key appearing on the bottom bar of the window and the address of the window https:// instead of http://, meaning a secure connection. Before completing any transaction online you should always check to make sure it is a secure connection. You can check to see if you are on a secure site if you see a padlock or a key in the address bar. If the padlock is closed or the key is unbroken then you are shopping on a secure website. In the event that an order cannot be completed, please contact our Customer Service, it is more than likely that there is a problem with the security. 
	</p>
	<h6 class="support-header margin-top-custom-20">Automatic Information</h6>
	<p class="margin-top-custom-10">
		Like many websites, we use "cookies". Cookies are alphanumeric identifiers that we transfer to your computer's hard drive through your Web browser to enable our systems to recognize your browser. These Cookies are used to receive and store certain types of information whenever you interact with our site. There are numerous companies and software that allow you to visit web sites anonymously and do not allow these cookies to be stored in your computer. Thanks for choosing <a href="{{ route('psDashboard') }}">PlumbersStock</a>. 
	</p>
</div>

@endsection