@extends('layouts.master')

@section('page-title', 'Contact Us')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Contact Us</li>
</ul>
@include('partials.success-message')
<div class="support-wrapper">
	<h4 class="text-center support-head">Contact PlumbersStock.com Today!</h4>
	<p class="margin-top-custom-10">
		PlumbersStock is a family owned and operated business with corporate offices in Cedar City, Utah. We appreciate you choosing us for plumbing supply and would like to help you in any way we can. Please contact us with any questions, comments or suggestions. We want to earn your business and keep you coming back. Customer service representatives are standing by ready to gladly help you with your purchasing decisions.
	</p>
	<div class="grid-x grid-margin-x">
		<div class="cell large-2 text-right">
			<img src="{{ asset('img/icons/arroba.svg') }}" width="64px">
		</div>
		<div class="cell large-10">
			 <p>
				<span class="support-label">Customer Support:</span><br />
				<span class="support-value">support@plumbersstock.com</span>
			 </p>
			 <p>
				<span class="support-label">Website Feedback:</span><br />
				<span class="support-value">webmaster@plumbersstock.com</span>
			 </p>
		</div>
	</div>
	<div class="grid-x grid-margin-x">
		<div class="cell large-2 text-right">
			<img src="{{ asset('img/icons/phone.svg') }}" width="64px">
		</div>
		<div class="cell large-10">
			 <p>
				<span class="support-label">Phone:</span><br />
				<span class="support-value">(435) 868-4020</span>
			 </p>
			 <p>
				<span class="support-label">Fax:</span><br />
				<span class="support-value">(435) 868-4019</span>
			 </p>
		</div>
	</div>
	<div class="grid-x grid-margin-x">
		<div class="cell large-2 text-right">
			<img src="{{ asset('img/icons/mail.svg') }}" width="64px">
		</div>
		<div class="cell large-10">
			<p>
				<span class="support-label">PlumbersStock.com</span><br />
				<span class="support-value">506 North 200 West · Cedar City, Utah 84721</span>
				<br />
				
			</p>
		</div>
	</div>

	<h6 class="support-header margin-top-custom-20">Our hours of operation are:</h6>
	<ul class="support-ul margin-top-custom-10">
		<li>7:00am - 7:00pm Monday - Friday MST</li>
		<li>8:00am - 2:00pm Saturday MST</li>
	</ul>
	<h6 class="support-header margin-top-custom-20">Holiday Hours</h6>
	<ul class="support-ul margin-top-custom-10">
		<li>
	    Customer Service will be closed on Memorial Day, The 4th of July, Labor Day, Thanksgiving, Christmas Eve, Christmas Day, New Years Eve and New Years Day.
		</li>
	</ul>
	<h4 class="support-header margin-top-custom-20">Please Call or E-mail</h4>
	<p class="margin-top-custom-10">
		 If you don’t have time to sit on the phone with a customer service representative, please use the form below to submit your question about our plumbing products. PlumbersStock prides itself on being a leader in customer service. Put us to the test. We know the business. We’ve been doing this for 20+ years and we realize it’s you, the customer, who keeps us in business. We keep our customers satisfied, and that’s what makes us #1 in online plumbing supply.
	</p>
	<h4 class="text-center support-header">Send Us A Message</h4>
	<p class="text-center"> Fill out the Form below and we will respond in a timely manner.</p>
	<form method="post" action="{{ route('psSupportContactUsSend') }}">
		<div class="grid-container">

			<div class="grid-x grid-padding-x">
				<div class="cell auto"></div>
				<div class="large-6 cell">
					<label><strong>Name</strong>
						<input type="text" class="radius" name="name" autocomplete="off" value="" required="" />
					</label>
					@if($errors->first('name'))
						<p class="alert-first text-center">{{ $errors->first('name') }}</p>
					@endif
				</div>
				<div class="cell auto"></div>
			</div>

			<div class="grid-x grid-padding-x">
				<div class="cell auto"></div>
				<div class="large-6 cell">
					<label><strong>Phone Number</strong>
						<input type="text" class="radius" name="phone_number" autocomplete="off" value="" />
					</label>
					@if($errors->first('phone_number'))
						<p class="alert-first text-center">{{ $errors->first('phone_number') }}</p>
					@endif
				</div>
				<div class="cell auto"></div>
			</div>

			<div class="grid-x grid-padding-x">
				<div class="cell auto"></div>
				<div class="large-6 cell">
					<label><strong>Email Address</strong>
						<input type="email" class="radius" name="email" autocomplete="off" value="" required="" />
					</label>
					@if($errors->first('email'))
						<p class="alert-first text-center">{{ $errors->first('email') }}</p>
					@endif
				</div>
				<div class="cell auto"></div>
			</div>

			<div class="grid-x grid-padding-x">
				<div class="cell auto"></div>
				<div class="large-6 cell">
					<label><strong>Order-Id (Optional)(Case Sensitive)</strong>
						<input type="text" class="radius" name="order_id" autocomplete="off" value="" />
					</label>
					@if($errors->first('order_id'))
						<p class="alert-first text-center">{{ $errors->first('order_id') }}</p>
					@endif
				</div>
				<div class="cell auto"></div>
			</div>

			<div class="grid-x grid-padding-x">
				<div class="cell auto"></div>
				<div class="large-6 cell">
					<label><strong>Message</strong>
						<textarea class="radius" name="message" rows="5" required=""></textarea>
					</label>
					@if($errors->first('message'))
						<p class="alert-first text-center">{{ $errors->first('message') }}</p>
					@endif
				</div>
				<div class="cell auto"></div>
			</div>

			<div class="grid-x grid-padding-x">
				<div class="large-12 cell text-center">
					@csrf()
					<button class="button success2 radius" type="submit">Submit Changes</button>
				</div>
			</div>

		</div>
	</form>
</div>

@endsection