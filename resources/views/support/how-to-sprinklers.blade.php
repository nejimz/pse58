@extends('layouts.master')

@section('page-title', 'Sprinkler Questions & How To Tutorials')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Sprinkler Questions & How To Tutorials</li>
</ul>

<h4>Sprinkler Questions & How To Tutorials</h4>
<h6>Sprinkler FAQ's</h6>
<p>
	PlumbersStock is a great place to buy sprinkler supply, but there are many questions to answer before proceeding with a purchase. Here we address common <strong>sprinkler questions</strong>. Inquiries range from <i>when should you water your lawn? to how do rain sensors work?</i> If you have difficulty locating information that is pertinent to your needs, then please <a href="{{ route('psSupportContactUs') }}">contact us</a>. You can do it via email, our chat feature, social media, or the old-fashioned way, by phone. Your questions often end up helping out others, because we like to choose from customer questions for our articles.
</p>
<h6>Sprinkler How to Tutorials</h6>
<p>
	For DIY homeowners, there are many resources here that will help you address specific problems. We also have guides for installations. Some are very difficult, and some are easy, so you should be aware of your skill and ability (and determination) and weigh that against what is required in each project. Sometimes it's just easier to hire someone to do the work for you. These <strong>irrigation-how-to</strong> guides will help you to gauge the commitment the project requires and then decide a plan of attack, accordingly.
</p>
<h6>Contractors: Contact Us for Special Pricing</h6>
<p>
	We can work with you on pricing, and that means better deals for you. The prices we list on our website are based on manufacturer guidelines, and we cannot advertise below their price floors. However, we can partner with you and offer them at prices better than you will likely find anywhere else. If you are interested in this program, please <a href="mailto:support@plumbersstock.com">email us</a>. Thank you for choosing <a href="{{ route('psDashboard') }}">PlumbersStock</a>.
</p>

@endsection