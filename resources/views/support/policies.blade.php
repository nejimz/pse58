@extends('layouts.master')

@section('page-title', 'Policies')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Policies</li>
</ul>

<div class="support-wrapper">
	<h4 class="text-center support-head">Policies & Terms of Use</h4>
	<p>
		Welcome to PlumbersStock.com where we strive to provide our customers with quality products and service. We have found the following conditions create a mutually beneficial relationship with our customers. By visiting our website you are accepting the following conditions:
	</p>
	<h5 class="support-header margin-top-custom-20">Privacy Policy</h5>
	<p class="margin-top-custom-10">Click <a href="{{ route('psSupportPrivacy') }}">here</a> to read our Prvacy Policy</p>

	<h5 class="support-header margin-top-custom-20">Shipping Policy</h5>
	<p class="margin-top-custom-10">Click <a href="{{ route('psSupportShipping') }}">here</a> to read our Shipping Policy</p>

	<h5 class="support-header margin-top-custom-20">Return Policy</h5>
	<p class="margin-top-custom-10">Click <a href="{{ route('psSupportReturnsAndExchanges') }}">here</a> to read our Returns & Exchanges Policy</p>
</div>

@endsection