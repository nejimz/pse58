@extends('layouts.master')

@section('page-title', 'Our Shipping Policy')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Our Shipping Policy</li>
</ul>

<div class="support-wrapper">
	<h4 class="text-center support-head">Shipping Policy</h4>

	<p>
		At PlumbersStock.com we put forth our best effort to ensure that shipping costs are correctly calculated at the time you place your order; however, under some circumstances it is not possible to do so. We reserve the right to notify you after the order has been placed of any additional shipping or handling charges necessary to deliver your order. Please note that these additional charges generally occur when shipping an item via freight. If notified of any additional charges, you have the right to cancel or make any necessary changes to your order.
	</p>
	<h6 class="support-header margin-top-custom-20">Free Shipping</h6>
	<ul class="support-ul margin-top-custom-10">
		<li><strong>Orders > 1000$ = FREE Ground Shipping</strong></li>
		<li><strong>Orders < 1000$ but > $500 = %50 off Ground Shipping</strong></li>
	</ul>
	<p>
		Free shipping is offered on orders over $1,000.00 that DO NOT have to be shipped common carrier freight (any item(s) over 100lbs, Toilets, bathtubs, water heaters). 
	</p>
	<p>
		 Orders shipping outside of the continental US do not qualify for shipping discounts, regardless of dollar amount. Please contact <a href="{{ route('psSupportContactUs') }}">Customer service</a> for a shipping quote.
	</p>
	<h6 class="support-header margin-top-custom-20">Shipping Types</h6>
	<p class="margin-top-custom-10">
		We offer Economy (USPS Parcel), Ground, Third day, Second day, and Overnight shipping. In stock orders must be placed before 2pm MST to be shipped that same day. In stock orders placed after 2pm MST will be shipped the next business day. Orders will be shipped to Canada via USPS or FedEx International Mail Services; all other international orders will be shipped via FedEx International Mail Services. Please allow 4-5 weeks for delivery with this method. We also offer LTL Freight Shipping for fragile and larger items. Shipping costs are calculated on the "ship-to" address. To obtain a shipping quote, please contact customer service. Orders are generally shipped as complete. We will not ship out your order in separate shipments unless you request it. We can divide your order into two different shipments at no additional cost to you. Each subsequent shipment after two shipments is $10.00.
	</p>
	<h6 class="support-header margin-top-custom-20">Freight Shipments</h6>
	<p class="margin-top-custom-10">
		 Many shipments are too large, too heavy, or too fragile to be shipped via package service and so they must be shipped by an LTL freight carrier. <a href="{{ route('psDashboard') }}">PlumbersStock.com</a> is not liable for damages that occur during shipment. Upon delivery, the driver will provide a delivery receipt to be signed by the customer, as acknowledgement that the goods have arrived in acceptable condition. Please inspect goods, even if packaging is in good condition. Any problems, such as damages or shortages, MUST be noted on the receipt. No claims can be made without this information and PlumbersStock.com will not be held responsible for any discrepancies not specified with the carrier at time of delivery.
	</p>
	<p>
		Carriers will contact customers before delivery. If the carrier is unable to reach the consignee with information provided (i.e. phone numbers, etc) within a 3-5 business days, storage fees begin to accrue at $75 a day. If no contact is made within five days, the product will be re-routed back to PlumbersStock.com. Any storage fees, return freight costs, and restock fees will apply and be withheld on any refunds to be made for the returned product. 
	</p>
	<h6 class="support-header margin-top-custom-20">Undeliverable Packages</h6>
	<p class="margin-top-custom-10">
		We occasionally have packages that are returned back to us. The returned package will be processed and handled according to the following circumstances:
	</p>
	<p class="margin-top-custom-10">
		<strong>Insufficient Address:</strong> When a package is delivered back to us with an "undeliverable address" label it means that the there was a problem with address on the shipping label. We will check within our system to verify all the information is correct. We will then contact you if we need additional information regarding your address.
	</p>
	<p class="margin-top-custom-10">
		<strong>Refusal:</strong> If we received a package back that has been refused we will process it as a return. Refused packages are subject to a 25% restock fee. If you need to return a product please contact Customer Service.
	</p>
</div>
@endsection