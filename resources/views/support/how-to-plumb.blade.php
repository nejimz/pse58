@extends('layouts.master')

@section('page-title', 'Plumbing How to Tutorials & FAQ`s')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Plumbing How to Tutorials & FAQ's</li>
</ul>

<h4>Plumbing How to Tutorials & FAQ's</h4>


<h6>Plumbing How to Tutorials</h6>
<p>
	Here we have plenty of resources to teach you how to complete specific plumbing projects. Tasks range from simple to difficult and we recommend that you be aware of the fact that just because you can do a job, doesn't mean you should. Sometimes they are a bigger pain than they are worth and your friendly neighborhood plumber will be very glad to help out. These <strong>plumbing how to</strong> guides will help you gauge the amount of work and skill these projects will take.
</p>
<p>
	That being said, there is a certain pride that comes with a job well done, especially when it's the king working on his own castle. If this notion floats your boat, then by all means, proceed with the work yourself.
</p>
<p>
	We have video guides and other types of helpful resources. If you do not see the project you need help with here, please <a href="{{ route('psSupportContactUs') }}"><strong>contact us</strong></a> and tell us about it so that we can create a tutorial just for you.
</p>
<h6>Plumbing Questions</h6>
<p>
	In addition to tutorial guides, we offer readers answers to <strong>plumbing FAQ's</strong>. Questions range from what kind of water heater you should get to how tankless water heaters work. Again, if you have any trouble finding the information you need, we are genuinely interested in providing customers with helpful resources. Please contact us and we will do what we can to answer your question and if it's a fairly common question, we'll add it to our resources here. Thanks for choosing PlumbersStock for <strong><a href="{{ route('psDashboard') }}">plumbing supply online</a></strong>.
</p>

@endsection