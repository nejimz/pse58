@extends('layouts.master')

@section('page-title', 'FAQ`s Page')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">FAQ's Page</li>
</ul>

<div class="support-wrapper">
	<h4 class="text-center support-head">Frequently Asked Questions</h4>

	<h5 class="support-header margin-top-custom-20">Wholesale Plumbing Supply</h5>
	<h6 class="support-header margin-top-custom-20">How can I tell if an item is in stock?</h6>
	<p class="margin-top-custom-10">
		If the item you are looking at says <strong>"In Stock and Ready To Ship!"</strong> then the item is most likely in stock. All other items will give the appropriate estimated shipping time. If an item states <strong>"Ships within *"</strong> the item is currently in transit to our warehouse and is available to ship within the days specified.
	</p>
	<h6 class="support-header margin-top-custom-20">How long does a product take to ship if it's not in stock?</h6>
	<p class="margin-top-custom-10">
		If we currently do not have a product in stock the appropriate estimated shipping time will be indicated below the price. It generally takes our online plumbing supply store about 7-10 business days to get a non-stock product into our warehouse to ship out; however this does vary by manufacturer and may take longer.
	</p>
	<h6 class="support-header margin-top-custom-20">Does the faucet include the valve?</h6>
	<p class="margin-top-custom-10">
		The wholesale plumbing supplies that we offer are purchased directly from the manufacturer as they would sell the product. Depending on the manufacturer, the valve may or may not be included. This information can be found on the manufacturer's specification sheets provided on our website. Some helpful hints are:
	</p>
	<ul class="support-ul margin-top-custom-10">
		<li>Moen and Delta products that require a valve to be purchased separately will have model numbers that begin with the letter T (indicating trim only).</li>
		<li>Danze products will have a T at the end of the model number (indicating trim only)</li>
		<li>Grohe roman tub and lavatory products have the valves included whereas shower trims do not.</li>
		<li>Take a look at the specification sheets located on the product listing.</li>
		<li>Information on whether that valve is included or sold separately can also be found on the individual manufacturer's website.</li>
		<li>Still not sure? Feel free to contact our customer service at 435-868-4020 and we will be more than happy to help you.</li>
	</ul>
	<h6 class="support-header margin-top-custom-20">Your website states free shipping but there is a shipping cost in my cart. What qualifies for free shipping?</h6>
	<p class="margin-top-custom-10">
		We do offer free shipping on our website for specific orders from our online plumbing supply store. Free shipping is only for orders over $1,000.00. Free shipping is for Economy shipping but if you would like a faster shipping method, shipping charges will apply. We also offer 50% off shipping on all orders over $500.00. There are some items that we sell that cannot be sent via package service such as: toilets, water heaters, and bathtubs. These items have to be sent common carrier freight and do not qualify for free shipping or discounted shipping. PlumbersStock's professional shipping services are one reason so many choose us for wholesale plumbing supplies online.
	</p>
	<h6 class="support-header margin-top-custom-20">How do I return an item?</h6>
	<p class="margin-top-custom-10">
		Contact our customer Service department at <strong>435-868-4020</strong> to authorize the return. You will receive an RMA number and an RMA form. The item must be in new/uninstalled condition to be returned. If a return authorization is not obtained, a 20% restock fee will apply. (Refer to the <a href="{{ url('psSupportReturnsAndExchanges') }}">Return Policy</a>)
	</p>
	<h6 class="support-header margin-top-custom-20">What is economy shipping and how long does it take?</h6>
	<p class="margin-top-custom-10">
		Economy shipping is USPS parcel, or FedEx Smartpost. The <strong>FedEx Smartpost</strong> is a shipping method in which the package is pickup at our warehouse and carried to your local town by FedEx. Once the package has arrived to a FedEx Smartpost sorting facility, it is then dropped off at the local Post office for delivery. Economy shipping of plumbing parts generally takes 7-10 business days from our warehouse to your door. Though the Economy shipping method may take longer, we offer it to you as a cost efficient way to get wholesale plumbing supply delivered directly to your door.
	</p>
	<h6 class="support-header margin-top-custom-20">How will my item ship?</h6>
	<p class="margin-top-custom-10">
		When you check out from our online plumbing supply store you will notice five different shipping methods to choose from. For ground, 3rd day, 2nd day, and next day, your order will be shipped out via either UPS or FedEx. Economy shipping generally ships out USPS Parcel and takes approximately 7-10 business days to be delivered to you.
	</p>
	<h6 class="support-header margin-top-custom-20">Where do you ship from?</h6>
	<p class="margin-top-custom-10">
		<a href="{{ route('psDashboard') }}">PlumbersStock's</a> warehouses are all located in Southern Utah; this is where we ship from. There are several items that we sell that we do not stock, these items are shipped directly to you from the factory or the manufacturer. This allows us to offer an unbeatably wide range of wholesale plumbing supplies online.
	</p>
	<h6 class="support-header margin-top-custom-20">Do you charge sales tax?</h6>
	<p class="margin-top-custom-10">
		PlumbersStock.com is only required to collect sales tax on orders shipped within the state of Utah. Orders shipped outside of the state of Utah do not have to pay sales tax.
	</p>
</div>

@endsection