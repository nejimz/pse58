@extends('layouts.master')

@section('page-title', 'HVAC Questions & How to Tutorials')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">HVAC Questions & How to Tutorials</li>
</ul>

<h4>HVAC Questions & How to Tutorials</h4>
<h6>HVAC FAQ's</h6>
<p>
	Maybe the most pertinent question of all is <i>what does HVAC stand for?</i> The answer is <strong>heating, ventilation, and air conditioning</strong>. That was an easy one. We offer readers answers to much more difficult <strong>HVAC questions</strong> in this section of our site. Questions range from what is a condensing boiler? to are vent free gas fireplaces safe? If you have any trouble finding the information you seek, we take a genuine interest in answering those tough questions, so please <a href="{{ route('psSupportContactUs') }}">contact us</a> either through chat, email, phone, or social media and ask us your questions! It's likely that we'll end up writing an article to answer the question, so that others can benefit from your ask.
</p>
<h6>HVAC How to Tutorials</h6>
<p>
	Here we offer homeowners and do-it-yourselfers plenty of resources to assist with specific heating and cooling projects. The various tasks range from simple to difficult. Remember the number one rule of is just because you can complete a project yourself, doesn't mean you should. If you buy a furnace at PlumbersStock and watch the video and decide you don't want to install it in the attic yourself, that's not a bad call. Hiring a professional can often be a big win for both parties. All that being said, there is nothing quite like the feeling you get with a job well done. These <strong>heating and cooling how to</strong> guides will help you to gauge the commitment the project requires.
</p>
<p>
	We have video guides, step-by-step guides, and other helpful resources. Again, if you do not see the project you need help with here, please tell us so that we can write an article about it to help you, and others like you. Thanks for choosing PlumbersStock as your <a href="{{ route('psDashboard') }}">home improvement store online</a>.
</p>

@endsection