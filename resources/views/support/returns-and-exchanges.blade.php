@extends('layouts.master')

@section('page-title', 'Privacy Policy')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Returns and Exchanges</li>
</ul>

<div class="support-wrapper">
	<h4 class="text-center support-head">Return Policy</h4>
	<h6 class="support-header margin-top-custom-20">Items that may be returned</h6>
	<p class="margin-top-custom-10">
		<strong>**All returns must be authorized by a PlumbersStock.com representative. Please contact customer service to set up a return. You will receive a completed RGA form that you MUST include in your return to receive credit.**</strong>
	</p>
	<ul class="support-ul">
		<li>Items that are being returned within 30 days of receipt, uninstalled, and in resalable condition complete with original packaging</li>
		<li>Items that were not shipped correctly</li>
		<li>Any defective items</li>
		<li>Items that were ordered in error by customer</li>
		<li>Items received not as described </li>
	</ul>
	<p>
		Product must arrive to PlumbersStock.com in resalable condition. Please note that if the return is not in it's original packaging, there will be a 15% restocking fee applied to the return. No defects or damages to product or packaging should be present to obtain a full credit (less shipping and restocking fees). All returns must be double boxed for a credit to be issued. Return shipping costs are not covered by PlumbersStock.com and PlumbersStock.com is not responsible for any damages that occur during return shipping. Special order items are NOT returnable and shipping charges are non refundable. NO EXCEPTIONS! Any items returned after 30 days of return authorization issuance will be refused. Return authorization must be made within 30 days of customer receipt. 
	</p>
	<p>
		All unauthorized returns are subject to a 15% restocking fee at the sole discretion of PlumbersStock.com. Exchanges are permitted within 30 days of receipt of order; however, new shipping charges will apply on product "trades" (i.e. items ordered at the customer's error with a request later for a return and a replacement with a new product). 	
	</p>
	<p>
		For orders over $2500, returns MUST be authorized by a supervisor. Many of these items are special order items that we purchase for our customers and the manufacturer will not allow returns. 
	</p>
	<h6 class="support-header margin-top-custom-20">Defective and Damaged Products</h6>
	<p class="margin-top-custom-10">
		<strong>Defective items will only be exchanged for the same product or in-store credit.</strong>
	</p>
	<ul class="support-ul">
		<li>Items sent at PlumbersStock's error will be returned at PlumbersStock's expense</li>
		<li>Items lost or damaged in shipping will be resent at PlumbersStock's expense</li>
		<li>Credit for damaged, or defective items will not be given until the item has been returned and inspected</li>
		<li>Items that are defective will be replaced at no additional charge</li>
	</ul>
	<p>
		Damages received during shipping must be notified to PlumbersStock.com within 14 days of package receipt. After this time frame, PlumbersStock.com will not be held responsible for damages not reported to PlumbersStock.com or the shipping company. Return authorizations for defective or damaged products seeking a replacement will not require payment. However, if the defective or damaged product is not returned within 30 days from the issuance of the return authorization, payment will be taken for the replacement product. You may also be required to provide pictures or other necessary documentation in order to receive a refund or replacement.
	</p>
	<p>
		Any defective product not returned back to PlumbersStock.com within 30 days from return authorization issuance will not receive any credit. Any defective product returned under the manufactures warranty, is only subject to a replacement, not a refund. 
	</p>
	<h6><strong>Damaged Packages</strong></h6>
	<p class="margin-top-custom-10">
		All items shipped via package service (i.e. UPS, FedEx, USPS, etc) that have been damaged in transit must be refused at delivery or a claim filed within 14 days of receipt from delivery. Please open and inspect all packages upon delivery to identify any damages immediately. No claims can be made without this information and PlumbersStock.com will not be held responsible for any discrepancies not specified with PlumbersStock.com or the shipping company. PlumbersStock.com will not be held responsible for any claims made after 14 days of customer receipt. 
	</p>
	<p>
		If your item was shipped via LTL freight, it is very important that you check your package and your item for damage BEFORE the driver leaves. The damage MUST be noted upon signing for the item. If damage is not noted, YOU WILL NOT RECEIVE A REFUND OR A REPLACEMENT PRODUCT. If damage is not noted upon delivery, we will not be able to receive a damage claim from the freight company to receive money for that damaged product. The customer in turn will not receive a refund or a replacement item. 
	</p>
	<h6 class="support-header margin-top-custom-20">Payments / Refunds</h6>
	<p>
		 Refunds will be given upon receipt and inspection. Returned products will be verified to be in new and resalable condition in it's original packaging before a refund will be given. The refund will be applied to the same payment method used. All credit card refunds may take 5-7 business days to process.
	</p>
	<p>
		PlumbersStock.com generally does not charge your card until your order has shipped. Some orders require special purchasing in which the payment must be taken before the product is ordered (i.e. orders shipping directly from the factory or manufacturer to the customer). We reserve the right to charge the credit card and investigate any order that seems fraudulent in any way. 
	</p>
	<h6 class="support-header margin-top-custom-20">Quotes</h6>
	<p class="margin-top-custom-10">
		To obtain a price quote please contact our customer service. You will receive a verbal quote over the phone. A verbal quote must be paid for immediately or verified via email to be valid. If you receive a formal PlumbersStock.com quote with a quote number, it is good for 30 days. The quote number must be provided to receive quoted price. 
	</p>
	<h6 class="support-header margin-top-custom-20">Liability</h6>
	<p class="margin-top-custom-10">
		It is the responsibility of the customer to inspect the product upon delivery to make sure it meets installations requirements. DO NOT alter or install the product until you are sure that you have the correct product, free of damage. Once the product has been installed, it is no longer returnable and is only covered by the manufacturer's limited warranty. 
	</p>
	<p>
		Unless otherwise noted, all merchandise on PlumbersStock.com is new and carries the manufacturer's original warranty. 
	</p>
	<p>
		At PlumbersStock.com we offer quality products and work hard to provide fast and accurate service. If you receive your order incorrect, damaged, or defective we will work to promptly correct the situation. We will refund or replace any items in this situation, pending availability. In the event of reshipping an item, your item will be shipped using the same shipping method and warehouse originally used. If expedited shipping or a different warehouse is desired, it is the customer's responsibility to pay the difference in cost. 
	</p>
	<p>
		PlumbersStock.com is not responsible or liable for the cost of installation or any costs associated with installation. It is recommended that customers DO NOT schedule a plumber or installer until they have the product in their possession. PlumbersStock.com is not responsible for any liability cost beyond the replacement costs of parts covered by the manufacturer's warranty. 
	</p>
	<p>
		At PlumbersStock.com we work hard to fulfill and ship your order out as fast as possible. We do reserve the right to back order or cancel orders at anytime for any reason. In the event this happens, you will be notified via email of the expected lead time or cancellation. 
	</p>
	<h6 class="support-header margin-top-custom-20">Cancellations</h6>
	<p class="margin-top-custom-10">
		<a href="{{ route('psDashboard') }}">PlumbersStock.com</a> strives to ship orders as soon as possible. Orders can be cancelled at anytime before shipment. To cancel your order, please contact our customer service department. There are no cancellation fees as long as the product has not been shipped. If the product has been shipped it cannot be cancelled. The customer will need to receive the item and follow the return policy. 
	</p>
	<h6 class="support-header margin-top-custom-20">Disclaimer</h6>
	<p class="margin-top-custom-10">
		At PlumbersStock.com we make every attempt to make sure that our prices and product information are correct. However, we are not liable for any typographical errors or pricing listed in error on our website. 
	</p>
	<strong>Plumbersstock.com reserves the right to cancel or refuse any order at anytime.</strong>
</div>

@endsection