@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<h5 class="text-center">Subscription Center for: {{ Sentinel::getUser()->first_name . ' ' . Sentinel::getUser()->last_name }}</h5>
		<h6 class="text-center">Email Address Receiving Subscriptions: {{ Sentinel::getUser()->email }}</h6>
		<h6>Subscriptions</h6>
		<p class="text-center">
			<button class="button radius">Submit Changes</button>
		</p>
		<p class="text-center">
			<i>To subscribe to or unsubscribe from a subscription just check or uncheck the box next to it.</i>
		</p>
	</div>
</div>

@endsection