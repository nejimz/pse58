@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li><a href="{{ route('address-book.index') }}">Address Book</a></li>
			<li class="disabled">{{ (isset($id))? 'Edit' : 'New' }}</li>
		</ul>
		<h5 class="text-center">Add New Address</h5>
		<form action="{{ $route }}" method="post">
			<div class="grid-container">
				@include('partials.error-messages')
				<div class="grid-x grid-padding-x">
					<div class="large-6 medium-6 small-12 cell">
						<label>Address Nickname:
							<input type="text" class="radius" name="nickname" autocomplete="off" value="{{ $nickname }}" />
						</label>
					</div>
					<div class="large-6 medium-6 small-12 cell">
						<label>Address Line 1:
							<input type="text" class="radius" name="address_line_1" autocomplete="off" value="{{ $address_line_1 }}" />
						</label>
					</div>

					<div class="large-6 medium-6 small-12 cell">
						<label>First Name:
							<input type="text" class="radius" name="first_name" autocomplete="off" value="{{ $first_name }}" />
						</label>
					</div>
					<div class="large-6 medium-6 small-12 cell">
						<label>Address Line 2:
							<input type="text" class="radius" name="address_line_2" autocomplete="off" value="{{ $address_line_2 }}" />
						</label>
					</div>

					<div class="large-6 medium-6 small-12 cell">
						<label>Last Name:
							<input type="text" class="radius" name="last_name" autocomplete="off" value="{{ $last_name }}" />
						</label>
					</div>
					<div class="large-6 medium-6 small-12 cell">
						<label>Zip Code:
							<input type="text" class="radius" name="zip_code" autocomplete="off" value="{{ $zip_code }}" />
						</label>
					</div>

					<div class="large-6 medium-6 small-12 cell">
						<label>Company:
							<input type="text" class="radius" name="company" autocomplete="off" value="{{ $company }}" />
						</label>
					</div>
					<div class="large-6 medium-6 small-12 cell">
						<label>City:
							<input type="text" class="radius" name="city" autocomplete="off" value="{{ $city }}" />
						</label>
					</div>

					<div class="large-6 medium-6 small-12 cell">
						<label>Phone Number:
							<input type="text" class="radius" name="phone_number" autocomplete="off" value="{{ $phone_number }}" />
						</label>
					</div>
					<div class="large-6 medium-6 small-12 cell">
						<label>State/Province:
							<select class="radius" id="state" name="state">
								@include('partials.select-state')
							</select>
						</label>
					</div>

					<div class="large-6 medium-6 small-12 cell">
						<div class="grid-x"><br><br>
							<div class="cell large-6">
								<input type="hidden" name="billing" value="0">
								<label><input type="checkbox" class="radius" id="billing" name="billing" value="1" {{ ($billing == 1) ? 'checked="checked"' : '' }} /> For Billing</label>
								<input type="hidden" name="shipping" value="0">
								<label><input type="checkbox" class="radius" id="shipping" name="shipping" value="1" {{ ($shipping == 1) ? 'checked="checked"' : '' }} /> For Shipping </label><br>
							</div>
							<div class="cell large-6">
								<input type="hidden" name="is_default_billing" value="0">
								<label><input type="checkbox" class="radius" id="is_default_billing" name="is_default_billing" value="1" {{ ($is_default_billing == 1) ? 'checked="checked"' : '' }} /> Set As Default</label>
								<input type="hidden" name="is_default_shipping" value="0">
								<label><input type="checkbox" class="radius" id="is_default_shipping" name="is_default_shipping" value="1" {{ ($is_default_shipping == 1) ? 'checked="checked"' : '' }} /> Set As Default</label>
							</div>
						</div>
					</div>
					<div class="large-6 medium-6 small-12 cell">
						<label>Country:
							<select class="radius" id="country" name="country">
								@include('partials.select-country')
							</select>
						</label>
					</div>

					<div class="large-12 medium-12 small-12 cell text-center">
						@csrf()
						@isset($id)
							@method('PUT')
						@endisset
						<button class="button radius" type="submit">Save Address</button>
					</div>

				</div>

			</div>
		</form>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){

		$('#state').val('{{ $state }}');
		$('#country').val('{{ $country }}');
	});
</script>
@endsection