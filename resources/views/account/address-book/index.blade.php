@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li><a href="{{ route('address-book.index') }}">Address Book</a></li>
			<li class="disabled">View</li>
		</ul>
		<h5 class="text-center">My Address Book</h5>
		@foreach($addresses as $address)
		<div class="card">
			<div class="card-divider">
				<h6><a href="{{ route('address-book.edit', $address->id) }}" title="Edit Address"><i class="fa fa-edit"></i></a> {{ $address->header_name }}</h6>
			</div>
			<div class="card-section">
				{{ $address->first_name . ' ' . $address->last_name }}<br>
				{{ $address->company }}<br>
				{{ $address->phone_number }}<br>
				{{ $address->address_line_1 }}<br>
				{{ $address->address_line_2 }}<br>
				{{ $address->city . ', ' . $address->state . ' ' . $address->zip_code }}<br>
			</div>
		</div>
		@endforeach
	</div>
</div>

@endsection