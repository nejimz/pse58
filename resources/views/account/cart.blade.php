@extends('layouts.master')

@section('page-title', 'My Cart (' . $item_count . ')')

@section('content')

<style type="text/css">
</style>

<div class="grid-x">
	<div class="cell large-6">
		<h4 class="ps-h-weight">My Cart</h4>
	</div>
	<div class="cell large-6 text-right">
		<button class="button radius">Proceed to Checkout</button>
	</div>
</div>

<div id="cart-table-header" class="grid-x grid-padding-x">
	<div class="cell large-6">Item</div>
	<div class="cell large-2 text-right">Price</div>
	<div class="cell large-1 text-center">Qty</div>
	<div class="cell large-2 text-right">Total</div>
	<div class="cell large-1 text-center">&nbsp;</div>
</div>

@if(session()->has('user.cart') && count(session()->get('user.cart')) > 0)
	@foreach(session()->get('user.cart') as $key => $row)
	<?php
	#dd(session()->get('user.cart'));
	$product = App\Models\Catalog\CatalogProduct::where('productId', intval($row['productId']))->first();
	$price = $product->pricing['mapPrice'];
	if ($product->pricing['mapPrice'] < 1) {
		$price = $product->inventory['availability'][$row['warehouse']]['price'];
	}
	$total = $price * $row['quantity'];
	$limit_quantity = 0;
	$grand_total += $total;
	?>

	<div id="cart-table-header-{{ $key }}" class="grid-x grid-padding-x hide-for-large">
		<div class="cell large-6">Item</div>
		<div class="cell large-2 text-right">Price</div>
		<div class="cell large-1 text-center">Qty</div>
		<div class="cell large-2 text-right">Total</div>
		<div class="cell large-1 text-center">&nbsp;</div>
	</div>
	<div id="cart-table-body-{{ $key }}" class="grid-x grid-padding-x cart-table-body">
		<div class="cell large-6 cart-cell">
			<div class="grid-x">
				<div class="cell large-3">
					<img src="//images3.plumbersstock.com/125/125/alternative/{{ $product->images['0']['id'] ?? 'default.jpg' }}" width="125px">
				</div>
				<div class="cell large-9">
					<a href="{{ route('psProductPage', $product->meta['slug']) }}"><h6 class="ps-h-weight margin-top-custom-20">{{ $product->title }}</h6></a>
					<span class="label primary radius">From {{ $product->vendor_name($row['warehouse']) }} Warehouse</span>
				</div>
			</div>
		</div>
		<div class="cell large-2 cart-cell text-center cart-table-body-td-colored">
			<input type="hidden" id="cart_price_{{ $key }}" name="price" value="{{ $price }}">
			<p class="margin-top-custom-20 text-right">{{ $currency . '' . number_format($price, 2) }}/{{ $product->uom }}</p>
		</div>
		<div class="cell large-1 cart-cell text-center cart-table-body-td-colored">
			<div class="grid-x margin-top-custom-10">
				<div class="cell auto">
					<input class="input-group-field text-center radius-left bo_quantity" name="quantity" id="cart_quantity_{{ $key }}" type="text" value="{{ $row['quantity'] }}" onkeyup="increment_decrement('cart_quantity_{{ $key }}', {{ $limit_quantity }})" onchange="calculate_total(this.value, {{ $key }})" />
				</div>
				<div class="cell large-5">
					<button type="button" class="button expanded radius-top-right input-number-top-btn" onclick="increment_decrement('cart_quantity_{{ $key }}', {{ $limit_quantity }}, 'inc')"><strong>+</strong></button>
					<button type="button" class="button expanded radius-bottom-right input-number-bottom-btn" onclick="increment_decrement('cart_quantity_{{ $key }}', {{ $limit_quantity }}, 'dec')"><strong>-</strong></button>
				</div>
			</div>
		</div>
		<div class="cell large-2 cart-cell text-center cart-table-body-td-colored">
			<input type="text" class="input-mask-money cart-field-hidden-list cart-field-hidden{{ $key }} hide" name="cart_total_price[{{ $key }}]" value="{{ $total }}">
			<p class=" margin-top-custom-20 text-right">{{ $currency }}<span class="cart_total_price{{ $key }}">{{  number_format($total, 2) }}</span></p>
		</div>
		<div class="cell large-1 cart-cell text-center cart-table-body-td-colored">
			<button type="submit" class="button clear small radius margin-top-custom-10" onclick="remove_item('{{ $key }}', '{{ route('psCartDelete', $key) }}')"><i class="fa fa-remove fa-lg"></i></button>
		</div>
	</div>
	@endforeach
@endif

<div class="callout primary radius text-center margin-top-custom-30">
	<h6>This order is eligible for free shipping!</h6>
</div>

<div class="grid-x">
	<div class="cell auto">&nbsp;</div>
	<div class="cell large-7 text-right">
		<div class="grid-x grid-padding-x">
			<div class="cell large-5">
				<label for="zip_code" class="text-right middle">Estimate shipping to zipcode:</label>
			</div>
			<div class="cell large-3">
				<input class="radius" type="text" id="zip_code" name="zip_code" value="" placeholder="20004" />
			</div>
			<div class="cell large-3 text-center">
				<button class="button expanded radius">Get Estimate</button>
			</div>
			<div class="cell large-1 text-center">
				<strong>------</strong>
			</div>
		</div>
	</div>
</div>

<div id="cart-subtotal" class="grid-x">
	<div class="cell large-6 text-right">
		<strong>Subtotal:</strong>
	</div>
	<div class="cell large-6 text-right ps-h-weight">
		<input type="text" class="grand-total hide input-mask-money" name="grand-total" value="{{ $grand_total }}}" />
		{{ $currency }}<span class="grand-total-text">{{ number_format($grand_total, 2) }}</span>
	</div>
</div>

<div id="cart-checkout-paypal" class="grid-x margin-top-custom-20">
	<div class="cell large-10"></div>
	<div class="cell large-2 text-center">
		<button class="button expanded radius">Proceed to Checkout</button>OR
		<button class="button expanded clear"><img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-medium.png" alt="Check out with PayPal"></button>
		<a href="javascript:void(0)">Do you have a promo code?</a>
	</div>
</div>

<p>
	Recommendations based on your cart
</p>

@endsection

@section('scripts')

<script type="text/javascript">
	var xhr_remove_item = null;

	$(function(){

		$('.input-mask-money').inputmask('decimal', {
			'alias': 'numeric',
			'groupSeparator': ',',
			'autoGroup': true,
			'digits': 2,
			'radixPoint': ".",
			'digitsOptional': false,
			'allowMinus': false,
			//'prefix': '$ ',
			'placeholder': '0'
			}
		);
	});

	function remove_item(key, route)
	{
		var confirmation = confirm('Are you sure to delete this item?');

		if (!confirmation) {
			return false;
		}

	    xhr_remove_item = $.ajax({
	    	type : 'post',
	    	url : route,
	    	cache : false,
	    	data : '_method=delete&_token={{ csrf_token() }}',
	    	dataType : "json",
	        async : false, 
	    	beforeSend: function(xhr){
	        	if (xhr_remove_item != null) {
	          		xhr_remove_item.abort();
	        	}
	    	}
	    }).done(function(response) {
			$('#cart-table-header-' + key + ', #cart-table-body-' + key).empty();
	    }).fail(function(jqXHR, textStatus) {
			$('#cart-table-header-' + key + ', #cart-table-body-' + key).empty();
	     	//var errors = error_message(jqXHR['responseJSON']['errors']);
	     	//$('.question-reply-validation-' + id).html('<div class="callout warning radius">' + errors + '</div>');
	    });

		return false;
	}

	function calculate_total(quantity, id)
	{
		var price = parseInt($('#cart_price_' + id).val());
		var quantity = parseInt(quantity);
		var total = price * quantity;

		$('.cart-field-hidden' + id).val(total);
		$('.cart_total_price' + id).text($('.cart-field-hidden' + id).val());
		$('.cart-field-hidden' + id).inputmask('unmaskedvalue');
		calculate_grand_total();
	}

	function calculate_grand_total()
	{
		var grand_total = 0;
		$('.cart-field-hidden-list').each(function(key, value){
			var total_price = value['value'].replace(",", "");
			total_price = parseFloat(total_price);
			grand_total += total_price;
		});
		$('.grand-total').val(grand_total);
		$('.grand-total-text').text($('.grand-total').val());
	}
</script>
@endsection