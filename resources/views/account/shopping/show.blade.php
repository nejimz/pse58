@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li><a href="{{ route('shopping.index') }}">Shopping list</a></li>
			<li class="disabled">View</li>
		</ul>
		<dl class="text-center">
			<dt>{{ $row->name }}</dt>
			<dd>{{ $row->description }}</dd>
		</dl>
		<table class="unstriped">
			<thead>
				<tr>
					<th width="20%" class="text-center">Image</th>
					<th width="60%">Item Information</th>
					<th width="20%"></th>
				</tr>
			</thead>
			<tbody>
				@forelse($row->products as $productItem)
				@php
				$product = $row->product($productItem['_id']);
				$image = '//images2.plumbersstock.com/120/120/' . $product->images[0]['id'];
				@endphp
				<tr>
					<td class="text-center">
						<img src="{{ $image }}" width="120px">
					</td>
					<td>
						<a href="{{ route('psProductPage', $product->meta['slug']) }}"><h5>{{ $product->title }}</h5></a>
						<div class="grid-x">
							<div class="cell large-4">
								<input type="number" class="radius" name="" value="{{ $productItem['qty'] }}">
								<p>@ {{ config('site.currency.USD.sign') . '' . $product->product_price }}</p>
								<p>Item Subtotal: @ {{ config('site.currency.sign') . '' . ($product->product_price * $productItem['qty']) }}</p>
							</div>
						</div>
					</td>
					<td>
						<form action="" method="post">
							<input type="hidden" name="" value="">
							<input type="hidden" name="" value="">
							<input type="hidden" name="" value="">
							<input type="hidden" name="" value="">
							<input type="hidden" name="" value="">
							@csrf()
							<button class="button expanded radius">Add To Cart</button>
						</form>
						<form action="" method="post">
							<input type="hidden" name="" value="">
							<input type="hidden" name="" value="">
							<input type="hidden" name="" value="">
							<input type="hidden" name="" value="">
							<input type="hidden" name="" value="">
							@csrf()
							<button class="button expanded hollow radius">Remove Item</button>
						</form>
					</td>
				</tr>
				@empty
				<tr>
					<td class="text-center">No Products</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>

@endsection