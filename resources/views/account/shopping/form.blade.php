@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li><a href="{{ route('shopping.index') }}">Shopping list</a></li>
			<li class="disabled">New</li>
		</ul>
		<h5 class="text-center">Create New List</h5>
		@include('partials.error-messages')
		<form action="{{ $route }}" method="post">
			
			<div class="grid-x grid-padding-x">
				<div class="large-12 medium-12 small-12 cell">
					<label>Shopping List Name::
						<input type="text" class="radius" name="name" autocomplete="off" value="" />
					</label>
				</div>
				<div class="large-12 medium-12 small-12 cell">
					<label>Shopping List Description::
						<textarea name="description" rows="5"></textarea>
					</label>
				</div>

				<div class="large-12 medium-12 small-12 cell text-center">
					<input type="hidden" name="product_id" value="123456">
					@csrf()
					<button class="button radius" type="submit">Save List</button>
				</div>
			</div>

		</form>
	</div>
</div>

@endsection