@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3 medium-3 small-12">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9 medium-9 small-12">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li class="disabled">Shopping list</li>
		</ul>
		<table class="unstriped">
			<thead>
				<tr>
					<th width="20%">Image</th>
					<th width="60%">Item Information</th>
					<th width="20%">Created At</th>
				</tr>
			</thead>
			<tbody>
				@forelse($rows as $row)
				<tr>
					<td>
						@if(isset($row->products) && count($row->products) > 0)
							@foreach($row->products as $product)
								{!! $row->product_image($product['_id'], 60, 60) !!}
							@endforeach
						@endif
					</td>
					<td>
						<a href="{{ route('shopping.show', $row->_id) }}">{{ $row->name }}</a>
						<p>{{ $row->description }}</p>
					</td>
					<td>{{ $row->created_at->format('M. d, Y') }}</td>
				</tr>
				@empty
				<tr>
					<td>
						<div class="callout warning radius">
							<h6 class="text-center">You Currently Do Not Have Any Shopping Lists.</h6>
						</div>
					</td>
				</tr>
				@endforelse
			</tbody>
		</table>
		<p class="text-center">
			<a href="{{ route('shopping.create') }}" class="button radius"><i class="fa fa-plus"></i> Create List</a>
		</p>
	</div>
</div>

@endsection