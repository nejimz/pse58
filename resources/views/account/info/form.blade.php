@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li><a href="{{ route('info.index') }}">Information</a></li>
			<li class="disabled">Edit</li>
		</ul>
		<h6 class="text-center">Edit Personal Information</h6>
		<div class="grid-x grid-padding-x">
			<form action="{{ route('info.update', Sentinel::getUser()->id) }}" method="post">
				@include('partials.error-messages')
				<div class="grid-container">

					<div class="grid-x grid-padding-x">

						<div class="large-6 medium-6 small-12 cell">
							<label>First Name:
								<input type="text" class="radius" name="first_name" autocomplete="off" value="{{ Sentinel::getUser()->first_name }}" />
							</label>
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<label>Phone Number:
								<input type="text" class="radius" name="phone_number" autocomplete="off" value="{{ Sentinel::getUser()->phone_number }}" />
							</label>
						</div>

						<div class="large-6 medium-6 small-12 cell">
							<label>Last Name:
								<input type="text" class="radius" name="last_name" autocomplete="off" value="{{ Sentinel::getUser()->last_name }}" />
							</label>
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<label>Fax:
								<input type="text" class="radius" name="fax" autocomplete="off" value="{{ Sentinel::getUser()->fax }}" />
							</label>
						</div>

						<div class="large-12 medium-12 small-12 cell text-center">
							@method('PUT')
							@csrf()
							<button class="button success radius font-white" type="submit">Save Changes</button>
						</div>

					</div>

				</div>
			</form>
		</div>
	</div>
</div>

@endsection