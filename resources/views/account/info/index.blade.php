@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li class="disabled">Information</li>
		</ul>
		<h4 class="text-center">My Account Information</h4>
		<div class="grid-x grid-padding-x">
			<div class="cell large-6">
				<div class="callout small">
					<h6>My Personal Information</h6>
					Name: {{ Sentinel::getUser()->first_name . ' ' . Sentinel::getUser()->last_name }}<br>
					Account #: {{ Sentinel::getUser()->eclipse_entity }}<br>
					Phone Number: {{ Sentinel::getUser()->phone_number }}<br>
					Fax: {{ Sentinel::getUser()->fax }}<br>
					<a href="{{ route('info.edit', Sentinel::getUser()->id) }}">Edit Personal Information</a>
				</div>
			</div>
			<div class="cell large-6">
				<div class="callout small">
					<h6>My Login Information</h6>
					Email: {{ Sentinel::getUser()->email }}<br>
					Password: ************<br>
					<a href="{{ route('login.edit', Sentinel::getUser()->id) }}">Edit Login Information</a>
				</div>
			</div>
			<!-- <div class="cell auto">&nbsp;</div>
			<div class="cell large-6">
				<div class="callout small">
					<h6>Default Shipping Address</h6>
					<a href="{{ route('address-book.edit', Sentinel::getUser()->id) }}">Edit</a>
				</div>
			</div> -->
		</div>
	</div>
</div>

@endsection