@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		@include('partials.error-messages')
		<ol class="progress-indicator">
			<li class="is-current" data-step="1"></li>
			<li class="" data-step="2"></li>
			<li class="" data-step="3"></li>
		</ol>
	</div>
</div>
<!-- 
<ol class="progress-indicator">
	<li class="is-complete" data-step="1">
	</li>
	<li class="is-complete" data-step="2">
	</li>
	<li class="is-current" data-step="3">
	</li>
</ol>

<ol class="progress-indicator">
	<li class="is-complete" data-step="1">
	</li>
	<li class="is-current" data-step="2">
	</li>
	<li class="" data-step="3">
	</li>
</ol>

<ol class="progress-indicator">
	<li class="is-complete" data-step="">
	<span>Arrive</span>
	</li>
	<li class="is-current" data-step="">
	<span>Check In</span>
	</li>
	<li class="" data-step="">
	<span>Depart</span>
	</li>
</ol>
 -->

@endsection