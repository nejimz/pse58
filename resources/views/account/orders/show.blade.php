@extends('layouts.print')

@section('page-title', 'Checkout')

@section('content')

<div class="grid-x">
	<div class="cell large-10">
		<i class="fa fa-check-circle fa-lg"></i> Order: {{ $order_id }}
	</div>
	<div class="cell large-2 text-right">
		<a href="#"><i class="fa fa-print fa-lg"></i> Print Order</a>
	</div>
</div>
<p>
	<i class="fa fa-info-circle fa-lg"></i> You will receive another email with tracking information when your order has shipped.
</p>
<table class="unstriped collapse" width="100%">
	<tr>
		<td colspan="3">Order Summary</td>
	</tr>
	<tr>
		<td class="text-right" colspan="3"><strong>Order #</strong> {{ $order_id }}</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="grid-x grid-margin-x grid-padding-x">
				<div class="cell large-6">
					<dl>
						<dt>Shipping Address</dt>
						<dd>{{ $row->shipping_name }}</dd>
						<dd>{{ $row->shipping['address1'] }}</dd>
						<dd>{{ $row->shipping['address2'] }}</dd>
						<dd>{{ $row->shipping['city'] }}</dd>
						<dd>{{ $row->shipping['state'] }}</dd>
						<dd>{{ $row->shipping['zipcode'] }}</dd>
					</dl>
				</div>
				<div class="cell large-6">
					<dl>
						<dt>Billing Address</dt>
						<dd>{{ $row->billing_name }}</dd>
						<dd>{{ $row->billing['address1'] }}</dd>
						<dd>{{ $row->billing['address2'] }}</dd>
						<dd>{{ $row->billing['city'] }}</dd>
						<dd>{{ $row->billing['state'] }}</dd>
						<dd>{{ $row->billing['zipcode'] }}</dd>
					</dl>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3">
		@foreach($row->items as $item)
			@php
			$product = App\Models\Catalog\CatalogProduct::where('_id', $item['_id'])->first()
			@endphp
			<div class="grid-x">
				<div class="cell large-2">{{ $item['qty'] }} x</div>
				<div class="cell large-7">{{ $product->title }}</div>
				<div class="cell large-3 text-right">{{ number_format(($item['price'] * $item['qty']), 2) }}</div>
			</div>
		@endforeach
		</td>
	</tr>
	<tr>
		<td width="80%" rowspan="5" valign="top">
			<strong>Payment Method</strong>
			<p>{{ $row->card_type}} ending in xxxx-{{ $row->card_number(true) }}</p>
		</td>
		<th width="10%" class="text-right">Subtotal:</th>
		<td width="10%" class="text-right">
			{{ number_format($row->totals['subtotal'], 2) }}
		</td>
	</tr>
	<tr>
		<th class="text-right">Tax:</th>
		<td class="text-right">
			{{ number_format($row->totals['taxes'], 2) }}
		</td>
	</tr>
	<tr>
		<th class="text-right">Shipping:</th>
		<td class="text-right">
			{{ number_format($row->totals['shipping'], 2) }}
		</td>
	</tr>
	@if(isset($row->totals['processing']) && $row->totals['processing'] > 0)
	<tr>
		<th class="text-right">Processing:</th>
		<td class="text-right">
			{{ number_format($row->totals['processing'], 2) }}
		</td>
	</tr>
	@endif
	<tr>
		<th class="text-right">Order Total:</th>
		<td class="text-right">
			{{ number_format($row->totals['grandTotal'], 2) }}
		</td>
	</tr>
</table>

@endsection