@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li><a href="{{ route('psAccountOrders') }}">Orders</a></li>
			<li class="disabled">Cancel</li>
		</ul>
		<h6>Cancel Order</h6>
		<p>* Indicates a required field</p>
		<form action="{{ route('psOrderCancelPut', $order_id) }}" method="post" enctype="multipart/form-data">
			@include('partials.error-messages')
			
			<div class="grid-x grid-padding-x">

				<div class="large-12 medium-12 small-12 cell">
					<label>*Reason
						<select id="reason" class="radius" name="reason">
							<option value="">Please Select a reason below</option>
							<option value="Shipping Lead Time">Shipping Lead Time</option>
							<option value="Ordered by mistake/No longer needed">Ordered by mistake/No longer needed</option>
							<option value="Item arrived too late">Item arrived too late</option>
							<option value="Damaged">Damaged</option>
							<option value="Defective">Defective</option>
							<option value="Received wrong item">Received wrong item</option>
							<option value="Other">Other</option>
							<option value="Website description error">Website description error</option>
						</select>
					</label>
				</div>

				<div id="description_div" class="large-12 medium-12 small-12 cell" style="{{ ($errors->first('description') || $description != '') ? '' : 'display: none;' }}">
					<label>* Description (Please be as specific as possible)
						<textarea class="form-control radius" name="description" rows="3">{{ $description }}</textarea>
					</label>
				</div>

				<div id="replacement_product_div" class="large-12 medium-12 small-12 cell" style="{{ ($errors->first('replacement_product') || $replacement_product != '') ? '' : 'display: none;' }}">
					<label>
						<input type="checkbox" name="replacement_product" value="1" {{ ($replacement_product != '') ? 'checked' : '' }}> I Need a replacement product
					</label>
				</div>

				<div id="image_div" class="large-12 medium-12 small-12 cell" style="{{ ($errors->first('image')) ? '' : 'display: none;' }}">
					<label>Image *
						<input type="file" class="radius" name="image" >
					</label>
				</div>

				<div class="large-12 medium-12 small-12 cell text-center">
					@method('PUT')
					@csrf()
					<button class="button success radius" type="submit">Submit Request</button>
				</div>

			</div>
		</form>
	</div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$(function(){
		$('#reason').val('<?php echo $reason; ?>');
		$('#reason').change(function(){
			var reason = $(this).val();
			if (reason == 'Damaged') {
				$('#description_div, #replacement_product_div, #image_div').show();
			} else if (reason == 'Defective') {
				$('#description_div').show();
				$('#replacement_product_div, #image_div').hide();
			} else if (reason == 'Received wrong item') {
				$('#description_div, #replacement_product_div, #image_div').show();
			} else if (reason == 'Other') {
				$('#description_div, #replacement_product_div').show();
				$('#image_div').hide();
			} else if (reason == 'Website description error') {
				$('#description_div').show();
				$('#replacement_product_div, #image_div').hide();
			} else {
				$('#description_div, #replacement_product_div, #image_div').hide();
			}
		});
	});
</script>

@endsection