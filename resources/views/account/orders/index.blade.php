@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li class="disabled">Orders</li>
		</ul>
		@include('partials.error-messages')
		<ul class="accordion" data-accordion data-allow-all-closed="true" data-multi-expand="true">
			@foreach($rows as  $row)
			<li class="accordion-item radius" data-accordion-item>
				<a href="#" class="accordion-title">
					<div class="grid-x">
						<div class="cell large-3">
							ORDER PLACED:<br>{{ $row->created_at->format('F d, Y')}}
						</div>
						<div class="cell large-3">
							TOTAL:<br>{{ config('site.currency.USD.sign') . ' ' . number_format(floatval($row->totals['grandTotal']), 2) }}
						</div>
						<div class="cell large-3">
							SHIP TO: <i class="fa fa-question-circle" data-tooltip tabindex="1" data-allow-html="true" title="{!! $row->shipping['firstName'] . ' ' . $row->shipping['lastName'] . '<br />' . $row->shipping['address1'] . (($row->shipping['address2'] != '')? '<br />' . $row->shipping['address2'] : '') . '<br />' . $row->shipping['city'] . ' ' . $row->shipping['state'] . ' ' . $row->shipping['zipcode'] . '<br />' . $row->shipping['country'] !!}"></i><br>{{ $row->shipping['city'] }}
						</div>
						<div class="cell large-3">
							ORDER: #{{ $row->orderId }}<br>
							{!! $row->customerPO . '<br />' ?? '' !!}
							FULFILLMENT ID: {{ $row->eclipse_id }}
						</div>
					</div>
				</a>

				<div class="accordion-content" data-tab-content>
					<div class="grid-x">
						<div class="cell large-9">
							@foreach($row->items as $item)
							@php
							$product = App\Models\Catalog\CatalogProduct::where('_id', $item['_id'])->first()
							@endphp
							<div class="grid-x">
								<div class="cell large-4">
									<img src="//images4.plumbersstock.com/90/90/{{ $product->images[0]['id'] }}" width="90px">
								</div>
								<div class="cell large-4">
									{{ $product->title }}
								</div>
								<div class="cell large-4">
									Shipping Status: {{ ($row->has_shipped)? 'Shipped' : 'Processing' }}
								</div>
							</div>
							@endforeach
						</div>
						<div class="cell large-3">
							<a href="{{ route('psAccountOrdersPrint', $row->orderId) }}" target="_new" class="button radius expanded"><i class="fa fa-print"></i></a>
							<a href="{{ route('psAccountOrdersTracking', $row->orderId) }}" class="button radius expanded">Tracking Information</a>
							@if($row->has_shipped)
							<a href="{{ route('psOrderCancel', $row->orderId) }}" class="button radius expanded hollow">Return Order</a>
							@else
							<a href="{{ route('psOrderCancel', $row->orderId) }}" class="button radius expanded hollow">Cancel Order</a>
							@endif
						</div>
					</div>
				</div>
			</li>
			@endforeach
		</ul>
	</div>
</div>

@endsection