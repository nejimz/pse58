@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li><a href="{{ route('psAccountOrders') }}">Orders</a></li>
			<li class="disabled">Return</li>
		</ul>
		<h6>Return Order</h6>
		<p>* Indicates a required field</p>
		<p class="text-center">
			* Reason
		</p>
	</div>
</div>

@endsection