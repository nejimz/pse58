@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li><a href="{{ route('psAccount') }}">Account</a></li>
			<li><a href="{{ route('info.index') }}">Information</a></li>
			<li class="disabled">Login</li>
		</ul>
		<h4 class="text-center">Edit Login Information</h4>
		<div class="grid-x grid-padding-x">
			<form action="{{ route('login.update', Sentinel::getUser()->id) }}" method="post">
				@include('partials.error-messages')
				<div class="grid-container">

					<div class="grid-x grid-padding-x">

						<div class="large-6 medium-6 small-12 cell">
							<label>Email Address:
								<input type="text" class="radius" name="email" autocomplete="off" placeholder="Only fill out if you wish to change your Email" />
							</label>
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<label>Password:
								<input type="password" class="radius" name="password" autocomplete="off" placeholder="Only fill out if you wish to change your Email" />
							</label>
						</div>

						<div class="large-6 medium-6 small-12 cell">
							<label>Confirm Email:
								<input type="text" class="radius" name="email_confirm" autocomplete="off" placeholder="Only fill out if you wish to change your Email" />
							</label>
						</div>
						<div class="large-6 medium-6 small-12 cell">
							<label>Confirm Password:
								<input type="password" class="radius" name="password_confirm" autocomplete="off" placeholder="Only fill out if you wish to change your Email" />
							</label>
						</div>

						<div class="large-12 medium-12 small-12 cell text-center">
							@method('PUT')
							@csrf()
							<button class="button success radius font-white" type="submit">Save Changes</button>
						</div>

					</div>

				</div>
			</form>
		</div>
	</div>
</div>

@endsection