@extends('layouts.master')

@section('page-title', 'My Account')

@section('content')

<div class="grid-x grid-padding-x">
	<div class="cell large-3">
		@include('partials.account-sidebar')
	</div>
	<div class="cell large-9">
		<ul class="breadcrumbs breadcrumbs-custom">
			<li class="disabled">Account</li>
		</ul>
		<h4 class="text-center">Welcome {{ Sentinel::getUser()->first_name }}</h4>
		<div class="grid-x grid-padding-x">
			<div class="cell large-6">
				<div class="callout small">
					<h6>My Orders</h6>
					<a href="{{ route('shopping.index') }}">My Shopping Cart</a><br>
					<a href="{{ route('psAccountOrders') }}">My Recent Orders</a>
				</div>
			</div>
			<div class="cell large-6">
				<div class="callout small">
					<h6>My Account</h6>
					Name: {{ Sentinel::getUser()->first_name . ' ' . Sentinel::getUser()->last_name }}<br>
					Email: {{ Sentinel::getUser()->email }}<br>
					<a href="{{ route('info.index') }}">View Account Information</a>
				</div>
			</div>
			<div class="cell large-6">
				<div class="callout small">
					<h6>Wish Lists</h6>
					<a href="{{ route('shopping.index') }}">My Wish Lists</a><br>
					<a href="{{ route('shopping.index') }}">Create New Wish List</a>
				</div>
			</div>
			<div class="cell large-6">
				<div class="callout small">
					<h6>My Address Book</h6>
					<a href="{{ route('address-book.index') }}">Manage Address Book</a><br>
					<a href="{{ route('address-book.create') }}">Create New Address</a>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection