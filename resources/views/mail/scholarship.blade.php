@extends('layouts.mail')
@section('email')
	<h3>{{ $msg }}</h3>
	<p>There has been a new Submission for {{$params->name}}</p>
	<p>Email: {{ $params->email }}</p>
	<p>School: <a href="{{ $params->school_url }}">{{$params->school}}</a></p>
	<p>Video: <a href="{{ $params->video_url }}">{{$params->video_url}}</a></p>
	<a href = "{{ url('admin/scholarship/applications') }}">Click Here</a>
@stop