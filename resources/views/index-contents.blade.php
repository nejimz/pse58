<div id="orbit-wrapper-main" class="grid-x margin-top-custom-10">
	<div class="cell auto">
		<div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
			<ul class="orbit-container orbit-container-custom">
				<?php
				$indexCounter = 0;
				?>
				@foreach($banners as $banner)
					<?php
					$banner_is_active = '';
					$banner_is_hidden = 'display:none;';
					if($indexCounter == 0){
						$banner_is_active = 'is-active';
						$banner_is_hidden = '';
					}
					?>

				<li class="orbit-slide" class="{{ $banner_is_active }}" style="{!! $banner_is_hidden !!}">
					<div class="grid-x">
						<div class="cell large-6">
							<a href="{{ $banner->slug }}">
								<img src="//images1.plumbersstock.com/545/545/marketing/{{ $banner->image }}" alt="{{ $banner->alt }}" width="545px" height="545px" class="lazyimg" />
							</a>
						</div>
						<div class="cell large-6">
							<h5 class="slider-header">{{ $banner->title }}</h5>
							<br />
							<p class="slider-content">{{ $banner->content }}</p>
							<br />
							<a href="{{ $banner->slug }}" class="button large hollow radius slider-button">{{ $banner->button }}</a>
						</div>
					</div>
				</li>
				<?php
				$indexCounter++;
				?>
				@endforeach
			</ul>
			<nav class="orbit-bullets">
				@for($i=0; $i<=count($banners)-1; $i++)
				<button data-slide="{{ $i }}" class="{{ ($i == 0)? 'is-active' : '' }}"><span class="show-for-sr"></span></button>
				@endfor
			</nav>
		</div>
	</div>
</div>
<div class="grid-x grid-margin-x margin-top-custom-50">
	<div class="cell small-12 medium-6 large-6">
		<div class="card text-center radius">
			&nbsp;<a href="support/shipping.html"><img src="{{ asset('img/home/spot1.jpg') }}" width="437px" class="lazyimg" /></a>&nbsp;
		</div>
	</div>

	<div class="cell small-12 medium-6 large-6">
		<div class="card text-center radius">
			&nbsp;<a href="delta.html"><img src="{{ asset('img/home/spot2.jpg') }}" width="437px" class="lazyimg" /></a>&nbsp;
		</div>
	</div>
</div>
<h4 class="text-center featured-product-head margin-top-custom-40">Featured Products</h4>
<div id="featured-product-items" class="grid-x grid-margin-x margin-top-custom-30">
	@foreach($featureds as $featured)
	<div class="cell large-4">
		<a href="{{ $featured->product()->meta['slug'] }}"><img src="//{{ $featured->image_url}}" width="350px" class="lazyimg" /></a>
		<h6>{!! $featured->product()->title !!}</h6>
		<div class="grid-x">
			<div class="shrink cell middle featured-price">
				<strike>$1,680.00</strike> 
				$1,008.00
			</div>
			<div class="auto cell">
				<a href="{{ $featured->product()->meta['slug'] }}" class="button hollow radius">Details</a>
			</div>
		</div>
	</div>
	@endforeach
</div>
<div class="grid-x grid-margin-x margin-top-custom-40">
	<div class="cell large-6 small-12">
		<div class="card card-train-pro radius">
			<a href="{{ url('support/contact.html') }}"><img class="lazyimg" src="{{ asset('img/home/spot3.jpg') }}" width="510px"></a>
		</div>
	</div>

	<div class="cell large-6 small-12">
		<table class="table review-table radius">
			<tr>
				<td width="60%" class="review-window">
					<span class="review active">"They have everything! And they actually shipped it as expected. No complaints here."</span>
					<span class="review" style="display: none;">"Quick shipment...also, they took back a part ordered in error without any haggle. Great!"</span>
					<span class="review" style="display: none;">"Knowledge and customer service was more than excellent. Pricing is fantastic. THANK YOU!"</span>
					<span class="review" style="display: none;">"Great help getting the right Irritrol timer, and fast delivery."</span>
					<span class="review" style="display: none;">"Product came on time, was what I ordered, and cheaper than any other company."</span>
					<span class="review" style="display: none;">"Everything was perfect. Shipped quickly, packaged great...installed and worked great."</span>
				</td>
				<td width="40%" class="ratings">
					<a href="https://www.resellerratings.com/store/PlumbersStock" target="_blank">
						@php
						$rating = 5
						@endphp
						@for ($i = 0; $i < floor($rating); $i++)
						<i class="fa fa-star"></i>
						@endfor
						@if (fmod($rating, 1.0) > 0)
						<i class="fa fa-star-half-o"></i>
						@endif
						@for ($i = ceil($rating); $i < 5; $i++)
						<i class="fa fa-star-o"></i>
						@endfor
						<i class="rating-caption">Over <span id="customersSatisfied">80,000</span> satisfied customers!</i>
					</a>
				</td>
			</tr>
		</table>
	</div>
</div>
<h4 class="text-center newest-guide-head margin-top-custom-40">Newest Guides and How-To's</h4>
<div id="newest-guide-grid" class="grid-x margin-top-custom-30">
 	@foreach($blog_posts as $post)
	<div class="cell large-6 small-12">
		<div class="grid-x">
			<div class="cell auto">
				<h5>{!! $post->title !!}</h5>
				<p class="p15">{!! $post->preview !!}</p>
				<a href="{{ $post->img['href'] }}" class="button hollow radius">Read More</a>
			</div>
			<div class="cell auto">
				<img src="//images0.plumbersstock.com/250/250/marketing/{{ $post->img['src'] }}" width="250px" class="lazyimg" />
			</div>
		</div>
	</div>
 	@endforeach
</div>
<div id="ps-info">
	<h4>#1 in Online Plumbing Supply - PlumbersStock.com</h4>
	<p class="p15"> Welcome to PlumbersStock.com! If you are looking to buy plumbing supplies online, then you have come to the right place. Professional plumbers and do-it-yourselfers alike call on us when they need fixtures or plumbing parts at fair prices. As leaders in the home improvement industry, we carry more than just plumbing supply. Here you will find great deals on a wide selection of tools, lawn & garden, and HVAC supply. We carry a wide range of household names and products to ensure that your needs are met, no matter the difficulty of the project that you face. After years of experience providing our customers with discount plumbing supplies, we are confident that we can earn and keep your business.</p>
	<div id="read-more-items" class="p15" style="display:none;">
		<h4>Plumbing Fixtures, Rough Plumbing, Parts, and Accessories</h4>
		<p class="margin-top-custom-20">One thing that sets us apart from the competition is our selection of plumbing parts. We have everything, whether you are looking for rough plumbing or finish. We carry all the names you know and trust, including Delta Faucet, Moen, TOTO, InSinkErator, American Standard, and much more! You can call on us when you’re in need of a brand new sink, a water heater part, or a supply line for your toilet. In short, we have everything and we have the best prices, so you can update your kitchen or bathroom on a tight budget.</p>

		<h4>Heating and Cooling Supply</h4>
		<p class="margin-top-custom-20">As an industry leader in heating and air conditioning supply, PlumbersStock.com has some of the hardest-to-find parts in the industry. In those cold winter months, when your furnace breaks and the local hardware doesn’t carry the part, you need a solution fast for your family. You should come here first, because we have great prices, and if we don’t have it, you probably won’t find it anywhere else. Some of the household brands we carry include Shoemaker, DuraVent, and more. It doesn’t matter if you are looking for a replacement return air grille or a new furnace, we have you covered. </p>

		<h4>Sprinkler and Landscape Supply</h4>
		<p class="margin-top-custom-20">PlumbersStock.com is well-versed in all things home improvement. Though our foundation is in online plumbing supply, our expertise in sprinklers, irrigation, and landscape supply is top-notch. Our prices are what really make us stand out, and we offer our customers trusted names in the industry, including Rainbird, Toro Irrigation, Irritrol, etc. We have sprinkler timers, rain sensors, gardening tools, fake rock covers, and more. Here, you are certain to find the best prices for your landscaping solutions. </p>

		<h4>Tools and Hardware</h4>
		<p class="margin-top-custom-20">Tools can be very expensive and replacement parts can be hard to find, which is why you should familiarize yourself with the selection we have here at PlumbersStock.com. With Milwaukee, Ridgid, Reed, and more great names from which to choose, we carry everything you need, from power tools to essential accessories, as well as hand tools. PlumbersStock has the selection to cover the needs of homeowners, and we even carry the kinds of high-tech plumbing tools and HVAC meters that only industry professionals use. </p>

		<h4 class="margin-top-custom-20">Why Choose PlumbersStock.com for Home Improvement?</h4>
		<h5>Our Partners</h5>
		<p class="margin-top-custom-20">Because of our valued reputation in the marketplace, we have been able to secure relationships with the most trusted plumbing supply companies in the industry. If you need wholesale prices on toilets, faucets, sinks, water heaters, and more, we are certain to have the desired plumbing part from your preferred manufacturer ready to ship to you. </p>
		<h5>Our Selection and Prices</h5>
		<p class="margin-top-custom-20">We literally have hundreds of thousands of discount products from which to choose. Our website conveniently allows you to browse quickly through numerous items that will be delivered right to your door, saving you time and frustration. Ordering plumbing supply online with PlumbersStock.com offers all this convenience and more. </p>
		<h5>Professional Customer Service</h5>
		<p class="margin-top-custom-20">We also put a strong emphasis on customer service. The next time you are in desperate need of the right plumbing product, put our experts to the test. Our customers always come back for more once they've seen the thorough service that we offer. You can order the specific plumbing supply parts you need from the convenience of your home. We allow you to track the status of your order right here on our site. It's no wonder that so many professional plumbers and homeowners choose PlumbersStock.com when they buy plumbing supplies online. Wholesale shoppers can save with our free shipping offer. </p>
		<h5>Shipping Details</h5>
		<p class="margin-top-custom-20">We can promptly get your shipment of discount plumbing supplies underway. In the rare case that we do not have the plumbing part you need in stock, we can still usually deliver in 7-10 business days. In order to meet the expectations set by manufacturers, we ship the products exactly as the manufacturers sell them. As long as you have not installed the fixture or part, we are very generous with our return policy. Just contact one of our representatives and they will walk you through the process. </p>

		<h4>Save on Discount Plumbing, HVAC, Sprinklers, Tools, and More</h4>
		<p class="margin-top-custom-20">Enjoy the lowest home improvement prices on the web at PlumbersStock.com. With so many benefits to shopping with us, there is no reason to go anywhere else. Nobody offers the savings we do, which is why it’s so easy for us to keep our customers. Order plumbing fixtures and parts online from the comfort of your home and keep some of your hard-earned money when you stretch your dollar with PlumbersStock.com. </p>
	</div>
	<a id="read-more-btn" class="button hollow radius">Read More</a>
</div>