
		<div class="callout small callout-footer margin-top-custom-50">
			<div class="container grid-x footer-wrapper">

				<div class="cell large-2 medium-4 small-12">
					<strong>Policies</strong>
					<ul class="menu vertical">
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Shipping Policy</a></li>
						<li><a href="#">Returns & Exchanges</a></li>
					</ul>
				</div>

				<div class="cell large-2 medium-4 small-12">
					<strong>Customer Service</strong>
					<ul class="menu vertical">
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Scholarship Opportunity</a></li>
						<li><a href="#">Frequently Asked Questions</a></li>
					</ul>
				</div>

				<div class="cell large-2 medium-4 small-12">
					<strong>How-To Guides</strong>
					<ul class="menu vertical">
						<li><a href="#">Plumbing Tutorials</a></li>
						<li><a href="#">HVAC Tutorials</a></li>
						<li><a href="#">Sprinkler Tutorials</a></li>
					</ul>
				</div>

				<div class="cell large-2 medium-4 small-12">
					<strong>My Account</strong>
					<ul class="menu vertical">
						<li><a href="#">Order History</a></li>
						<li><a href="#">Account Information</a></li>
						<li><a href="#">My Shopping Lists</a></li>
					</ul>
				</div>

				<div class="cell large-2 medium-4 small-12">
					<strong>PlumbersStock</strong>
					<ul class="menu vertical">
						<li><a href="#">Careers</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="#">Reviews</a></li>
					</ul>
				</div>

				<div class="cell large-2 medium-4 small-12">
					<strong>Newsletter Sign-up</strong>
					<form id="newsletter-sign-up">
						<div class="input-group">
							<input type="text" name="" class="input-group-field radius-left" />
							<div class="input-group-button">
								<button type="submit" class="button small success radius-right" style="background-color: #5cb85c;">Sign Up!</button>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
		<div class="container">
			<div class="grid-x site-badge margin-top-custom-40">
				<div class="cell large-2 medium-3 small-12">
					<a href="https://www.shopperapproved.com/reviews/PlumbersStock.com/" class="shopperlink"><img src="https://c813008.ssl.cf2.rackcdn.com/23883-med.png" style="border: 0" alt="Shopper Award" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by shopperapproved \251 '+d.getFullYear()+'.'); return false;" /></a><script type="text/javascript">(function() { var js = window.document.createElement("script"); js.src = '//www.shopperapproved.com/seals/certificate.js'; js.type = "text/javascript"; document.getElementsByTagName("head")[0].appendChild(js); })();</script><br>
				</div>
				<div class="cell large-2 medium-3 small-12">
					<span id="siteseal">
						<script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=0sHydqExr9LzZpDisP3MQD2aJV1pqoi3djSvP3PiXGVruM6xRdiFQFusmob6"></script>
					</span><br>
				</div>
				<div class="cell large-2 medium-3 small-12">
					<div id="GTS_CONTAINER" style="dispay:inline-table; margin:0 30px;"></div><br>
				</div>
				<div class="cell large-2 medium-3 small-12">
					<a href="https://www.resellerratings.com" onclick="window.open('https://seals.resellerratings.com/landing.php?seller=24302','name','height=760,width=780,scrollbars=1');return false;">
						<img style='border:none;' src='//seals.resellerratings.com/seal.php?seller=24302' oncontextmenu="alert('Copying Prohibited by Law - ResellerRatings seal is a Trademark of Answers.com'); return false;" />
					</a><br>
				</div>
			</div>
			<p class="ps-copy-rights text-center margin-top-custom-40">
				Copyright &copy; 2007 - 2019 <strong class="footer-dot">.</strong> PlumbersStock 
				<span class="footer-terms-of-use"><a href="">Term of Use</a> <strong class="footer-dot">.</strong> <a href="">All Categories</a></span>
			</p>
		</div>