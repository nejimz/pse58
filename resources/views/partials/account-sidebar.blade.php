<h6 class="text-center">My Account</h6>
<ul class="accordion accordion-account" data-accordion data-allow-all-closed="true">
	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">My Orders</a>
		<div class="accordion-content" data-tab-content>
			<a href="{{ route('psCart') }}">My Shopping Cart</a><br/>
			<a href="{{ route('psAccountOrders') }}">My Order History</a><br/>
		</div>
	</li>
	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">My Wist Lists</a>
		<div class="accordion-content" data-tab-content>
			<a href="{{ route('shopping.index') }}">My Wish Lists</a><br/>
			<a href="{{ route('shopping.create') }}">Create New List</a><br/>
		</div>
	</li>
	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">My Account</a>
		<div class="accordion-content" data-tab-content>
			<a href="{{ route('psAccount') }}">My Dashboard</a><br/>
			<a href="{{ route('info.index') }}">My Information</a><br/>
			<a href="{{ route('psAccountSubscriptions') }}">My Subscriptions</a><br/>
		</div>
	</li>
	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">My Address Book</a>
		<div class="accordion-content" data-tab-content>
			<a href="{{ route('address-book.index') }}">My Address Book</a><br/>
			<a href="{{ route('address-book.create') }}">Create New Address</a><br/>
		</div>
	</li>
</ul>