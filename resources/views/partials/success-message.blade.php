@if(session('success_message'))
<div class="callout success text-center">{!! session('success_message') !!}</div>
@elseif(session('custom_error'))
<div class="callout warning text-center">{!! session('custom_error') !!}</div>
@endif