@if($errors->any())
<div class="callout warning">
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@elseif(session('custom_error'))
<div class="callout warning">
	{{ session('custom_error') }}
</div>
@elseif(session('success_msg'))
<div class="callout success">
	{{ session('success_msg') }}
</div>
@endif