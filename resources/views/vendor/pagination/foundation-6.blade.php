@if ($paginator->hasPages())
<nav aria-label="Pagination" class="text-center margin-top-custom-30">
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            <li class="pagination-previous disabled">Previous</li>
        @else
            <li class="pagination-previous">
                <a href="{{ $paginator->previousPageUrl() }}">Previous</a>
            </li>
        @endif

        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="ellipsis" aria-hidden="true"></li>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="current radius">{{ $page }}</li>
                    @else
                        <li><a aria-label="Page {{ $page }}" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        @if ($paginator->hasMorePages())
            <li class="pagination-next radius">
                <a href="{{ $paginator->nextPageUrl() }}">Next</a>
            </li>
        @else
            <li class="pagination-next disabled">Next</li>
        @endif
    </ul>
</nav>
@endif
