<?php
$request = request();
$http_host = $request->server('HTTP_HOST');
$request_scheme = $request->server('REQUEST_SCHEME');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>@yield('page-title') | PlumbersStock</title>
	<meta name="description" content="Buy plumbing supply online wholesale, as well as tools, sprinklers and HVAC supply. Save on plumbing with the home improvement experts at PlumbersStock.com.">
	<meta name="keywords" content="online plumbing supply">
	<meta name="robots" content="NOINDEX,NOFOLLOW">
	<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{ asset('apple-touch-icon-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('apple-touch-icon-precomposed.png') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app-custom.css') }}">
	<style type="text/css">
	</style>
	<script>
	    window.dataLayer = window.dataLayer || [];
	    function gtag() {
	        dataLayer.push(arguments);
	    }
	    gtag('js', new Date());
	    gtag('config', 'UA-3169519-1');
	</script>
</head>
<body>
	<header class="hide-for-small-only">
		<div class="container">
			<div class="grid-x container-header">
				<div class="cell large-6 medium-6">
					<ul class="menu header-menu">
						<li>
							<a title="Follow PlumbersStock on Facebook" target="_blank" href="https://www.facebook.com/PlumbersStock">
								<i class="fa fa-facebook-square fa-lg"></i>
							</a>
						</li>
						<li>
							<a title="Follow PlumbersStock on Linkedin" target="_blank" href="https://www.linkedin.com/company/plumbersstock-com">
								<i class="fa fa-linkedin-square fa-lg"></i>
							</a>
						</li>
						<li>
							<a title="Follow PlumbersStock on Pinterest" target="_blank" href="https://www.pinterest.com/source/plumbersstock.com">
								<i class="fa fa-pinterest-square fa-lg"></i>
							</a>
						</li>
						<li>
							<a title="Follow PlumbersStock on Youtube" target="_blank" href="https://www.youtube.com/user/plumbersstock">
								<i class="fa fa-youtube-square fa-lg"></i>
							</a>
						</li>
						<li>
							<a title="Follow PlumbersStock on Twitter" target="_blank" href="https://twitter.com/plumbersstock">
								<i class="fa fa-twitter-square fa-lg"></i>
							</a>
						</li>
						<li>
							<a class="header-menu-dot">
								<strong class="fa-lg">.</strong>
							</a>
						</li>
						<li>
							<a title="Read Our Blog" target="_blank" href="https://www.plumbersstock.com/blog">
								<span class="fa fa-phone fa-lg"></span> <span class="header-menu-font">435-868-4020</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="cell large-6 medium-6">
					<ul class="menu header-menu float-right">
						@if(Sentinel::check())
						<li>
							<a title="Read Our Blog" href="{{ route('psUserLogout') }}">
								<span class="fa fa-sign-out"></span> <span class="header-menu-font">Logout</span>
							</a>
						</li>
						@else
						<li>
							<a title="Read Our Blog" href="{{ route('psUserRegistration') }}">
								<span class="fa fa-user-plus"></span> <span class="header-menu-font">Register</span>
							</a>
						</li>
						<li>&nbsp;&nbsp;&nbsp;</li>
						<li>
							<a title="Read Our Blog" href="{{ route('psUserLogin') }}">
								<span class="fa fa-sign-in"></span> <span class="header-menu-font">Login</span>
							</a>
						</li>
						@endif
					</ul>
				</div>
			</div>
		</div>
	</header>
	<div class="container header-holder">
		<div id="header-nav" class="grid-x">
			<div class="cell shrink site-logo">
				<a href="{{ route('psDashboard') }}">
					<img src="{{ asset('img/logo.png') }}" class="lazyimg">
				</a>
			</div>
			<div class="cell shrink show-for-small-only burger-menu">
				<a href="javascript:void(0)" data-toggle="offCanvasLeftSplitMenu" class="menu-icon-button" type="button">
					<i class="fa fa-bars fa-2x"></i>
				</a>
			</div>
			<div class="cell auto">
				<form id="fe-search-frm">
					<div class="grid-container">
						<div class="grid-x grid-padding-x">
							<div class="medium-12 cell">
								<div class="input-group">
						        	<input class="input-group-field radius-left" type="text" placeholder="Search" />
									<div class="input-group-button">
										<button type="submit" class="button btn-search radius-right"><i class="fa fa-search"></i></button>
									</div>
								</div>
						    </div>
						</div>
					</div>
				</form>
			</div>
			<div class="cell shrink text-right">
				<a href="{{ route('psCart') }}" class="hollow button primary radius btn-cart">View Cart</a>
				<i class="fa fa-caret-left fa-2x hide-for-small-only view-cart-caret"></i>
				<a href="javascript:void(0)" class="button view-cart-price radius hide-for-small-only">
					{{ config('site.currency.USD.sign') }} 
					@if(session()->has('user.cart-total'))
						{{ number_format(session('user.cart-total'), 2) }}
					@else
						0.00
					@endif
				</a>
			</div>
		</div>
		<div class="grid-x margin-top-custom-10 margin-bottom-custom-20">
			<div class="cell large-12">
				<div class="off-canvas-wrapper">
					<div class="off-canvas position-left" id="offCanvasLeftSplitMenu" data-off-canvas data-transition="overlap"></div>
				</div>
				<div id="site-menus-wrapper" class="off-canvas-content hide-for-small-only" data-off-canvas-content>
					@include('partials.main-menu')
					<?php
						/*$keyMainMenus = $http_host . '-' .  $request_scheme . '-main-menu';
						$main_menus = Cache::store('redis')->remember($keyMainMenus, 180, function(){
							return view('layouts.main-menu-fe')->render();
						});
						echo $main_menus;*/
					?>
				</div>
			</div>
		</div>
		@yield('content')
	</div><!-- div class="container" -->

	<div class="callout small callout-footer margin-top-custom-50">
		<div class="container grid-x footer-wrapper">

			<div class="cell large-2 medium-4 small-12">
				<strong>Policies</strong>
				<ul class="menu vertical">
					<li><a href="{{ route('psSupportPrivacy') }}">Privacy Policy</a></li>
					<li><a href="{{ route('psSupportShipping') }}">Shipping Policy</a></li>
					<li><a href="{{ route('psSupportReturnsAndExchanges') }}">Returns & Exchanges</a></li>
				</ul>
			</div>

			<div class="cell large-2 medium-4 small-12">
				<strong>Customer Service</strong>
				<ul class="menu vertical">
					<li><a href="{{ route('psSupportContactUs') }}">Contact Us</a></li>
					<li><a href="{{ route('psAboutScholarship') }}">Scholarship Opportunity</a></li>
					<li><a href="{{ route('psSupportFAQ') }}">Frequently Asked Questions</a></li>
				</ul>
			</div>

			<div class="cell large-2 medium-4 small-12">
				<strong>How-To Guides</strong>
				<ul class="menu vertical">
					<li><a href="{{ route('psHowToPlumb') }}">Plumbing Tutorials</a></li>
					<li><a href="{{ route('psHowToHVAC') }}">HVAC Tutorials</a></li>
					<li><a href="{{ route('psHowToSprinklers') }}">Sprinkler Tutorials</a></li>
				</ul>
			</div>

			<div class="cell large-2 medium-4 small-12">
				<strong>My Account</strong>
				<ul class="menu vertical">
					<li><a href="{{ route('psAccountOrders') }}">Order History</a></li>
					<li><a href="{{ route('info.index') }}">Account Information</a></li>
					<li><a href="{{ route('shopping.index') }}">My Shopping Lists</a></li>
				</ul>
			</div>

			<div class="cell large-2 medium-4 small-12">
				<strong>PlumbersStock</strong>
				<ul class="menu vertical">
					<li><a href="{{ route('psCareers') }}">Careers</a></li>
					<li><a href="{{ route('psTeam') }}">About Us</a></li>
					<li><a href="{{ route('psSupportContactUs') }}">Our Blog</a></li>
					<li><a href="{{ route('psReviews') }}">Reviews</a></li>
				</ul>
			</div>

			<div class="cell large-2 medium-4 small-12">
				<strong>Newsletter Sign-up</strong>
				<form id="newsletter-sign-up" action="{{ route('psNewsletter') }}" method="POST">
					<div class="input-group">
						<input type="text" name="subscribeEmail" class="input-group-field radius-left" placeholder="xyz@email.com" />
						<div class="input-group-button">
							@csrf()
							<button type="submit" class="button small success radius-right" style="background-color: #5cb85c;">Sign Up!</button>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="grid-x site-badge margin-top-custom-40">
			<div class="cell large-2 medium-3 small-12 text-center">
				<a href="https://www.shopperapproved.com/reviews/PlumbersStock.com/" class="shopperlink"><img src="https://c813008.ssl.cf2.rackcdn.com/23883-med.png" style="border: 0" alt="Shopper Award" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by shopperapproved \251 '+d.getFullYear()+'.'); return false;" /></a><script type="text/javascript">(function() { var js = window.document.createElement("script"); js.src = '//www.shopperapproved.com/seals/certificate.js'; js.type = "text/javascript"; document.getElementsByTagName("head")[0].appendChild(js); })();</script><br>
			</div>
			<div class="cell large-2 medium-3 small-12 text-center">
				<span id="siteseal">
					<script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=0sHydqExr9LzZpDisP3MQD2aJV1pqoi3djSvP3PiXGVruM6xRdiFQFusmob6"></script>
				</span><br>
			</div>
			<div class="cell large-2 medium-3 small-12 text-center">
				<div id="GTS_CONTAINER" style="dispay:inline-table; margin:0 30px;"></div><br>
			</div>
			<div class="cell large-2 medium-3 small-12 text-center">
				<a href="https://www.resellerratings.com" onclick="window.open('https://seals.resellerratings.com/landing.php?seller=24302','name','height=760,width=780,scrollbars=1');return false;">
					<img style='border:none;' src='//seals.resellerratings.com/seal.php?seller=24302' oncontextmenu="alert('Copying Prohibited by Law - ResellerRatings seal is a Trademark of Answers.com'); return false;" />
				</a><br>
			</div>
		</div>
		<p class="ps-copy-rights text-center margin-top-custom-40">
			Copyright &copy; 2007 - 2019 <strong class="footer-dot">.</strong> PlumbersStock 
			<span class="footer-terms-of-use"><a href="{{ route('psSupportPolicies') }}">Term of Use</a> <strong class="footer-dot">.</strong> <a href="">All Categories</a></span>
		</p>
	</div>
	<script src="{{ asset('js/app.js') }}"></script>
	<script>
		$(document).foundation();
		if (Foundation.MediaQuery.is('small only')) {
			$('.site-badge .cell').css('text-align', 'center');
		}
	</script>
	<!-- begin olark code -->
	<!-- <script data-cfasync="false" type='text/javascript'></script> -->
	<noscript><a href="https://www.olark.com/site/4695-424-10-3439/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
	<!-- Custom olark styles go here-->
	<!-- end olark code -->
	@yield('scripts')
</body>
</html>

