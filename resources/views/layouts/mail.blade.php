<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<img src="{{ asset('img/email_logo.png') }}" alt="Plumbersstock Logo" style="padding:0px; margin:0px" />
		@yield('email')
	</body>
</html>