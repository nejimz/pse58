<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>@yield('page-title') | PlumbersStock</title>
	<meta name="description" content="Buy plumbing supply online wholesale, as well as tools, sprinklers and HVAC supply. Save on plumbing with the home improvement experts at PlumbersStock.com.">
	<meta name="keywords" content="online plumbing supply">
	<meta name="robots" content="NOINDEX,NOFOLLOW">
	<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
	<link rel="apple-touch-icon" href="{{ asset('apple-touch-icon-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('apple-touch-icon-precomposed.png') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app-custom.css') }}">
	<style type="text/css">
	</style>
</head>
<body>
	<div class="container">
		@yield('content')
	</div><!-- div class="container" -->
	<script src="{{ asset('js/app.js') }}"></script>
	<script>
		$(document).foundation();
		if (Foundation.MediaQuery.is('small only')) {
			$('.site-badge .cell').css('text-align', 'center');
		}
	</script>
	<!-- begin olark code -->
	<!-- <script data-cfasync="false" type='text/javascript'></script> -->
	<noscript><a href="https://www.olark.com/site/4695-424-10-3439/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
	<!-- Custom olark styles go here-->
	<!-- end olark code -->
	@yield('scripts')
</body>
</html>

