@extends('layouts.master')

@section('page-title', 'About Us')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">About Us</li>
</ul>

<div class="support-wrapper">
	<h4 class="text-center support-head">Our Vision</h4>
	<p class="margin-top-custom-10">
		PlumbersStock.com was founded with the vision of providing top quality plumbing parts and brands at wholesale prices to customers around the world. PlumbersStock was created and is backed by a multi-million dollar family-owned and operated wholesale company with decades of experience in the Plumbing, HVAC, and Irrigation industry. With our first plumbing supply store established over 20 years ago, we are full of experience and we strive to be the best in the business. Now we can offer our excellent plumbing parts online, allowing us to supply the entire United States. We are here to provide you with the knowledge, products, prices, and support usually reserved only for contractors and service professionals. 
	</p>
	<h5 class="support-header margin-top-custom-10">How Do We Differ from Other Plumbing Supply Stores?</h5>
	<p class="margin-top-custom-10">
		Representing many of the best known names in the industry, we take care to ensure you have access to the highest quality products and resources without the overpricing and overall cost inflation that you find shopping with many of our competitors. Some competitors offer cheap plumbing supplies at expensive prices. We offer affordable, but quality plumbing parts. To help cut your cost even more we also offer free shipping on bulk orders. 
	</p>
	<p>
		Most online retailers that offer industry specific and specialty products have more in common with "Marketing" and "Big Box" companies. If you go with another online plumbing supply store you are likely to find they sell products that are simply listings driven by profit margins, with little or no firsthand knowledge of how it works or the industry standards involved. This means that product support invariably gets directed to manufacturers. We keep a full-service in house call center with knowledgeable personnel who can answer any question you might have about a plumbing part. We have on our staff licensed plumbing contactors as well as dozens of wholesale professionals with over 20 years experience specializing in all of our various product lines. Within our organization we can find the answer to any question in our industry. 
	</p>
	<p>
		Our online competitors don't generally buy direct from manufactures, they buy inventory from 3rd party warehouses and wholesale stock. We buy our inventory direct, this allows us to offer lower prices, better warranty support, and complete product lines. There is no better place to buy plumbing parts online. 
	</p>
	<h5 class="support-header margin-top-custom-10">Order Fulfillment Process</h5>
	<p class="margin-top-custom-10">
		Because we offer every product that our manufactures produce, some items on our site are special order and those specific plumbing parts are marked with a shipping lead time. But rest assured, our system allows us to process, place and fill out-of-stock orders faster than anyone else. The moment an order is placed, our purchasers are alerted and they place an order with the manufacture. The same day the product is received in our warehouse it is quickly and efficiently shipped to you. 
	</p>
	<p>
		We also operate our own shipping and order fulfillment center out of our own inventory warehouses so that we can quickly send your needed plumbing parts. Online orders can be processed very quickly. Outfitted with some of the best equipment & technology available and co-located with the bulk of our product inventory, customer's orders are processed, packaged and shipped accurately and immediately. Don't settle for cheap plumbing supplies from our competitors. Choose PlumbersStock.com for affordable prices and excellent products. 
	</p>
	<h5 class="support-header margin-top-custom-10">Proudly Providing the Best Plumbing Parts Online</h5>
	<p class="margin-top-custom-10">
		Here at PlumbersStock.com, we understand the importance of making sure our customers are satisfied with both our plumbing parts and our services. Our customers benefit from the excellent pricing reliability of a large industry supplier, with the courtesy, knowledge, and customer service of a family owned business. We pride ourselves on striving for excellence in every facet of what we do. We know you'll rely on PlumbersStock.com as your source for Plumbing, HVAC, and Irrigation supplies. 
	</p>
</div>

@endsection