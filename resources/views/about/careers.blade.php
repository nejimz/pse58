@extends('layouts.master')

@section('page-title', 'Careers')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Careers</li>
</ul>
@include('partials.success-message')
<div class="support-wrapper">
	<h4 class="text-center support-head">Careers with PlumbersStock</h4>
	<p class="margin-top-custom-10">
		We have the need for all kinds of talent, and if you are looking for a nice work environment with opportunity to rise in the ranks, then you’ve found it. Whether you want to work for the corporate offices in web marketing, web design, customer service, etc., or you’re good with logistics and want a warehouse job, <a href="#application-form">apply now</a>. We’ve got use for plumbing professionals, those with HVAC knowledge, and experts in sprinkler supply with our PlumbersStock. There is a ton of opportunity for driven, intelligent people. We value our employees and would love you to come see if you are a good fit for the team. 
	</p>

	<div class="grid-x">
		<div class="cell auto"></div>
		<div class="cell large-8">
			<form id="application-form" action="{{ route('psCareersPost') }}" method="post" enctype="multipart/form-data">
				<h4 class="text-center support-header">Submit your application</h4>
				<div class="grid-container">

					<div class="grid-x">
						<div class="large-12 medium-12 small-12 cell">
							<label><strong>Full Name</strong>
								<input type="text" class="radius" name="full_name" autocomplete="off" value="{{ $full_name }}" required="" />
							</label>
							@if($errors->first('full_name'))
								<p class="alert-first">{{ $errors->first('full_name') }}</p>
							@endif
						</div>
					</div>

					<div class="grid-x">
						<div class="large-12 medium-12 small-12 cell">
							<label><strong>Email</strong>
								<input type="email" class="radius" name="email" autocomplete="off" value="{{ $email }}" required="" />
							</label>
							@if($errors->first('email'))
								<p class="alert-first">{{ $errors->first('email') }}</p>
							@endif
						</div>
					</div>

					<div class="grid-x">
						<div class="large-12 medium-12 small-12 cell">
							<label><strong>Phone</strong>
								<input type="text" class="radius" name="phone" autocomplete="off" value="{{ $phone }}" />
							</label>
							@if($errors->first('phone'))
								<p class="alert-first">{{ $errors->first('phone') }}</p>
							@endif
						</div>
					</div>

					<div class="grid-x">
						<div class="large-12 medium-12 small-12 cell">
							<label><strong>Resume/CV</strong></label>
							How would you like to submit your resume?
							<label><input type="radio" name="application_type" id="application_type" class="application_type_document" value="document" checked=""> Upload Document</label>
							<label><input type="radio" name="application_type" id="application_type" class="application_type_url" value="url"> Online Resume</label>
						</div>
					</div>

					<div class="grid-x">
						<div class="large-12 medium-12 small-12 cell">
							<label id="inputFile" style="display: none;"><strong>Upload Document</strong>
								<input type="file" class="radius" name="document" autocomplete="off" value="" />
								@if($errors->first('document'))
									<p class="alert-first">{{ $errors->first('document') }}</p>
								@endif
							</label>
							<label id="inputUrl" style="display: none;"><strong>Enter URL</strong>
								<input type="text" class="radius" name="url" autocomplete="off" value="{{ $url }}" />
								@if($errors->first('url'))
									<p class="alert-first">{{ $errors->first('url') }}</p>
								@endif
							</label>
						</div>
					</div>

					<div class="grid-x">
						<div class="large-12 medium-12 small-12 cell">
							<label for="cover-letter"><strong>Cover Letter (Optional)</strong></label>
							<textarea rows="10" id="cover_letter" name="cover_letter">{{ $cover_letter }}</textarea>
						</div>
					</div>

					<div class="grid-x">
						<div class="large-12 medium-12 small-12 cell text-center">
							@csrf()
							<button class="button success2 radius" type="submit">Submit Application</button>
						</div>
					</div>

				</div>
			</form>
		</div>
		<div class="cell auto"></div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){
		<?php
		if ($application_type == 'url') {
			echo '$(".application_type_url").attr("checked", "checked");$("#inputFile").hide();$("#inputUrl").show();';
		} else {
			echo '$(".application_type_document").attr("checked", "checked");$("#inputUrl").hide();$("#inputFile").show();';
		}
		?>

		$('input[name=\'application_type\'').change(function(){
			var value = $(this).val();
			if (value == 'document') {
				$('#inputFile').show();
				$('#inputUrl').hide();
			} else {
				$('#inputUrl').show();
				$('#inputFile').hide();
			}
		});
	});
</script>
@endsection