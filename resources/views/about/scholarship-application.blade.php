@extends('layouts.master')

@section('page-title', 'Scholarship Application')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">Scholarship Application</li>
</ul>
@include('partials.success-message')
<div class="support-wrapper">
	<h4 class="text-center support-head">Plumbing and HVAC as a Career</h4>
	<p class="margin-top-custom-10">
		PlumbersStock.com is pleased to offer a unique scholarship opportunity for those studying at colleges and trade schools to become <strong>plumbers, HVAC technicians or sprinkler and irrigation specialists</strong> (or other related fields, like construction management). We understand the importance of education, and as proud members of the community, we like to give back. In an effort to help students achieve their dreams, we are offering scholarships to three winners who will share <strong>a total of $3,000</strong>.
	</p>
	<h5 class="support-header margin-top-custom-20">Who Is Eligible to Participate in the PlumbersStock Student Scholarship Contest?</h5>
	<p class="margin-top-custom-10">
		For eligibility, you must be enrolled at a participating school for higher learning (if your school is not participating, please urge them to and <a href="{{ route('psSupportContactUs') }}">contact us</a> about it). The program is designed with the intent that all participants can their job placement prospects. To apply for the scholarship, <strong>please follow these steps:</strong>
	</p>
	<ol class="support-ul">
		<li> Create a "how-to-guide" video for home improvement novices demonstrating your skill in plumbing, HVAC, or irrigation. Examples: "How to Replace a Toilet", "How to Fix a Leaky Faucet", "How to Diagnose Water Heater Issues", etc.</li>
		<li>Follow the guidelines below to publish your video.</li>
		<li>Complete the <a href="{{ route('psSupportContactUs') }}">submission form</a> so that we are notified of your participation.</li>
	</ol>
	<p>
		The student with the best video will receive <strong>$2,000</strong> to complete their education.
	</p>
	<p>
		The two runners-up will both receive <strong>$500</strong> in scholarship money.
	</p>
	<p>
		<strong>25 students will receive "honorable mention"</strong> which will be documented on our blog. They can list this honor on their resume. With a quality video that you can show to prospective employers, <strong>you are sure to improve your employment potential.</strong> We designed our scholarship contest with this intent so that all participants can "win", in a sense. 
	</p>
	<h6 class="support-header margin-top-custom-20">To Officially Submit:</h6>
	<ol class="support-ul margin-top-custom-10">
		<li>
			<strong>Create your video:</strong> Please edit the videos so that they are no longer than 10 minutes. There is no minimum requirement, but just remember the more informative the better. Please have an introduction at the beginning of the video where you state
			<ul>
				<li>The Topic.</li>
				<li>Your First Name.</li>
				<li>That it is a Student Project.</li>
			</ul>
			Your video will be graded primarily on the information presented and how useful it is to viewers. Secondarily, it will be graded on creativity and production quality. 
		</li>
		<li><strong>Publish your video:</strong> Post your completed how-to-guide to a free video hosting site (like YouTube or Vimeo). </li>
		<li>
			<strong>Endorse your video:</strong> When you publish the video, in the description please state:
			<ul>
				<li>What your video is about.</li>
				<li>What school you go to.</li>
				<li>Explain that it is for the PlumbersStock.com Student Scholarship Contest.</li>
				<li>Leave a link to a relevant page on our website: <a href="{{ route('psDashboard') }}">www.PlumbersStock.com</a></li>
			</ul>
		</li>
		<li>
			<strong>Complete the Form Below:</strong>
			<form method="post" action="{{ route('psAboutScholarshipPost') }}" enctype="multipart/form-data">

				<div class="grid-container">
					<div class="grid-x">
						<div class="large-6 medium-6 small-12 cell">
							<label><strong>Full Name</strong>
								<input type="text" class="radius" name="full_name" autocomplete="off" value="{{ $full_name }}" />
							</label>
							@if($errors->first('full_name'))
								<p class="alert-first">{{ $errors->first('full_name') }}</p>
							@endif
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>

					<div class="grid-x">
						<div class="large-6 medium-6 small-12 cell">
							<label><strong>Email Address</strong>
								<input type="email" class="radius" name="email" autocomplete="off" value="{{ $email }}" />
							</label>
							@if($errors->first('email'))
								<p class="alert-first">{{ $errors->first('email') }}</p>
							@endif
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>

					<div class="grid-x">
						<div class="large-6 medium-6 small-12 cell">
							<label><strong>Confirm Email Address</strong>
								<input type="text" class="radius" name="confirm_email" autocomplete="off" value="{{ $confirm_email }}" />
							</label>
							@if($errors->first('confirm_email'))
								<p class="alert-first">{{ $errors->first('confirm_email') }}</p>
							@endif
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>

					<div class="grid-x">
						<div class="large-6 medium-6 small-12 cell">
							<label><strong>School Name</strong>
								<input type="text" class="radius" name="school_name" autocomplete="off" value="{{ $school_name }}" />
							</label>
							@if($errors->first('school_name'))
								<p class="alert-first">{{ $errors->first('school_name') }}</p>
							@endif
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>

					<div class="grid-x">
						<div class="large-6 medium-6 small-12 cell">
							<label><strong>School URL</strong>
								<input type="text" class="radius" name="school_url" autocomplete="off" value="{{ $school_url }}" />
							</label>
							@if($errors->first('school_url'))
								<p class="alert-first">{{ $errors->first('school_url') }}</p>
							@endif
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>

					<div class="grid-x">
						<div class="large-6 medium-6 small-12 cell">
							<label><strong>Video URL</strong>
								<input type="text" class="radius" name="video_url" autocomplete="off" value="{{ $video_url }}" />
							</label>
							@if($errors->first('video_url'))
								<p class="alert-first">{{ $errors->first('video_url') }}</p>
							@endif
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>

					<div class="grid-x">
						<div class="large-6 medium-6 small-12 cell text-center">
							@csrf()
							<button class="button success radius font-white" type="submit">Submit Application</button>
						</div>
						<div class="auto cell">&nbsp;</div>
					</div>

				</div>

			</form>
		</li>
	</ol>
	<h6 class="support-header margin-top-custom-20">Terms & Conditions</h6>
	<strong class="margin-top-custom-10">Eligibility</strong>
	<p>
		This opportunity is available to any high school seniors who will be soon enrolling at a participating school, as well as any students who are currently enrolled (including part-timers). Only those going to schools within the United States are eligible. To qualify for the PlumbersStock Student Scholarship Contest you must meet all these requirements. 
	</p>
	<strong class="margin-top-custom-10">Deadline</strong>
	<p>
		Submissions must be received by <strong>March 31, 2020</strong> to qualify. If submitted after March 31st, they will be considered for the next scholarship opportunity. There are no citizenship requirements. 
	</p>
	<strong class="margin-top-custom-10">Selecting a Winner</strong>
	<p>
		All entries will be evaluated by our chosen panel of judges. They will be judged by the information presented, creativity, and overall quality (though production quality is the least important factor). Check back on this page for the announcement of the winners on April 30, 2020. Once we can verify eligibility, the winners will receive e-mail notification prior to the public announcement. The students' approved schools will be endowed with a one-time disbursement of $2,000 or $500 as a scholarship for their winning student. The student must ensure receipt of funds by the school and if anything is amiss, give us notification by <strong>July 1, 2020</strong>. If the school prefers that the student receive the scholarship directly, arrangements can be made to comply. 
	</p>
	<p>
		PlumbersStock.com reserves the right to discontinue this scholarship contest at anytime without any notice. This scholarship contest is void where prohibited. 
	</p>
	<p>
		<a href="{{ route('psAboutScholarshipWinner') }}" class="button hollow radius">See past winners</a>
	</p>
</div>

@endsection