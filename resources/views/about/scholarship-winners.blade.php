@extends('layouts.master')

@section('page-title', 'Scholarship Winners')

@section('content')

<ul class="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li class="disabled">Scholarship Winners</li>
</ul>

<div class="support-wrapper">
	<h4 class="text-center support-head">Scholarship Winners</h4>
	<div class="grid-x margin-top-custom-40 grid-padding-x">
		@foreach($winners as $winner)
		<div class="cell large-6">
			<div class="callout radius">
				<div class="media-object-section">
					<iframe src="//{{ $winner->video_url }}" allowfullscreen="true" frameborder="0" width="100%" style="min-height:350px;max-width:600px;"></iframe>
				</div>
				<div class="grid-x text-center">
					<div class="cell large-5">
						<h6><i class="fa fa-trophy fa-3x" style="color:#ffb646;"></i><br><strong>{!! $winner->created_at->toDateTime()->format('Y') !!} Winner</strong></h6>
					</div>
					<div class="cell large-7">
						<h4 style="margin-bottom: 2px;" class="margin-top-custom-10">{{ $winner->name }}</h4>
						{{ $winner->school }}
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>

@endsection