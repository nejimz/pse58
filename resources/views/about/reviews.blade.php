@extends('layouts.master')

@section('page-title', 'PlumbersStock.com Reviews')

@section('content')

<ul class="breadcrumbs">
	<li><a href="{{ route('psDashboard') }}">Home</a></li>
	<li class="disabled">PlumbersStock.com Reviews</li>
</ul>

<div class="support-wrapper">
	<h4 class="">PlumbersStock Reviews</h4>
	<p class="margin-top-custom-10">
		PlumbersStock.com takes pride in knowing that of all the things we do, providing great customer support ranks among our finest accomplishments. We continually strive to treat every order with care with the goal of meeting our customers' (both domestic and international) needs. As with any business, we sometimes suffer human error, resulting in untimely mistakes. In such an event where we do not meet customer expectations, our friendly and knowledgeable customer service department is willing and able to assist in any way needed. Within our ever-expanding inventory, we offer our customers access to not only everyday products, but also items that are rare. These items are often expensive to package, or may require longer lead times. Please browse through these <strong>PlumbersStock reviews</strong> from our valued customers and see for yourself how we handle the everyday and the extraordinary. It is our goal here at <a href="{{ route('psDashboard') }}">PlumbersStock.com</a> to provide customers with professional service, quality support, and products they desire at a price they can afford.
	</p>
	<div id="shopper_review_page">
		<div id="review_header"></div>
		<div id="merchant_page"></div>
		<div id="review_image"><a href="http://www.shopperapproved.com/reviews/PlumbersStock.com/" target="_blank" rel="nofollow"></a></div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript"> var sa_review_count = 20; var sa_date_format = 'F j, Y'; function saLoadScript(src) { var js = window.document.createElement("script"); js.src = src; js.type = "text/javascript"; document.getElementsByTagName("head")[0].appendChild(js); } saLoadScript('//www.shopperapproved.com/merchant/23883.js'); </script>
@endsection