$(function(){
	var fresh_menu = $('#site-menus-wrapper').html();

	$('#offCanvasLeftSplitMenu').html(fresh_menu);
	$('#offCanvasLeftSplitMenu ul').addClass('vertical drilldown text-left');
	$('#offCanvasLeftSplitMenu ul li').addClass('text-left').removeClass('text-center');
	var drilldown = new Foundation.Drilldown($('#offCanvasLeftSplitMenu'), 'data-auto-apply-class');
	$('.is-drilldown').attr('style', '');

	$('.lazyimg').lazyload({
	    effect : "fadeIn",
	    effect_speed: 1000,
	    load: function(elements_left, settings){
	       // code here
    	}
   });
});

$(function() {
	var ele = $('#customersSatisfied');
	var clr = null;
	var start = 75000;
	var increment = 43;
	var speed = [100,150,300,10,200];
	var place = 0;
	(loop = function() {
		clearTimeout(clr);
		(inloop = function() {
			ele.html(start+=1);
			if(!(start % increment)) {
				return;
			}
			clr = setTimeout(inloop, speed[place]);
		})();
		setTimeout(loop, (Math.random() * 1500) >> 0 );
		place+=1;
		if(place > 4){
			place = 0;
		}
	})();
});

$(function() {
	var reviewTimer = window.setInterval(function() {
		var current	= $('.review-window .review.active'),
			next	= current.next('.review');

			if (next.length == 0) {
				next = $('.review-window .review').eq(0);
			}

			$.when(current.fadeOut()).then(function() {
				current.removeClass('active');

				next.fadeIn();

				next.addClass('active');
			});
	}, 10000);
});

$(function() {
	$('#read-more-btn').click(function(e){
		$('#read-more-items').toggle();

		var label = $(this).text();

		if (label == 'Read More') {
			$(this).text('Read Less');
		} else {
			$(this).text('Read More')
		}
		e.preventDefault();
	})
});

$(function(){
	var xhr_newsletter_sign_up = null;
	$('#newsletter-sign-up').submit(function(e){
		xhr_newsletter_sign_up = $.ajax({
			type : $(this).attr('method'),
			url : $(this).attr('action'),
			data : $(this).serialize(),
			cache : false,
			dataType : "json",
			async : false, 
			beforeSend: function(xhr){
				if (xhr_newsletter_sign_up != null) {
					xhr_newsletter_sign_up.abort();
				}
			}
		}).done(function(result) {
			alert(result);
		}).fail(function(jqXHR, textStatus) {
			alert('An error has occur. Please reload your browser.');
		});
		//alert('action=' + $(this).attr('action') + '; type=' + $(this).attr('method') + ';');
		e.preventDefault();
	});
});

function error_message(responseJSON)
{
	var error_string = '';
	$.each(responseJSON, function(key, value){
		error_string += '' + value[0] + '<br />';
	});

	return error_string;
}

function increment_decrement(id, max, type)
{
	var field = $('#' + id).val();

	if(isNaN(field)) {
		field = 1;
		alert('Input is not a number!');
	} else {
		field = parseInt(field);

		if (type == 'inc') {
			if (field < max || max == 0) {
				field++;
			}
		} else if (type == 'dec') {
			field--;
			if (field < 1) {
				field = 1;
			}
		} else {

			if (max == 0) {
				field = field;
			} else if (field > max) {
				field = max;
			} else if (field < 1) {
				field = 1;
			}
		}
		$('.increment_decrement_validation_msg').html('');
	}
	$('#' + id).focus().val(field).change();//.trigger( 'change' );
	return false;
}

/*** GTS ***/
(function() {
	var gts = document.createElement("script");
	gts.type = "text/javascript";
	gts.async = true;
	gts.src = "https://www.googlecommerce.com/trustedstores/api/js";
	var s = document.getElementsByTagName("script")[0];
	s.parentNode.insertBefore(gts, s);
})();

var gts = gts || [];
gts.push(["id", "518485"]);
gts.push(["badge_position", "USER_DEFINED"]);
gts.push(["badge_container", "GTS_CONTAINER"]);
gts.push(["locale", "en_US"]);
gts.push(["google_base_offer_id", "2619037"]);
gts.push(["google_base_subaccount_id", "2619037"]);
gts.push(["google_base_country", "US"]);
gts.push(["google_base_language", "en"]);
/*** GTS End ***/


/*** begin olark code ***/
/*<![CDATA[*/
window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
		f[z]=function(){
		(a.s=a.s||[]).push(arguments)};var a=f[z]._={
		},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
		f[z]("call",n,arguments) }})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
		0:+new Date};a.P=function(u){
		a.p[u]=new Date-a.p[0]};function s(){
		a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
		hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
		return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
		b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
		b.contentWindow[g].open()}catch(w){
		c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
		var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
		b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
		loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
		/* custom configuration goes here (www.olark.com/documentation) */
		olark.identify('4695-424-10-3439');/*]]>*/
